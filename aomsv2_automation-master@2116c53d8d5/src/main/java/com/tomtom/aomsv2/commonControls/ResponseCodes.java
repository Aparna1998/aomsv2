/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.commonControls;

public class ResponseCodes {

    // Add Feature
    public static final int ADD_FEATURE_SUCCESS_RESPONSE_CODE = 204;
    public static final int ADD_FEATURE_MALFORMED_REQUEST_CODE = 400;
    public static final int ADD_FEATURE_CLIENT_UNAUTHORIZED_CODE = 403;
    public static final int ADD_FEATURE_ID_ALREADY_PRESENT_CODE = 409;

    // Add Package
    public static final int ADD_PACKAGE_SUCCESS_RESPONSE_CODE = 204;
    public static final int ADD_PACKAGE_MALFORMED_REQUEST_CODE = 400;
    public static final int ADD_PACKAGE_ALREADY_EXISTS = 409;
    public static final int ADD_PACKAGE_CLIENT_UNAUTHORIZED = 403;

    // Update Package
    public static final int UPDATE_PACKAGE_MALFORMED_REQUEST_CODE = 400;
    public static final int UPDATE_PACKAGE_UPDATED_SUCCESSFULLY = 204;
    public static final int UPADTE_PACKAGE_ALREADY_IN_SAME_STATE = 409;
    public static final int UPDATE_PACKAGE_CLIENT_UNAUTHORIZED = 403;

    // FetchFeature
    public static final int FETCH_FEATURE_NO_SUCH_FEATURE_FOUND = 404;
    public static final int FETCH_FEATURE_MALFORMED_REQUEST_CODE = 400;
    public static final int FETCH_FEATURE_SUCCESS_RESPONSE_CODE = 200;
    public static final int FETCH_FEATURE_CLIENT_UNAUTHORIZED_CODE = 403;

    // FetchPackage
    public static final int FETCH_PACKAGE_NO_SUCH_FEATURE_FOUND = 404;
    public static final int FETCH_PACKAGE_MALFORMED_REQUEST_CODE = 400;
    public static final int FETCH_PACKAGE_SUCCESS_RESPONSE_CODE = 200;
    public static final int FETCH_PACKAGE_CLIENT_UNAUTHORIZED_CODE = 403;

    // FetchLicense
    public static final int FETCH_LICENSE_NO_SUCH_PACKAGE_FOUND = 409;
    public static final int FETCH_LICENSE_MALFORMED_REQUEST_CODE = 400;
    public static final int FETCH_LICENSE_SUCCESS_RESPONSE_CODE = 200;
    public static final int FETCH_LICENSE_CLIENT_UNAUTHORIZED = 403;

    // DeletePackage
    public static final int DELETE_PACKAGE_DELETED_SUCCESSFULLY = 204;
    public static final int DELETE_PACKAGE_BAD_REQUEST_CODE = 400;
    public static final int DELETE_PACKAGE_CLIENT_UNAUTHORIZED = 403;

    // DeleteFeature
    public static final int DELETE_FEATURE_DELETED_SUCCESSFULLY = 204;
    public static final int DELETE_FEATURE_BAD_REQUEST_CODE = 400;
    public static final int DELETE_FEATURE_CLIENT_UNAUTHORIZED = 403;

}
