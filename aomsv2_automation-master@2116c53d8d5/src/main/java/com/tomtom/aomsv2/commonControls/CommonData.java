/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.commonControls;

import java.io.InputStream;
import java.util.Properties;

public class CommonData {

    public static String ADD_FEATURE = null;
    public static String ADD_PACKAGE = null;
    public static String FETCH_FEATURE = null;
    public static String FETCH_PACKAGE = null;
    public static String UPDATE_PACKAGE = null;
    public static String FETCH_LICENSE = null;
    public static String DELETE_PACKAGE = null;
    public static String DELETE_FEATURE = null;

    public static final String USER_DIRECTORY = System.getProperty("user.dir");
    public static Properties prop = loadProperties();

    public static Properties loadProperties() {
        try {

            Properties prop = new Properties();
            InputStream input = CommonData.class.getClassLoader().getResourceAsStream("config.properties");
            prop.load(input);

            ADD_FEATURE = prop.getProperty("ADD_FEATURE_RESOURCE_URI");
            ADD_PACKAGE = prop.getProperty("ADD_PACKAGE_RESOURCE_URI");
            FETCH_FEATURE = prop.getProperty("FETCH_FEATURE_RESOURCE_URI");
            FETCH_PACKAGE = prop.getProperty("FETCH_PACKAGE_RESOURCE_URI");
            UPDATE_PACKAGE = prop.getProperty("UPDATE_PACKAGE_RESOURCE_URI");
            FETCH_LICENSE = prop.getProperty("FETCH_LICENSE_RESOURCE_URI");
            DELETE_PACKAGE = prop.getProperty("DELETE_PACKAGE_RESOURCE_URI");
            DELETE_FEATURE = prop.getProperty("DELETE_FEATURE_RESOURCE_URI");

        } catch (Exception e) {
            System.out.println("Properties file is not properly loaded");
        }
        return prop;

    }

    public static final String ADD_FEATURE_TEST_DATA_PATH = "/src/test/resources/testData/AddFeature.xlsx";
    public static final String ADD_PACKAGE_TEST_DATA_PATH = "/src/test/resources/testData/AddPackage.xlsx";
    public static final String UPDATE_PACKAGE_TEST_DATA_PATH = "/src/test/resources/testData/UpdatePackage.xlsx";
    public static final String FETCH_FEATURE_TEST_DATA_PATH = "/src/test/resources/testData/FetchFeature.xlsx";
    public static final String FETCH_PACKAGE_TEST_DATA_PATH = "/src/test/resources/testData/FetchPackage.xlsx";
    public static final String FETCH_LICENSE_TEST_DATA_PATH = "/src/test/resources/testData/FetchLicense.xlsx";
    public static final String DELETE_PACKAGE_TEST_DATA_PATH = "/src/test/resources/testData/DeletePackage.xlsx";
    public static final String DELETE_FEATURE_TEST_DATA_PATH = "/src/test/resources/testData/DeleteFeature.xlsx";
    public static final String PREREQUISITE_TEST_DATA_PATH = "/src/test/resources/testData/TestDataPrerequisite.xlsx";
    public static final String INFO_JSON_PATH= "info.json";
    public static final String CPT_METADATA_XSD_PATH = "/src/test/resources/cpt-metadata-2.0.0.xsd";
    public static final String CPT_OEM_1_0_XSD_PATH = "/src/test/resources/cpt-oem-1.0.xsd";
    public static final String CPT_OEM_SAMPLE_1_1_PATH = "/src/test/resources/cpt-oem-sample-1.1.xsd";
    public static final String SAMPLE_XSD_PATH = "/src/main/resources/sample.xsd";
    public static final String SAMPLE_XML_PATH = "/src/main/resources/samplee.xml";
    public static final String METADATA_XML_PATH = "/src/main/resources/metadataSource.xml";
    public static final String INVALIDMETADATA_XML_PATH = "/src/test/resources/invalidmetadata.xml";
    public static final String APIGEE_SECURITY_CERTIFICATE_PATH = "/src/main/resources/AOMS_CLIENT_PREPROD_001.p12";
    public static final String BASE_URI = System.getProperty("BASEURI");
    public static final String HOSTNAME = System.getProperty("DATABASE_HOSTNAME");
    public static final String PORTNUMBER = System.getProperty("DATABASE_PORTNUMBER");
    public static final String DATABASE_NAME = System.getProperty("DATABASE_NAME");
    public static final String AOIS_USERNAME = System.getProperty("AOIS_USERNAME");
    public static final String AOIS_PASSWORD = System.getProperty("AOIS_PASSWORD");
    public static final String CPIS_USERNAME = System.getProperty("CPIS_USERNAME");
    public static final String CPIS_PASSWORD = System.getProperty("CPIS_PASSWORD");
    public static final String API_KEY = "apikey";   
    public static final String BASE_URI_APIGEE = System.getProperty("BASE_URI_APIGEE");
    public static final String APIGEE_CS_SERVICES_FEATURE_APIKEY = System.getProperty("APIGEE_CS_SERVICES_FEATURE_APIKEY");
    public static final String APIGEE_CS_SERVICES_PACKAGE_APIKEY = System.getProperty("APIGEE_CS_SERVICES_PACKAGE_APIKEY");
    public static final String APIGEE_CS_SERVICES_LICENSE_APIKEY = System.getProperty("APIGEE_CS_SERVICES_LICENSE_APIKEY");
    public static final String APIGEE_CS_SERVICES_TENANT_ID = System.getProperty("APIGEE_CS_SERVICES_TENANT_ID");
    public static final String XRAY_TICKET_SUMMARY = System.getProperty("XRAY_TICKET_SUMMARY");
    public static final String EXTENT_REPORTS_AUTHOR_NAME = System.getProperty("AUTHOR_NAME");
    public static final String ENIVRONMENT_RUN = System.getProperty("ENVIRONMENT");
    public static final String DEPLOYMENT_BUILD_VERSION = System.getProperty("BUILD_VERSION");
    public static final String EXTENT_FILE = "/src/main/resources/ExtentConfig.xml";    
    public static final String FILEBEAT_URL ="http://172.29.227.148/elk/api/console/proxy?path=/filebeat-6.5.4-";
    public static final String FILEBEAT_METHOD ="/doc/_search&method=POST";
      

}
