package com.tomtom.aomsv2.commonControls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.restassured.RestAssured;

public class Base {
    
    private static final Logger LOGGER = LoggerFactory.getLogger("commonControls.Base");

    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
        
    }
    
    
   @AfterMethod
      public void printline() throws InterruptedException {
          LOGGER.info("-------------------------------------------------------------------------------------------------------------------------------------------");
      }

}
