/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.commonControls;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

import com.google.gson.Gson;
import com.mongodb.util.JSON;
import com.tomtom.aomsv2.logresponse.Example;
import com.tomtom.aomsv2.logresponse.Hits;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.restassured.config.SSLConfig;
import io.restassured.response.Response;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

public class CommonMethods {
    private static final Logger LOGGER = LoggerFactory.getLogger("CommonMethods");

    public static Connection connectDBAoisSchema() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");

        String connectionURL = "jdbc:postgresql://" + CommonData.HOSTNAME + ':' + CommonData.PORTNUMBER + '/'
                + CommonData.DATABASE_NAME;

        Connection conn = DriverManager
                .getConnection(connectionURL, CommonData.AOIS_USERNAME, CommonData.AOIS_PASSWORD);
        return conn;
    }

    public static Connection connectDBCpisSchema() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        String connectionURL = "jdbc:postgresql://" + CommonData.HOSTNAME + ':' + CommonData.PORTNUMBER + '/'
                + CommonData.DATABASE_NAME;
        LOGGER.info("password is " + CommonData.CPIS_PASSWORD);
        Connection conn = DriverManager
                .getConnection(connectionURL, CommonData.CPIS_USERNAME, CommonData.CPIS_PASSWORD);
        return conn;

    }

    public static Connection connectDBAoisSchemaWithPreprodAPIGEE() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        String connectionURL = "jdbc:postgresql://" + CommonData.HOSTNAME + ':' + CommonData.PORTNUMBER + '/'
                + CommonData.DATABASE_NAME;
        Connection conn = DriverManager
                .getConnection(connectionURL, CommonData.AOIS_USERNAME, CommonData.AOIS_PASSWORD);
        return conn;
    }

    public static Connection connectDBCpisSchemaWithPreprodAPIGEE() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        String connectionURL = "jdbc:postgresql://" + CommonData.HOSTNAME + ':' + CommonData.PORTNUMBER + '/'
                + CommonData.DATABASE_NAME;
        Connection conn = DriverManager
                .getConnection(connectionURL, CommonData.CPIS_USERNAME, CommonData.CPIS_PASSWORD);
        return conn;
    }

    /**
     * @return SSLConfig object
     */
    @SuppressWarnings("deprecation")
    public static SSLConfig getSSLConfig() {
        String clientPassword = "changeit";
        String clientCertificatePath = CommonData.USER_DIRECTORY+CommonData.APIGEE_SECURITY_CERTIFICATE_PATH;

        KeyStore clientStore;
        org.apache.http.conn.ssl.SSLSocketFactory lSchemeSocketFactory = null;
        try {
            clientStore = KeyStore.getInstance("PKCS12");
            clientStore.load(new FileInputStream(clientCertificatePath), clientPassword.toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(clientStore, clientPassword.toCharArray());
            KeyManager[] kms = kmf.getKeyManagers();

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kms, null, new SecureRandom());
            lSchemeSocketFactory = new org.apache.http.conn.ssl.SSLSocketFactory(sslContext);
        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException | KeyManagementException
                | CertificateException | IOException e) {
            LOGGER.error("Error while loading the client certificate" + e.getMessage());
        }

        return SSLConfig.sslConfig().with().sslSocketFactory(lSchemeSocketFactory).and().allowAllHostnames();
        //return SSLConfig.sslConfig().with().keystoreType("PKCS12").keyStore(new File(clientCertificatePath), clientPassword).and().allowAllHostnames();

    }

    public static void IsInValidJSONFormat(Response response) {
        try {
            JSON.parse(response.andReturn().body().asString());
        } catch (Exception e) {
            LOGGER.info("Response JSON is not in valid format");
            fail();
        }
    }

    public static void createJSONForInfoFile() throws IOException {

        String reqbody = "{\"fields\":{\"project\":{\"id\": \"12671\"},\"summary\":"
                + "\""
                + CommonData.XRAY_TICKET_SUMMARY
                + "\""
                + ",\"issuetype\":{\"id\": \"10602\"},\"customfield_10390\": {\"value\":\"AOMS\"},\"components\" :[{\"name\":\"CSS Add-ons Management Service\"}]}}";
        writeToFile(reqbody);

    }

    public static void writeToFile(String data) {
        OutputStream os = null;
        try {
            os = new FileOutputStream(new File(CommonData.INFO_JSON_PATH));
            os.write(data.getBytes(), 0, data.length());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void verifyLogsfromKibana(String URLParameters)
            throws UnrecoverableKeyException, KeyManagementException, MalformedURLException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, IOException {
        BufferedReader br = null;
        HttpURLConnection con = null;
        try {

            String urlParameters = URLParameters;
            LOGGER.info("The search query is: " + urlParameters);
            DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
            Date date = new Date();
            LOGGER.info("Date set in the filebeat URL is " + dateFormat.format(date));
            String dateToBeSet = dateFormat.format(date);
            String url_string = CommonData.FILEBEAT_URL + dateToBeSet
                    + CommonData.FILEBEAT_METHOD;

            URL url = new URL(url_string);
            URLEncoder.encode(url_string, "UTF-8");
            LOGGER.info("The URL set is: " + url);
            con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.addRequestProperty("Authorization", "Basic dXNlcjpPeEoya0ZXMXZISlE=");
            con.addRequestProperty("kbn-xsrf", "reporting");

            OutputStream outStream = con.getOutputStream();
            OutputStreamWriter wr = new OutputStreamWriter(outStream, "UTF-8");

            wr.write(urlParameters);
            Thread.sleep(200);
            wr.flush();
            wr.close();
            outStream.close();
            con.connect();

            InputStream jsonform = con.getInputStream();

            br = new BufferedReader(new InputStreamReader(jsonform));

            String input = null;

            while ((input = br.readLine()) != null) {

                LOGGER.info("The result from Kibana in JSON format is:" + input);
                Gson gson = new Gson();
                Example fp = gson.fromJson(input, Example.class);
                Hits ht = new Hits();
                ht = fp.getHits();
                int count = ht.getTotal();
                LOGGER.info("The total count of hits is: " + count);
                assertEquals(count, 1);
                LOGGER.info(" \" Bad Request \" Log Verified");

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            br.close();
            con.disconnect();
        }

    }
    
    
    

}
