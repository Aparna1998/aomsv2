/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.commonControls;

public class Messages {

    // Add Feature Messages
    public static final String ADD_FEATURE_CREATED = "HTTP/1.1 204 Feature is created successfully";
    public static final String ADD_FEATURE_MALFORMED_REQUEST = "HTTP/1.1 400 Bad Request";
    public static final String ADD_FEATURE_CLIENT_UNAUTHORIZED = "HTTP/1.1 403 Client Unauthorized";
    public static final String ADD_FEATURE_ID_ALREADY_PRESENT = "HTTP/1.1 409 Feature with the given ID already exists";

    // Add Package Messages
    public static final String ADD_PACKAGE_CREATED = "HTTP/1.1 204 Package is created successfully";
    public static final String ADD_PACKAGE_MALFORMED_REQUEST = "HTTP/1.1 400 Bad Request";
    public static final String ADD_PACKAGE_FEATURE_ID_NOT_FOUND = "HTTP/1.1 404 No such feature could be found";
    public static final String ADD_PACKAGE_ALREADY_EXISTS = "HTTP/1.1 409 Package with the given ID already exists";
    public static final String ADD_PACKAGE_CLIENT_UNAUTHORIZED = "HTTP/1.1 403 Client Unauthorized";

    // Update Package Messages
    public static final String UPDATE_PACKAGE_ALREADY_EXISTS = "HTTP/1.1 204 Package is updated successfully";
    public static final String UPDATE_PACKAGE_MALFORMED_REQUEST = "HTTP/1.1 400 Bad Request";
    public static final String UPDATE_PACKAGE_ALREADY_IN_SAME_STATE = "HTTP/1.1 409 Package already in the same state";
    public static final String UPDATE_PACKAGE_CLIENT_UNAUTHORIZED = "HTTP/1.1 403 Client Unauthorized";

    // Fetch Feature Messages
    public static final String FETCH_FEATURE_SUCCESS = "HTTP/1.1 200 OK";
    public static final String FETCH_FEATURE_PACKAGE_NOT_FOUND = "HTTP/1.1 404 No such package could be found";
    public static final String FETCH_FEATURE_MALFORMED_REQUEST = "HTTP/1.1 400 Bad Request";
    public static final String FETCH_FEATURE_CLIENT_UNAUTHORIZED = "HTTP/1.1 403 Client Unauthorized";

    // Fetch Package Messages
    public static final String FETCH_PACKAGE_SUCCESS = "HTTP/1.1 200 OK";
    public static final String FETCH_PACKAGE_MALFORMED_REQUEST = "HTTP/1.1 400 Bad Request";
    public static final String FETCH_PACKAGE_CLIENT_UNAUTHORIZED = "HTTP/1.1 403 Client Unauthorized";

    // Fetch License Messages
    public static final String FETCH_LICENSE_SUCCESS = "HTTP/1.1 200 OK";
    public static final String FETCH_LICENSE_NO_PACKAGE_FOUND = "HTTP/1.1 409 No such package could be found";
    public static final String FETCH_LICENSE_MALFORMED_REQUEST = "HTTP/1.1 400 Bad Request";
    public static final String FETCH_LICENSE_CLIENT_UNAUTHORIZED = "HTTP/1.1 403 Client Unauthorized";

    // Delete Package Messages
    public static final String DELETE_PACKAGE_SUCCESS = "HTTP/1.1 204 Package is deleted successfully";
    public static final String DELETE_PACKAGE_BAD_REQUEST = "HTTP/1.1 400 Bad Request";
    public static final String DELETE_PACKAGE_CLIENT_UNAUTHORIZED = "HTTP/1.1 403 Client Unauthorized";

    // Delete Feature Messages
    public static final String DELETE_FEATURE_SUCCESS = "HTTP/1.1 204 Feature is deleted successfully";
    public static final String DELETE_FEATURE_BAD_REQUEST = "HTTP/1.1 400 Bad Request";
    public static final String DELETE_FEATURE_CLIENT_UNAUTHORIZED = "HTTP/1.1 403 Client Unauthorized";
}
