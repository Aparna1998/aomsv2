/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.updatePackage;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.commonControls.Base;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.commonControls.Messages;
import com.tomtom.aomsv2.commonControls.ResponseCodes;
import com.tomtom.aomsv2.request.entity.UpdatePackage;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * This class has the common methods for Update Package API which are referenced by all other test cases.
 * 
 * @author palvadi
 */
public class UpdatePackageUtils extends Base {

    private static final Logger LOGGER = LoggerFactory.getLogger("UpdatePackage.UpdatePackageUtils");

    /*
     * Variables defined for corresponding row numbers present in Add Feature Test data sheet
     */
    public static final int PACKAGE_ID_IN_USE = 1;
    public static final int PACKAGE_ID_OUT_OF_USE = 2;
    public static final int PACKAGE_ID_DEPRECATED = 3;
    public static final int STATUS_IS_INVALID = 4;
    public static final int PACKAGE_ID_NOT_IN_DATABASE = 5;
    public static final int PACKAGE_ID_GREATER_THAN_256_CHARACTERS = 6;
    public static final int PACKAGE_ID_IS_255_CHARACTERS = 7;
    public static final int PACKAGE_DOESNT_BELONG_TO_TENANT = 8;
    public static final int TENANT_ID_GREATER_THAN_256_CHARACTERS = 9;
    public static final int TENANT_ID_HAVING_SPECIAL_CHARACTERS = 12;
    public static final int TENANT_ID_IS_255_CHARACTERS = 11;
    public static final int X_REQUEST_ID_GREATER_THAN_256_CHARACTERS = 12;
    public static final int X_REQUEST_ID_IS_255_CHARACTERS = 13;
    public static final int TENANT_ID_IS_MISSING = 16;
    public static final int X_REQUEST_ID_IS_MISSING = 17;
    public static final int STATUS_IS_MISSING = 18;
    public static final int PACKAGE_ID_IS_MISSING = 19;
    public static final int X_REQUEST_ID_IS_256_CHARACTERS = 18;
    public static final int TENANT_ID_IS_256_CHARACTERS = 19;
    public static final int PACKAGE_ID_IS_256_CHARACTERS = 20;

    public static final int MULTIPLE_PACKAGE_INUSE_PACKAGE1 = 22;
    public static final int MULTIPLE_PACKAGE_INUSE_PACKAGE2 = 23;
    public static final int MULTIPLE_PACKAGE_INUSE_PACKAGE3 = 24;
    public static final int MULTIPLE_PACKAGE_INUSE_PACKAGE4 = 25;
    public static final int FEATURE_ID_NOT_LINKED_TO_PACKAGE_ID = 27;
    public static final int FEATURE_ID_NOT_IN_DATABASE = 28;
    public static final int E2E_SCENARIO = 29;
    public static final int PACKAGE_ID_AND_FEATURE_ID_PRESENT_AND_NOT_LINKED_TO_TENANT = 30;
    public static final int PACKAGE_ID_AND_FEATURE_ID_PRESENT_AND_FEATURE_ID_NOT_LINKED_TO_TENANT = 31;
    /* Variables defined for test data Hash Map of the method 'testData' */
    public static final String PACKAGE_ID = "PackageId";
    public static final String STATUS = "Status";
    public static final String TENANT_ID = "TenantId";
    public static final String X_REQUEST_ID = "XRequestId";
    public static final String FEATURE_ID = "FeatureId";
    public static String packageId;
    public static String packageIdStr = null;

    public static final String SUCCESSRESPONSE = "successresponse";
    public static final String MALFORMEDRESPONSE = "malformedresponse";
    public static final String CLIENTUNAUTHORIZED = "clientunauthorized";
    public static final String FEATUREIDALREADYPRESENT = "featurealreadypresent";
    public static Object HTTPSTATUSCODE = "HTTPSTATUSCODE";
    public static Object HTTPSTATUSMESSAGE = "HTTPSTATUSMESSAGE";
    public static final Object LOGFILENAME = "/var/log/aomsv2/UpdatePackage.log";
    public static Object CATEGORY = "CATEGORY";

    @Test
    public static HashMap<String, String> testData(int rownum) throws IOException {
        /* This method takes the testdata from TestData Prerequisite excel and puts in Hash Map*/
        HashMap<String, String> hm = new HashMap<String, String>();

        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream("testData/UpdatePackage.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("UpdatePackage");
        XSSFRow currentRow = testdatasheet.getRow(rownum);

        XSSFCell packageId = currentRow.getCell(1);
        String packageIdStr = packageId.toString();
        hm.put(PACKAGE_ID, packageIdStr);

        XSSFCell featureId = currentRow.getCell(2);
        String featureIdStr = featureId.toString();
        hm.put(FEATURE_ID, featureIdStr);

        XSSFCell status = currentRow.getCell(3);
        String packageStatus = status.toString();
        hm.put(STATUS, packageStatus);

        XSSFCell tenantid = currentRow.getCell(4);
        String tenantIdStr = tenantid.toString();
        hm.put(TENANT_ID, tenantIdStr);

        XSSFCell xRequestId = currentRow.getCell(5);
        String xRequestIdStr = xRequestId.toString();
        if (xRequestIdStr.equals("LengthGreaterThan256Characters")) {
            hm.put(X_REQUEST_ID,
                    "hsafdhsfdjgsagfhsagfhsagdhfgsahfghjsagfhagdshfgasfhsagfhagsfghsagfhjgdsfdsgdshagdgfhjagdshfghjagsdhfgafhgdsafgdsahfgdsafhdsgfhgsddhfgafgdsfdshfgasdhjsssssssssssssssssssssssssssssssssssssssiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiieeeeeedddd");
        } else {
            Random rd = new Random();
            int randInt1 = rd.nextInt(100000);
            String sALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder salt = new StringBuilder();
            Random rnd = new Random();
            while (salt.length() < 18) {
                int index = (int) (rnd.nextFloat() * sALTCHARS.length());
                salt.append(sALTCHARS.charAt(index));
            }
            String saltStr = salt.toString();
            String xRequestId_random = randInt1 + saltStr;
            hm.put(X_REQUEST_ID, xRequestId_random);
        }
        fis.close();
        wb.close();
        return hm;
    }

    public static String getPackageId(HashMap<String, String> hm) {

        String PackageId = hm.get(UpdatePackageUtils.PACKAGE_ID);
        return PackageId;

    }

    public static String getXRequestId(HashMap<String, String> hm) {

        String XRequestId = hm.get(UpdatePackageUtils.X_REQUEST_ID);
        return XRequestId;

    }

    public static Response updatePackageAPI(HashMap<String, String> hhm, boolean apigeeFlag) {
        Map<String, String> testdata = new HashMap<String, String>();
        testdata = hhm;
        UpdatePackage requestdata = new UpdatePackage();
        requestdata.setPackageId(hhm.get(PACKAGE_ID));
        requestdata.setFeatureId(hhm.get(FEATURE_ID));
        requestdata.setState(hhm.get(STATUS));
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';

        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().post(CommonData.UPDATE_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata.get(UpdatePackageUtils.TENANT_ID))
                    .header("X-Request-Id", testdata.get(UpdatePackageUtils.X_REQUEST_ID)).when()
                    .post(CommonData.UPDATE_PACKAGE);

            LOGGER.debug("The TenantId is " + testdata.get(UpdatePackageUtils.TENANT_ID));
            LOGGER.debug("The X-Request-Id is " + testdata.get(UpdatePackageUtils.X_REQUEST_ID));
        }

        LOGGER.debug("The request body is " + requestbody);

        return response;
    }

    public static Response updatePackageAPIWithOutFeatureId(HashMap<String, String> hhm, boolean apigeeFlag) {
        Map<String, String> testdata = new HashMap<String, String>();
        testdata = hhm;
        UpdatePackage requestdata = new UpdatePackage();
        requestdata.setPackageId(hhm.get(PACKAGE_ID));
        requestdata.setState(hhm.get(STATUS));
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';

        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().post(CommonData.UPDATE_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata.get(UpdatePackageUtils.TENANT_ID))
                    .header("X-Request-Id", testdata.get(UpdatePackageUtils.X_REQUEST_ID)).when()
                    .post(CommonData.UPDATE_PACKAGE);
            LOGGER.debug("The TenantId is " + testdata.get(UpdatePackageUtils.TENANT_ID));
            LOGGER.debug("The X-Request-Id is " + testdata.get(UpdatePackageUtils.X_REQUEST_ID));
        }
        LOGGER.debug("The request body is " + requestbody);

        return response;
    }

    public static Response updatePackageRequestHavingOnlyPackageId(HashMap<String, String> hhm, boolean apigeeFlag) {
        Map<String, String> testdata = new HashMap<String, String>();
        testdata = hhm;
        UpdatePackage requestdata = new UpdatePackage();
        requestdata.setPackageId(hhm.get(PACKAGE_ID));
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';

        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().post(CommonData.UPDATE_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata.get(UpdatePackageUtils.TENANT_ID))
                    .header("X-Request-Id", testdata.get(UpdatePackageUtils.X_REQUEST_ID)).when()
                    .post(CommonData.UPDATE_PACKAGE);
            LOGGER.debug("The TenantId is " + testdata.get(UpdatePackageUtils.TENANT_ID));
            LOGGER.debug("The X-Request-Id is " + testdata.get(UpdatePackageUtils.X_REQUEST_ID));
        }

        LOGGER.debug("The request body is " + requestbody);

        return response;
    }

    public static Response updatePackageRequestHavingOnlyPackageIdAndFeatureId(HashMap<String, String> hhm,
            boolean apigeeFlag) {
        Map<String, String> testdata = new HashMap<String, String>();
        testdata = hhm;
        UpdatePackage requestdata = new UpdatePackage();
        requestdata.setPackageId(hhm.get(PACKAGE_ID));
        requestdata.setFeatureId(hhm.get(FEATURE_ID));
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';

        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().post(CommonData.UPDATE_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata.get(UpdatePackageUtils.TENANT_ID))
                    .header("X-Request-Id", testdata.get(UpdatePackageUtils.X_REQUEST_ID)).when()
                    .post(CommonData.UPDATE_PACKAGE);
            LOGGER.debug("The TenantId is " + testdata.get(UpdatePackageUtils.TENANT_ID));
            LOGGER.debug("The X-Request-Id is " + testdata.get(UpdatePackageUtils.X_REQUEST_ID));
        }

        LOGGER.debug("The request body is " + requestbody);

        return response;
    }

    public static String jsonMapping(UpdatePackage requestdata) {
        List<JSONObject> list;
        JSONObject jsonobject = new JSONObject(requestdata);
        list = new ArrayList<JSONObject>();
        list.add(jsonobject);
        String reqbody = list.toString();
        int endIndex = reqbody.length();
        String requestbody = reqbody.substring(1, endIndex - 1);

        return requestbody;
    }

    public static String getPackageId(String status) throws IOException, NullPointerException {
        if (status == "In-use") {
            FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY
                    + CommonData.UPDATE_PACKAGE_TEST_DATA_PATH);
            XSSFWorkbook wb = new XSSFWorkbook(fis);
            XSSFSheet testdatasheet = wb.getSheet("UpdatePackage");
            XSSFRow currentRow = testdatasheet.getRow(1);
            XSSFCell packageId = currentRow.getCell(1);
            packageIdStr = packageId.toString();
            LOGGER.debug("The Package Id set is " + packageIdStr);
            wb.close();
        }

        else if (status == "Deprecated") {
            FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY
                    + CommonData.UPDATE_PACKAGE_TEST_DATA_PATH);
            XSSFWorkbook wb = new XSSFWorkbook(fis);
            XSSFSheet testdatasheet = wb.getSheet("UpdatePackage");
            XSSFRow currentRow = testdatasheet.getRow(2);
            XSSFCell packageId = currentRow.getCell(1);
            packageIdStr = packageId.toString();
            LOGGER.debug("The Package Id set is " + packageIdStr);
            wb.close();
        }

        else if (status == "Out-of-use") {
            FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY
                    + CommonData.UPDATE_PACKAGE_TEST_DATA_PATH);
            XSSFWorkbook wb = new XSSFWorkbook(fis);
            XSSFSheet testdatasheet = wb.getSheet("UpdatePackage");
            XSSFRow currentRow = testdatasheet.getRow(3);
            XSSFCell packageId = currentRow.getCell(1);
            packageIdStr = packageId.toString();
            LOGGER.debug("The Package Id set is " + packageIdStr);
            wb.close();
        }

        return packageIdStr;
    }

    public static void updatePackage(String PackageId, String status, boolean apigeeFlag) throws IOException,
            NullPointerException {
        HashMap<String, String> testdata = testData(UpdatePackageUtils.PACKAGE_ID_IN_USE);

        UpdatePackage requestdata = new UpdatePackage();
        requestdata.setPackageId(PackageId);
        requestdata.setState(status);
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().post(CommonData.UPDATE_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata.get(UpdatePackageUtils.TENANT_ID))
                    .header("X-Request-Id", testdata.get(UpdatePackageUtils.X_REQUEST_ID)).when()
                    .post(CommonData.UPDATE_PACKAGE);
        }
        LOGGER.debug("The request body is " + requestbody);
        LOGGER.debug("The TenantId is " + testdata.get(UpdatePackageUtils.TENANT_ID));
        LOGGER.debug("The X-Request-Id is " + testdata.get(UpdatePackageUtils.X_REQUEST_ID));

        validateSuccessResponse(response);
    }

    public static Response updatePackageRequestBodyIsEmpty(boolean apigeeFlag) throws IOException, NullPointerException {
        HashMap<String, String> testdata = testData(UpdatePackageUtils.PACKAGE_ID_IN_USE);
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body("").when()
                    .post(CommonData.UPDATE_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body("")
                    .header("X-TENANT", testdata.get(UpdatePackageUtils.TENANT_ID))
                    .header("X-Request-Id", testdata.get(UpdatePackageUtils.X_REQUEST_ID)).when()
                    .post(CommonData.UPDATE_PACKAGE);
            LOGGER.debug("The TenantId is " + testdata.get(UpdatePackageUtils.TENANT_ID));
            LOGGER.debug("The X-Request-Id is " + testdata.get(UpdatePackageUtils.X_REQUEST_ID));
        }
        LOGGER.debug("The request body is empty ");

        return response;
    }

    public static Response updatePackageWhenHeadersAreEmpty() throws IOException, NullPointerException {
        HashMap<String, String> testdata = testData(UpdatePackageUtils.PACKAGE_ID_IN_USE);

        UpdatePackage requestdata = new UpdatePackage();
        requestdata.setPackageId(testdata.get(UpdatePackageUtils.PACKAGE_ID));
        requestdata.setState(testdata.get(UpdatePackageUtils.FEATURE_ID));
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';
        Response response = given().relaxedHTTPSValidation().body(requestbody).when().post(CommonData.UPDATE_PACKAGE);
        LOGGER.debug("The request body is " + requestbody);
        LOGGER.debug("The TenantId is not set");
        LOGGER.debug("The X-Request-Id is not set");

        return response;

    }

    public static void updatePackageParameterDuplication(String PackageId, String status, boolean apigeeFlag)
            throws IOException, NullPointerException {
        HashMap<String, String> testdata = testData(UpdatePackageUtils.PACKAGE_ID_IN_USE);

        UpdatePackage requestdata = new UpdatePackage();
        requestdata.setPackageId(PackageId);
        requestdata.setState(status);
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().post(CommonData.UPDATE_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata.get(UpdatePackageUtils.TENANT_ID))
                    .header("X-Request-Id", testdata.get(UpdatePackageUtils.X_REQUEST_ID)).when()
                    .post(CommonData.UPDATE_PACKAGE);
            LOGGER.debug("The TenantId is " + testdata.get(UpdatePackageUtils.TENANT_ID));
            LOGGER.debug("The X-Request-Id is " + testdata.get(UpdatePackageUtils.X_REQUEST_ID));
        }
        LOGGER.debug("The request body is " + requestbody);

        validateSuccessResponse(response);
    }

    public static void updatePackageToSameStatus(String PackageId, String status, boolean apigeeFlag)
            throws IOException, NullPointerException {
        HashMap<String, String> testdata = testData(UpdatePackageUtils.PACKAGE_ID_IN_USE);

        UpdatePackage requestdata = new UpdatePackage();
        requestdata.setPackageId(PackageId);
        requestdata.setState(status);
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';

        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().post(CommonData.UPDATE_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata.get(UpdatePackageUtils.TENANT_ID))
                    .header("X-Request-Id", testdata.get(UpdatePackageUtils.X_REQUEST_ID)).when()
                    .post(CommonData.UPDATE_PACKAGE);
            LOGGER.debug("The TenantId is " + testdata.get(UpdatePackageUtils.TENANT_ID));
            LOGGER.debug("The X-Request-Id is " + testdata.get(UpdatePackageUtils.X_REQUEST_ID));
        }
        LOGGER.debug("The request body is " + requestbody);

        validatePackageAlreadyInSameState(response);
    }

    public static String setPackageId(String sheetname, int RowNumber, int cellNumber) throws IOException,
            NullPointerException {
        FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY + CommonData.UPDATE_PACKAGE_TEST_DATA_PATH);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet(sheetname);
        XSSFRow currentRow = testdatasheet.getRow(RowNumber);
        XSSFCell packageId = currentRow.getCell(cellNumber);
        String packageIdStr = packageId.toString();
        wb.close();
        return packageIdStr;
    }

    public static String setStatus(String sheetname, int RowNumber, int cellNumber) throws IOException,
            NullPointerException {
        FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY + CommonData.UPDATE_PACKAGE_TEST_DATA_PATH);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet(sheetname);
        XSSFRow currentRow = testdatasheet.getRow(RowNumber);
        XSSFCell statusCell = currentRow.getCell(cellNumber);
        String status = statusCell.toString();
        wb.close();
        return status;
    }

    public static void validateSuccessResponse(Response response) {
        response.then().assertThat().statusCode(ResponseCodes.UPDATE_PACKAGE_UPDATED_SUCCESSFULLY).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.UPDATE_PACKAGE_ALREADY_EXISTS);

        LOGGER.debug("The response is " + Messages.UPDATE_PACKAGE_ALREADY_EXISTS);

    }

    public static void validateMalformedRequestResponse(Response response) {
        response.then().assertThat().statusCode(ResponseCodes.UPDATE_PACKAGE_MALFORMED_REQUEST_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.UPDATE_PACKAGE_MALFORMED_REQUEST);

        LOGGER.debug("The response is " + Messages.UPDATE_PACKAGE_MALFORMED_REQUEST);
        LOGGER.debug("The content type is " + ContentType.JSON);
    }

    public static void validatePackageAlreadyInSameState(Response response) {
        response.then().assertThat().statusCode(ResponseCodes.UPADTE_PACKAGE_ALREADY_IN_SAME_STATE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat()
                .statusLine(Messages.UPDATE_PACKAGE_ALREADY_IN_SAME_STATE);

        LOGGER.debug("The response is " + Messages.UPDATE_PACKAGE_ALREADY_IN_SAME_STATE);
        LOGGER.debug("The content type is " + ContentType.JSON);
    }

    public static void validateClientUnauthorizedResponse(Response response) {
        response.then().assertThat().statusCode(ResponseCodes.UPDATE_PACKAGE_CLIENT_UNAUTHORIZED).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat()
                .statusLine(Messages.UPDATE_PACKAGE_CLIENT_UNAUTHORIZED);

        LOGGER.debug("The response is " + Messages.UPDATE_PACKAGE_ALREADY_IN_SAME_STATE);
        LOGGER.debug("The content type is " + ContentType.JSON);

    }
    
    public static void verifylogsforSuccessResponse(Object packageId, Object xRequestId) throws MalformedURLException,
    IOException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
    KeyStoreException, CertificateException {



try {

    HTTPSTATUSCODE = "HTTPSTATUSCODE: 204";
    HTTPSTATUSMESSAGE = "Package is updated successfully";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"" + packageId + "\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
            + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The URL parameters is " + urlParameters);
    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 


}

public static void verifylogsforBadRequestResponse(Object packageId, Object xRequestId)
    throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
    NoSuchAlgorithmException, KeyStoreException, CertificateException {

try {
    HTTPSTATUSCODE = "HTTPSTATUSCODE: 400";
    HTTPSTATUSMESSAGE = "Bad Request";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"Response Body:" + HTTPSTATUSCODE
            + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE + "\"}},{\"match\":{\"source\":\""
            + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The URL parameters is " + urlParameters);
    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 


}

public static void verifylogsforPackageAlreadyInSameStateResponse(Object packageId, Object xRequestId)
    throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
    NoSuchAlgorithmException, KeyStoreException, CertificateException {


try {

    HTTPSTATUSCODE = "HTTPSTATUSCODE: 409";
    HTTPSTATUSMESSAGE = "Package is already in same state";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"" + packageId + "\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
            + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The URL parameters is " + urlParameters);
    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 


}
    
    
}