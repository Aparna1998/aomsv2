/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.updatePackage;

import org.testng.annotations.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has different Miscellaneous test cases of Update Package API through ELB/Nodes.
 * 
 * @author palvadi
 */
public class MiscellaneousScenarios {

    private static final Logger LOGGER = LoggerFactory.getLogger("UpdatePackage.UpdatePackage_Miscellaneous");
    HashMap<String, String> testdata;


    @Test(groups = "UpdatePackage")
    public void updatePackageRequestHavingOnlyPackageIdinBody() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("********************Starting TestCase \"updatePackageRequestHavingOnlyPackageIdinBody\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.PACKAGE_ID_IN_USE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageId(testdata, false);
        UpdatePackageUtils.validateSuccessResponse(response);
        testdata= null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageRequestHavingOnlyPackageIdAndFeatureId() throws IOException, ClassNotFoundException,
            SQLException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("********************Starting TestCase \"updatePackageRequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.PACKAGE_ID_IN_USE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateSuccessResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageRequestHavingOnlyPackageIdAndFeatureIdAndFeatureIdNotLinkedtoPackage() throws IOException,
            ClassNotFoundException, SQLException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"updatePackageRequestHavingOnlyPackageIdAndFeatureIdAndFeatureIdNotLinkedtoPackage\"********************");

        testdata = UpdatePackageUtils
                .testData(UpdatePackageUtils.FEATURE_ID_NOT_LINKED_TO_PACKAGE_ID);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateSuccessResponse(response);
        testdata= null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageRequestHavingOnlyPackageIdAndFeatureIdAndFeatureIdNotinDatabase() throws IOException,
            ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"updatePackageRequestHavingOnlyPackageIdAndFeatureIdAndFeatureIdNotinDatabase\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackagePackageIdisNotPresent() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"updatePackagePackageIdisNotPresent\"");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.PACKAGE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

 
    @Test(groups = "UpdatePackage")
    public void updatePackagePackageIdandFeatureIdPresentAndNotLinkedtoTenant() throws IOException,
            ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"updatePackagePackageIdandFeatureIdPresentAndNotLinkedtoTenant\"");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.PACKAGE_ID_AND_FEATURE_ID_PRESENT_AND_NOT_LINKED_TO_TENANT);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackagePackageIdandFeatureIdPresentAndOnlyFeatureIdIsLinkedtoTenant() throws IOException,
            ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"updatePackagePackageIdandFeatureIdPresentAndOnlyFeatureIdIsLinkedtoTenant\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.PACKAGE_ID_AND_FEATURE_ID_PRESENT_AND_FEATURE_ID_NOT_LINKED_TO_TENANT);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;
    }

    @Test(groups = "UpdatePackage")
    public void updatePackagePackageIdandFeatureIdPresentAndOnlyPackageIdIsLinkedtoTenant() throws IOException,
            ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"updatePackagePackageIdandFeatureIdPresentAndOnlyPackageIdIsLinkedtoTenant\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.PACKAGE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;
    }

    @Test(groups = "UpdatePackage")
    public void updatePackageRequestHavingOnlyPackageIdAndState() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"updatePackageRequestHavingOnlyPackageIdAndState\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;
    }

    @Test(groups = "UpdatePackage")
    public void updatePackageMultipleValidRequestsAreSent() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"updatePackageMultipleValidRequestsAreSent\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;
    }

    @Test(groups = "UpdatePackage")
    public void updatePackageTwoRequestsWithSameDetails() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"updatePackageTwoRequestsWithSameDetails\"********************");
        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageTwoRequestsWhichDoesNotBelongToTenant() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"updatePackageTwoRequestsWhichDoesNotBelongToTenant\"********************");

        HashMap<String, String> testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;
    }

    @Test(groups = "UpdatePackage")
    public void updatePackageTwoRequestsOneLinkedtoFeatureAndOneNotLinkedToFeatureId() throws IOException,
            ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"UpdatePackage_RequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageTwoRequestsOneStatusIsValidOneStatusIsInvalid() throws IOException,
            ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"UpdatePackage_RequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageMultipleRequestWithInvalidRequestBody() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"UpdatePackage_RequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageMultipleRequestsWithOnlyStatesInBoth() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"UpdatePackage_RequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageMultipleRequestsWithOnlyPackageIdInBoth() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"UpdatePackage_RequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageMultipleRequestsWithOnlyFeatureIdInBoth() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"UpdatePackage_RequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageMultipleRequestsWithOnlyPackageIdAndFeatureIdInBoth() throws IOException,
            ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"UpdatePackage_RequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageMultipleRequestsWithOnlyPackageIdAndStateInBoth() throws IOException,
            ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"UpdatePackage_RequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageMultipleRequestsWithOnlyFeatureIdAndStateInBoth() throws IOException,
            ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"UpdatePackage_RequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "UpdatePackage")
    public void updatePackageMultiplePackageIdInOneRequest() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"UpdatePackage_RequestHavingOnlyPackageIdAndFeatureId\"********************");

        testdata = UpdatePackageUtils.testData(UpdatePackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = UpdatePackageUtils.updatePackageRequestHavingOnlyPackageIdAndFeatureId(testdata, false);
        UpdatePackageUtils.validateMalformedRequestResponse(response);
        testdata = null;

    }

}
