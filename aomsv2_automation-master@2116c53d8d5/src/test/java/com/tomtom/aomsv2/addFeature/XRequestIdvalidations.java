/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.addFeature;

import org.testng.annotations.Test;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.addFeatureXRequestValidations;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.fetchResponsefromDbandCompare;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.testData;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.verifyDataIsNotInserted;

import com.tomtom.aomsv2.commonControls.CommonData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for XRequestId field in Add Feature Request and verifying
 * the response through ELB/Nodes.
 * 
 * @author palvadi
 */

public class XRequestIdvalidations {
    
    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    
    
    private static final Logger LOGGER = LoggerFactory.getLogger("addFeature.AddFeatureXRequestIdvalidations");
    HashMap<String, String> testdata;
    
    @AfterMethod
    public void releaseResources()
    {
        testdata = null;
    }

    @Test(groups = "AddFeature")
    public void addFeatureWithXRequestIDof256characters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWithXRequestIDof256characters\"********************");

        testdata = testData(AddFeatureUtils.X_REQUEST_ID_OF_256_CHARACTERS);
        Response response = addFeatureXRequestValidations(testdata);
        AddFeatureUtils.validateSuccessResponse(response);
        fetchResponsefromDbandCompare(testdata, false);
    }

    @Test(groups = "AddFeature")
    public void addFeatureWithXRequestIDof255characters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWithXRequestIDof255characters\"********************");

        testdata = testData(AddFeatureUtils.X_REQUEST_ID_OF_255_CHARACTERS);
        Response response = addFeatureXRequestValidations(testdata);
        AddFeatureUtils.validateSuccessResponse(response);
        fetchResponsefromDbandCompare(testdata, false);
    }

    @Test(groups = "AddFeature")
    public void addFeatureWithXRequestIDgreaterthan256characters() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWithXRequestIDgreaterthan256characters\"********************");

        testdata = testData(AddFeatureUtils.X_REQUEST_ID_GREATER_THAN_256_CHARACTERS);
        Response response = addFeatureXRequestValidations(testdata);
        AddFeatureUtils.validateMalformedResponse(response);
        verifyDataIsNotInserted(testdata, false);
    }

    @Test(groups = "AddFeature")
    public void addFeatureWithXRequestIDHasSpecialCharacters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWithXRequestIDHasSpecialCharacters\"********************");

        testdata = testData(AddFeatureUtils.X_REQUEST_ID_HAS_SPECIAL_CHARACTERS);
        Response response = addFeatureXRequestValidations(testdata);
        AddFeatureUtils.validateSuccessResponse(response);
        fetchResponsefromDbandCompare(testdata, false);
    }

    @Test(groups = "AddFeature")
    public void addFeatureXRequestIdisMissing() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureXRequestIdisMissing\"********************");

        testdata = testData(AddFeatureUtils.TENANT_ID_GREATER_THAN_256_CHARACTERS);
        Response response = addFeatureXRequestValidations(testdata);
        AddFeatureUtils.validateMalformedResponse(response);
        verifyDataIsNotInserted(testdata, false);
    }

    @Test(groups = "AddFeature")
    public void addFeatureXRequestIdisEmpty() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureXRequestIdisEmpty\"********************");

        testdata = testData(AddFeatureUtils.TENANT_ID_GREATER_THAN_256_CHARACTERS);
        Response response = addFeatureXRequestValidations(testdata);
        AddFeatureUtils.validateMalformedResponse(response);
        verifyDataIsNotInserted(testdata, false);
    }

}
