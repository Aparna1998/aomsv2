/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.addFeature;

import org.testng.annotations.Test;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.addFeature;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.testData;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.validateMalformedResponse;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.validateSuccessResponse;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.verifyDataIsNotInserted;

import com.tomtom.aomsv2.commonControls.CommonData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for Description field for Add Feature API and verifying
 * the response through ELB/Nodes.
 * 
 * @author palvadi
 */

public class FeatureIdValidations {
    

    private static final Logger LOGGER = LoggerFactory.getLogger("addFeature.AddFeatureFeatureIdValidations");
    HashMap<String, String> testdata;

    @AfterMethod
    public void releaseResources()
    {
        testdata = null;
    }
    
    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    

    @Test(groups = "AddFeature")
    public void addFeatureWithFeatureIdof256characters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("******************Starting TestCase \"addFeatureWithFeatureIdof256characters\"********************");

        testdata = testData(AddFeatureUtils.FEATURE_ID_256_CHARACTERS);
        Response response = addFeature(testdata, false);
        validateSuccessResponse(response);
    }

    @Test(groups = "AddFeature")
    public void addFeatureWithFeatureIdof255characters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWithFeatureIdof255characters\"********************");

        testdata = testData(AddFeatureUtils.FEATURE_ID_255_CHARACTERS);
        Response response = addFeature(testdata, false);
        validateSuccessResponse(response);

    }

    @Test(groups = "AddFeature")
    public void addFeatureWithFeatureIdgreaterthan256characters() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWithFeatureIdgreaterthan256characters\"********************");

        testdata = testData(AddFeatureUtils.FEATURE_ID_GREATER_THAN_256_CHARACTERS);
        Response response = addFeature(testdata, false);
        validateMalformedResponse(response);
        verifyDataIsNotInserted(testdata, false);

    }

    @Test(groups = "AddFeature")
    public void addFeatureWithFeatureIdhasspecialcharacters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWithFeatureIdhasspecialcharacters\"********************");

        testdata = testData(AddFeatureUtils.FEATURE_ID_HAS_SPECIAL_CHARACTER);
        Response response = addFeature(testdata, false);
        AddFeatureUtils.validateSuccessResponse(response);

    }

    @Test(groups = "AddFeature")
    public void addFeatureWhenFeatureIdisEmpty() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWhenFeatureIdisEmpty\"********************");

        testdata = testData(AddFeatureUtils.FEATURE_ID_IS_EMPTY);
        Response response = addFeature(testdata, false);
        validateMalformedResponse(response);
        verifyDataIsNotInserted(testdata, false);

    }

    @Test(groups = "AddFeature")
    public void addFeatureWhenFeatureIdisNull() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWhenFeatureIdisNull\"********************");

        testdata = testData(AddFeatureUtils.FEATURE_ID_NULL);
        Response response = addFeature(testdata, false);
        validateMalformedResponse(response);
        verifyDataIsNotInserted(testdata, false);

    }

    @Test(groups = "AddFeature")
    public void addFeatureWhenFeatureIdisMissing() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWhenFeatureIdisMissing\"********************");

        testdata = testData(AddFeatureUtils.FEATURE_ID_NULL);
        Response response = addFeature(testdata, false);
        validateMalformedResponse(response);
        verifyDataIsNotInserted(testdata, false);

    }

    @Test(groups = "AddFeature")
    public void addFeatureIdAlreadyPresent() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureIdAlreadyPresent\"********************");

        testdata = testData(AddFeatureUtils.FEATURE_ALREADY_PRESENT);
        Response response = addFeature(testdata, false);
        AddFeatureUtils.validateFeatureIdAlreadyExistsResponse(response);
    }

}
