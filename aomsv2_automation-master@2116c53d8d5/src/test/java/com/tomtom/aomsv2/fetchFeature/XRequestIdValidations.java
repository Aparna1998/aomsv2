/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.fetchFeature;

import org.testng.annotations.Test;
import com.tomtom.aomsv2.commonControls.CommonData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations of XRequest Id field for Fetch Feature API and verifying
 * the response through ELB/Nodes.
 * 
 * @author palvadi
 */
public class XRequestIdValidations {

    private static final Logger LOGGER = LoggerFactory.getLogger("FetchFeature.FetchFeature");
    HashMap<String, String> testdata;

    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    
    
    @Test(groups = "FetchFeature")
    public void xRequestis256Characters() throws IOException, SQLException, ClassNotFoundException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException, InterruptedException {
        LOGGER.info("********************Starting TestCase \"xRequestis256Characters\"********************");

        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.X_REQUEST_ID_256_CHARACTERS);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateSuccessResponse(response);
        FetchFeatureUtils.checkDatabaseAndCompare(testdata, false);
        testdata = null;
    }

    @Test(groups = "FetchFeature")
    public void xRequestIdis257Characters() throws IOException, SQLException, ClassNotFoundException {
        LOGGER.info("********************Starting TestCase \"xRequestIdis257Characters\"********************");


        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.X_REQUEST_ID_257_CHARACTERS);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateMalformedResponse(response);
        testdata = null;
    }

    @Test(groups = "FetchFeature")
    public void xRequestIdisNull() throws IOException, SQLException, ClassNotFoundException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            InterruptedException {
        LOGGER.info("********************Starting TestCase \"xRequestIdisNull\"********************");

        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.X_REQUEST_ID_IS_NULL);
        Response response = FetchFeatureUtils.getResponseXRequestValidations(testdata);
        FetchFeatureUtils.validateSuccessResponse(response);
        FetchFeatureUtils.checkDatabaseAndCompare(testdata, false);
        testdata = null;
    }

    @Test(groups = "FetchFeature")
    public void xRequestIdisNullAsString() throws IOException, SQLException, ClassNotFoundException,
            InterruptedException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException {
        LOGGER.info("********************Starting TestCase \"xRequestIdisNullAsString\"********************");

        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.X_REQUEST_ID_IS_NULL_AS_STRING);
        Response response = FetchFeatureUtils.getResponseXRequestValidations(testdata);
        FetchFeatureUtils.validateSuccessResponse(response);
        FetchFeatureUtils.checkDatabaseAndCompare(testdata, false);
        testdata = null;
    }

    @Test(groups = "FetchFeature")
    public void XrequestIdisRepeated() throws IOException, SQLException, ClassNotFoundException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException, InterruptedException {
        LOGGER.info("********************Starting TestCase \"XrequestIdisRepeated\"********************");

        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.VALID_DATA);
        Response response = FetchFeatureUtils.getResponseInputParametersRepeated(testdata, "XREQUESTIDREPEATED", false);
        FetchFeatureUtils.validateMalformedResponse(response);
        testdata = null;
    }

    @Test(groups = "FetchFeature")
    public void xRequestIdisMissing() throws IOException, SQLException, ClassNotFoundException {
        LOGGER.info("********************Starting TestCase \"xRequestIdisMissing\"********************");

        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.X_REQUEST_ID_IS_MISSING);
        Response response = FetchFeatureUtils.getResponseXRequestValidations(testdata);
        FetchFeatureUtils.validateSuccessResponse(response);
        FetchFeatureUtils.checkDatabaseAndCompare(testdata, false);
        FetchFeatureUtils.VerifyResponseCountforSingleResponse(response);
        testdata = null;
    }

}
