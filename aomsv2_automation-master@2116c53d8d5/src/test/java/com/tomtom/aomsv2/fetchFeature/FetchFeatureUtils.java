/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.fetchFeature;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertNotEquals;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.commonControls.Base;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.commonControls.Messages;
import com.tomtom.aomsv2.commonControls.ResponseCodes;
import com.tomtom.aomsv2.request.entity.AddFeatureRequestNew;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * This class has the common methods for Fetch Feature API which are referenced by all other test cases.
 * 
 * @author palvadi
 */
public class FetchFeatureUtils extends Base{

    private static final Logger LOGGER = LoggerFactory.getLogger("FetchFeature.Utils");

    /*
     * Variables defined for corresponding row numbers present in Add Feature Test data sheet
     */
    public static final int VALID_DATA = 1;
    public static final int FEATURE_ID_NOT_IN_DATABASE = 2;
    public static final int FEATURE_ID_256_CHARACTERS_NOT_IN_DATABASE = 3;
    public static final int FEATURE_ID_257_CHARACTERS = 4;
    public static final int FEATURE_ID_255_CHARACTERS_NOT_IN_DATABASE = 5;
    public static final int FEATURE_ID_256_CHARACTERS_PRESENT_IN_DATABASE = 6;
    public static final int FEATURE_ID_255_CHARACTERS_PRESENT_IN_DATABASE = 7;
    public static final int X_REQUEST_ID_256_CHARACTERS = 8;
    public static final int X_REQUEST_ID_257_CHARACTERS = 9;
    public static final int TENANT_ID_256_CHARACTERS = 10;
    public static final int TENANT_ID_255_CHARACTERS = 11;
    public static final int TENANT_ID_257_CHARACTERS = 12;
    public static final int FEATURE_ID_IS_NULL = 13;
    public static final int TENANT_NOT_IN_DATABASE = 14;
    public static final int TENANT_ID_IS_NULL = 15;
    public static final int X_REQUEST_ID_IS_NULL = 16;
    public static final int FEATURE_ID_NOT_MAPPED_WITH_TENANT = 17;
    public static final int FEATURE_ID_IS_MISSING = 18;
    public static final int FEATURE_ID_IS_NULL_AS_STRING = 19;
    public static final int X_REQUEST_ID_IS_NULL_AS_STRING = 20;
    public static final int X_REQUEST_ID_IS_MISSING = 21;
    public static final int X_TENANT_ID_IS_MISSING = 22;
    public static final int X_TENANT_ID_IS_NULL_AS_STRING = 23;
    public static final int E2E_FEATURE_ID = 24;
    public static final int PACKAGE_CHECKING_FEATURE_ID_IS_NOT_DELETED = 25;
    /* Variables defined for test data Hash Map of the method 'testData' */
    public static final String FEATURE_ID_EXCEL = "Featureid_Excel";
    public static final String TENANT_ID_EXCEL = "TenantId_Excel";
    public static final String X_REQUEST_ID_EXCEL = "X_requestId_Excel";
    public static final String FEATURE_ID_DATABASE = "feature_id_database";
    public static final String DESCRIPTION_DATABASE = "description_database";
    public static final String DB_FEATURE_ID = "DB_FEATURE_ID";
    public static final String DB_TENANT_ID = "DB_TENANT_ID";
    public static final String DB_DESCRIPTION = "DB_DESCRIPTION";
    public static final String FEATURE_ID_JSONRESPONSE = "FEATURE_ID_JSONRESPONSE";
    public static final String DESCRIPTION_JSONRESPONSE = "DESCRIPTION_JSONRESPONSE";

    public static final String SUCCESSRESPONSE = "successresponse";
    public static final String MALFORMEDRESPONSE = "malformedresponse";
    public static final String CLIENTUNAUTHORIZED = "clientunauthorized";
    public static final String FEATUREIDALREADYPRESENT = "featurealreadypresent";
    public static Object HTTPSTATUSCODE = "HTTPSTATUSCODE";
    public static Object HTTPSTATUSMESSAGE = "HTTPSTATUSMESSAGE";
    public static final Object LOGFILENAME = "/var/log/aomsv2/FetchFeature.log";
    public static Object CATEGORY = "CATEGORY";

    public static Response response;

    @Test
    public static HashMap<String, String> testData(int rownum) throws IOException {
        /* This methods fetches the testdata from FetchFeature testdata excel and puts the testdata in HashMap*/
        HashMap<String, String> hm = new HashMap<String, String>(100);

        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream("testData/FetchFeature.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("FetchFeature");
        XSSFRow currentRow = testdatasheet.getRow(rownum);

        XSSFCell featureId = currentRow.getCell(1);
        String featureIdStr = featureId.toString();
        hm.put(FEATURE_ID_EXCEL, featureIdStr);

        XSSFCell tenantId = currentRow.getCell(2);
        String tenantIdStr = tenantId.toString();
        hm.put(TENANT_ID_EXCEL, tenantIdStr);

        XSSFCell xRequestID = currentRow.getCell(3);
        String xRequestIdStr = xRequestID.toString();

        if (xRequestIdStr.equals("XrequestLengthIs257Characters")) {
            hm.put(X_REQUEST_ID_EXCEL,
                    "hsafdhsfdjgsagfhsagfhsagdhfgsahfghjsagfhagdshfgasfhsagfhagsfghsagfhjgdsfdsgdshagdgfhjagdshfghjagsdhfgafhgdsafgdsahfgdsafhdsgfhgsdhfgafgdsfdshfgasdhjsssssssssssssssssssssssssssssssssssssssiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiieeeeeedddd");
        } else

        {
            Random rd = new Random();
            int randInt1 = rd.nextInt(100000);
            String sALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder salt = new StringBuilder();
            Random rnd = new Random();
            while (salt.length() < 18) {
                int index = (int) (rnd.nextFloat() * sALTCHARS.length());
                salt.append(sALTCHARS.charAt(index));
            }
            String saltStr = salt.toString();
            String xRequestId_random = randInt1 + saltStr;
            hm.put(X_REQUEST_ID_EXCEL, xRequestId_random);

        }
        fis.close();
        wb.close();
        return hm;
    }


    public static Response getResponse(HashMap<String, String> testdata, boolean apigeeFlag) {
        /* This methods forms the request body and fetches the response*/
        if (testdata.get(FEATURE_ID_EXCEL).equals("NULL")) {
            if (apigeeFlag) {
                response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                        .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY)
                        .queryParam("featureId", " ").when().get(CommonData.FETCH_FEATURE);
            } else {
                response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                        .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL)).queryParam("featureId", " ").when()
                        .get(CommonData.FETCH_FEATURE);
                LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
                LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));
            }

            LOGGER.debug("The Feature Id set is : null");

        } else if (testdata.get(FEATURE_ID_EXCEL).equals("NULLASSTRING")) {
            if (apigeeFlag) {
                response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                        .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY)
                        .queryParam("featureId", "null").when().get(CommonData.FETCH_FEATURE);
            } else {
                response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                        .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL)).queryParam("featureId", "null")
                        .when().get(CommonData.FETCH_FEATURE);
                LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
                LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));
            }

            LOGGER.debug("The Feature Id set is : 'null'");

        } else if (testdata.get(FEATURE_ID_EXCEL).equals("MISSING")) {
            if (apigeeFlag) {
                response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                        .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY).when()
                        .get(CommonData.FETCH_FEATURE);
            } else {
                response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                        .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);
                LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
                LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));
            }

            LOGGER.debug("The Feature Id is not set");

        } else {
            if (apigeeFlag) {
                response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                        .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY)
                        .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);
            } else {
                response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                        .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                        .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

                LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
                LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));
        }

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));

        }

        return response;
    }

    public static Response getResponseTenantValidations(HashMap<String, String> testdata) {
        /* This methods forms the requestbody for different Tenant Validations and fetches the response*/
        String tenantId = (String) testdata.get(TENANT_ID_EXCEL);
        switch (tenantId) {

        case "NULL":

            response = given().relaxedHTTPSValidation().header("X-TENANT", "")
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id set is : null ");
            LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));

            break;

        case "NULLASSTRING":

            response = given().relaxedHTTPSValidation().header("X-TENANT", "null")
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id set is : 'null' ");
            LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));
            break;

        case "MISSING":
            response = given().relaxedHTTPSValidation().header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id is not set");
            LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));

            break;

        default:
            response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
            LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));

            break;

        }

        return response;
    }

    public static Response getResponseXRequestValidations(HashMap<String, String> testdata) {
        /* This methods forms the requestbody for different XRequestId Validations and fetches the response*/

        String tenantId = (String) testdata.get(TENANT_ID_EXCEL);
        switch (tenantId) {

        case "NULL":

            response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                    .header("X-Request-Id", "").queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when()
                    .get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
            LOGGER.debug("The X_Request Id set is : null");

            break;

        case "NULLASSTRING":

            response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
            LOGGER.debug("The X_Request Id set is : 'null'");

            break;

        case "MISSING":
            response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
            LOGGER.debug("The X_Request Id is not set");

            break;

        default:
            response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
            LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));

            break;
        }

        return response;
    }

    public static Response getResponseInputParametersRepeated(HashMap<String, String> testdata,
            String testcasecondition, boolean apigeeFlag) {
        /* This methods forms the requestbody when Input parameters are repeated*/

        switch (testcasecondition) {

        case "FEATUREIDREPEATED":

            if (apigeeFlag) {
                response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                        .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY)
                        .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL))
                        .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            } else {
                response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                        .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                        .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL))
                        .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

                LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
                LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));
            }
            LOGGER.debug("The Feature Id is set twice");
            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));

            break;

        case "TENANTIDREPEATED":

            response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                    .header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The Tenant Id is set twice");
            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
            LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));

            break;

        case "XREQUESTIDREPEATED":
            response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The X_Request Id is set twice");
            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
            LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));

            break;

        case "RANDOMHEADERPASSED":
            response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                    .header("asjdf", "sdkfj").header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                    .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when().get(CommonData.FETCH_FEATURE);

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
            LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));
            LOGGER.debug("The random header key value pair is " + "key :" + "asjdf" + "pair : " + "sdkfj");

            break;

        case "RANDOMQUERYOARAMETERPASSED":
            if (apigeeFlag) {
                response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                        .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY)
                        .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).queryParam("adsadsad", "asdsad")
                        .when().get(CommonData.FETCH_FEATURE);

            } else {
                response = given().relaxedHTTPSValidation().header("X-TENANT", testdata.get(TENANT_ID_EXCEL))
                        .header("X-Request-Id", testdata.get(X_REQUEST_ID_EXCEL))
                        .queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).queryParam("adsadsad", "asdsad")
                        .when().get(CommonData.FETCH_FEATURE);
                LOGGER.debug("The Tenant Id set is : " + testdata.get(TENANT_ID_EXCEL));
                LOGGER.debug("The X_Request Id set is : " + testdata.get(X_REQUEST_ID_EXCEL));
            }

            LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
            LOGGER.debug("The random query param key value pair is " + "key :" + "adsadsad" + "pair : " + "asdsad");

            break;

        }

        return response;
    }

    public static Response getResponseWhenHeadersAreMissing(HashMap<String, String> testdata) {
        /* This methods forms the requestbody when headers are missing*/
        response = given().relaxedHTTPSValidation().queryParam("featureId", testdata.get(FEATURE_ID_EXCEL)).when()
                .get(CommonData.FETCH_FEATURE);

        LOGGER.debug("The Feature Id set is : " + testdata.get(FEATURE_ID_EXCEL));
        LOGGER.debug("Both the headers are not set ");
        return response;
    }

    public static int getResponseCount(Response response) {
        /* This methods gets the response count*/
        response.then().contentType(ContentType.JSON).extract();
        List<String> jsonResponse = response.jsonPath().getList("$");
        int count = jsonResponse.size();
        return count;

    }

    public static void VerifyResponseCountforSingleResponse(Response response) {
        /* This methods verifies the response when there is single repsonse*/
        int count = getResponseCount(response);
        int expectedCount = 1;
        AssertJUnit.assertEquals(count, expectedCount);
    }

    public static void verifyMultipleFeaturesResponse(Response response, boolean apigeeFlag)
            throws ClassNotFoundException, SQLException {
        /* This methods validates the response when the response has multiple results*/
        int count = getResponseCount(response);

        HashMap<String, String> jsonresponse = new HashMap<String, String>();
        for (int i = 1; i <= count; i++) {

            String featureId = response.jsonPath().getString("featureId");
            jsonresponse.put(FEATURE_ID_JSONRESPONSE, featureId);
            String description = response.jsonPath().getString("description");
            jsonresponse.put(DESCRIPTION_JSONRESPONSE, description);

            Connection con = null;
            if (apigeeFlag) {
                con = CommonMethods.connectDBAoisSchemaWithPreprodAPIGEE();
            } else {
                con = CommonMethods.connectDBAoisSchema();
            }
            HashMap<String, String> dbresponse = new HashMap<String, String>();

            String Query = "select * from aois.addon_feature";
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery(Query);
            rs.absolute(i);
            String featureIdDbresponse = rs.getString(1);

            dbresponse.put(DB_FEATURE_ID, featureIdDbresponse);
            String description_dbresponse = rs.getString(3);
            LOGGER.debug("The db response for description id is" + description_dbresponse);
            dbresponse.put(DB_DESCRIPTION, description_dbresponse);

            AssertJUnit.assertEquals(jsonresponse.get(FEATURE_ID_JSONRESPONSE), dbresponse.get(DB_FEATURE_ID));
            AssertJUnit.assertEquals(jsonresponse.get(DESCRIPTION_JSONRESPONSE), dbresponse.get(DB_DESCRIPTION));

        }

    }

    public static String jsonMapping(AddFeatureRequestNew requestdata) {
        /* This methods makes the JSON request body from the testdata*/
        List<JSONObject> list;
        JSONObject jsonobject = new JSONObject(requestdata);
        list = new ArrayList<JSONObject>();
        list.add(jsonobject);
        String reqbody = list.toString();
        int endIndex = reqbody.length();
        String requestbody = reqbody.substring(1, endIndex - 1);

        LOGGER.debug("The Request body set is " + requestbody);
        return requestbody;
    }

    public static void validateSuccessResponse(Response response) {
        /* This methods verifies the success response codes and messages*/
        CommonMethods.IsInValidJSONFormat(response);
        response.then().assertThat().statusCode(ResponseCodes.FETCH_FEATURE_SUCCESS_RESPONSE_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.FETCH_FEATURE_SUCCESS);
        String contentLength = response.getHeader("Content-Length");
        int contentLengthInt = Integer.parseInt(contentLength);
        assertNotEquals(2, contentLengthInt);

        LOGGER.debug("The Response Message is " + Messages.FETCH_FEATURE_SUCCESS);
        LOGGER.debug("The Content Type is " + ContentType.JSON);
        LOGGER.debug("The Content Length is " + contentLengthInt);

    }

    public static void validateMalformedResponse(Response response) {
        /* This methods verifies the bad request response codes and messages*/
        response.then().assertThat().statusCode(ResponseCodes.FETCH_FEATURE_MALFORMED_REQUEST_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.FETCH_FEATURE_MALFORMED_REQUEST);
        String contentLength = response.getHeader("Content-Length");
        int contentLengthInt = Integer.parseInt(contentLength);
        assertNotEquals(2, contentLengthInt);

        LOGGER.debug("The Response Message is " + Messages.FETCH_FEATURE_MALFORMED_REQUEST);
        LOGGER.debug("The Content Type is " + ContentType.JSON);
        LOGGER.debug("The Content Length is " + contentLengthInt);

    }

    public static void validateResponseBodyisNull(Response response) {
        /* This methods verifies the response codes and messages when there is NULL response and 200 OK*/
        response.then().assertThat().statusCode(ResponseCodes.FETCH_FEATURE_SUCCESS_RESPONSE_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.FETCH_FEATURE_SUCCESS);
        String contentLength = response.getHeader("Content-Length");
        int contentLengthInt = Integer.parseInt(contentLength);
        AssertJUnit.assertEquals(2, contentLengthInt);

        LOGGER.debug("The Response Message is " + Messages.FETCH_FEATURE_SUCCESS);
        LOGGER.debug("The Content Type is " + ContentType.JSON);
        LOGGER.debug("The Content Length is " + contentLengthInt);

    }

    public static void validateClientNotAuthorizedResponse(Response response) {
        /* This methods verifies the client unauthorized response codes and messages*/
        response.then().assertThat().statusCode(ResponseCodes.ADD_FEATURE_CLIENT_UNAUTHORIZED_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat()
                .statusLine(Messages.FETCH_FEATURE_CLIENT_UNAUTHORIZED);
        String contentLength = response.getHeader("Content-Length");
        int contentLengthInt = Integer.parseInt(contentLength);
        assertNotEquals(2, contentLengthInt);

        LOGGER.debug("The Response Message is " + Messages.FETCH_FEATURE_CLIENT_UNAUTHORIZED);
        LOGGER.debug("The Content Type is " + ContentType.JSON);
        LOGGER.debug("The Content Length is " + contentLengthInt);

    }

    public static void validateNoSuchFeatureFound(Response response) {
        /* This methods verifies No such feature found response codes and messages*/
        response.then().assertThat().statusCode(ResponseCodes.FETCH_FEATURE_NO_SUCH_FEATURE_FOUND).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.FETCH_FEATURE_PACKAGE_NOT_FOUND);
        String contentLength = response.getHeader("Content-Length");
        int contentLengthInt = Integer.parseInt(contentLength);
        assertNotEquals(2, contentLengthInt);

        LOGGER.debug("The Response Message is " + Messages.FETCH_FEATURE_PACKAGE_NOT_FOUND);
        LOGGER.debug("The Content Type is " + ContentType.JSON);
        LOGGER.debug("The Content Length is " + contentLengthInt);

    }

    public static void checkDatabaseAndCompare(HashMap<String, String> testdata, boolean apigeeFlag)
            throws SQLException, ClassNotFoundException {
        /* This methods verifies the response against database*/
        Connection con = null;
        HashMap<String, String> dbresponse = new HashMap<String, String>();
        HashMap<String, String> testdatafromexcel = new HashMap<String, String>();
        testdatafromexcel = testdata;
        String featureIdInsertedThroughAPI = testdatafromexcel.get(FEATURE_ID_EXCEL);
        String tenantIdInsertedThroughAPI = null;
        if (apigeeFlag) {
            con = CommonMethods.connectDBAoisSchemaWithPreprodAPIGEE();
            tenantIdInsertedThroughAPI = CommonData.APIGEE_CS_SERVICES_TENANT_ID;
        } else {
            con = CommonMethods.connectDBAoisSchema();
            tenantIdInsertedThroughAPI = testdatafromexcel.get(TENANT_ID_EXCEL);
        }

        String Query = "select * from aois.addon_feature where id=" + "\'" + featureIdInsertedThroughAPI + "\'" + ";";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(Query);
        if (rs.next()) {

            String featureId = rs.getString(1);
            LOGGER.debug("The db response for feature id is" + featureId);
            dbresponse.put(DB_FEATURE_ID, featureId);

            String tenantId = rs.getString(2);
            LOGGER.debug("The db response for tenant id is" + tenantId);
            dbresponse.put(DB_TENANT_ID, tenantId);

        }

        String featureIdFromDatabase = dbresponse.get(DB_FEATURE_ID);
        String tenantFromDatabase = dbresponse.get(DB_TENANT_ID);
        AssertJUnit.assertEquals(featureIdFromDatabase, featureIdInsertedThroughAPI);
        AssertJUnit.assertEquals(tenantFromDatabase, tenantIdInsertedThroughAPI);

    }

    public static void verifyDataIsNotPresentInDb(HashMap<String, String> testdata, boolean apigeeFlag)
            throws ClassNotFoundException, SQLException {
        /* This methods verifies if the requested data is not present in database*/
        HashMap<String, String> testdatafromexcel = new HashMap<String, String>();
        testdatafromexcel = testdata;
        Connection con = null;
        if (apigeeFlag) {
            con = CommonMethods.connectDBAoisSchemaWithPreprodAPIGEE();
        } else {
            con = CommonMethods.connectDBAoisSchema();
        }
        Statement stmt = con.createStatement();
        String featureIdInsertedThroughAPI = testdatafromexcel.get(FEATURE_ID_EXCEL);
        String Query = "select * from aois.addon_feature where id=" + "\'" + featureIdInsertedThroughAPI + "\'" + ";";
        ResultSet rs = stmt.executeQuery(Query);

        if (!rs.next()) {
            AssertJUnit.assertTrue(true);
        } else {
            AssertJUnit.assertFalse(true);
        }

    }

    public static void checkDatabaseAndCompareWhenFeatureIdisNull(HashMap<String, String> testdata, boolean apigeeFlag)
            throws SQLException, ClassNotFoundException {
        /* This methods verifies against database when FeatureId is NULL*/
        Connection con = null;
        if (apigeeFlag) {
            con = CommonMethods.connectDBAoisSchemaWithPreprodAPIGEE();
        } else {
            con = CommonMethods.connectDBAoisSchema();
        }
        HashMap<String, String> dbresponse = new HashMap<String, String>();

        String Query = "select * from aois.addon_feature +';'";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(Query);
        while (rs.next()) {

            String featureId = rs.getString(1);
            LOGGER.debug("The db response for feature id is" + featureId);
            dbresponse.put(DB_FEATURE_ID, featureId);

            String tenantId = rs.getString(2);
            LOGGER.debug("The db response for tenant id is" + tenantId);
            dbresponse.put(DB_TENANT_ID, tenantId);

        }
    }
    
    public static void verifylogsforSuccessResponse(Object featureId, Object xRequestId) throws MalformedURLException,
    IOException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
    KeyStoreException, CertificateException {

try {

    HTTPSTATUSCODE = "HTTPSTATUSCODE: 200";
    HTTPSTATUSMESSAGE = "OK";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"" + featureId + "\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"INFO\"}},{\"match\":{\"message\":\""
            + HTTPSTATUSMESSAGE + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The Query string is: " + urlParameters);
    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 


}

public static void verifylogsforBadRequest(Object featureId, Object xRequestId) throws MalformedURLException,
    IOException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
    KeyStoreException, CertificateException {



try {

    HTTPSTATUSCODE = "HTTPSTATUSCODE: 400";
    HTTPSTATUSMESSAGE = "Bad Request";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
            + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The Query string is: " + urlParameters);
    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 


}

public static void verifylogsforClientUnauthorized(Object featureId, Object xRequestId)
    throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
    NoSuchAlgorithmException, KeyStoreException, CertificateException {


try {
    HTTPSTATUSCODE = "HTTPSTATUSCODE: 403";
    HTTPSTATUSMESSAGE = "Client Unauthorized";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"" + featureId
            + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
            + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The Query string is: " + urlParameters);
    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 

}

public static String getFeatureId(HashMap<String, String> hm) {

    String FeatureId = hm.get(FetchFeatureUtils.FEATURE_ID_EXCEL);
    return FeatureId;

}

public static String getXRequestId(HashMap<String, String> hm) {

    String XRequestId = hm.get(FetchFeatureUtils.X_REQUEST_ID_EXCEL);
    return XRequestId;

}

}
