/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.healthCheck;

import org.testng.annotations.Test;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.testData;
import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.addPackage.AddPackageUtils;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.fetchFeature.FetchFeatureUtils;
import com.tomtom.aomsv2.fetchLicense.FetchLicenseUtils;
import com.tomtom.aomsv2.fetchPackage.FetchPackageUtils;
import com.tomtom.aomsv2.request.entity.UpdatePackage;
import com.tomtom.aomsv2.updatePackage.UpdatePackageUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;

/**
 * This class has the test case covering end to end flow of the application through ELB/Nodes.
 * 
 * @author palvadi
 */

public class ValidScenario {

    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    
    private Logger LOGGER = LoggerFactory.getLogger("healthCheck.ValidScenario");


    @Test(groups = "HealthCheck")
    public void HealthCheck() throws IOException, ClassNotFoundException, SQLException, JAXBException {

        HashMap<String, String> testdataaddFeature = new HashMap<String, String>();
        testdataaddFeature = AddFeatureUtils.testData(AddFeatureUtils.FEATURE_E2E_SCENARIO);
        Response responseaddFeature = AddFeatureUtils.addFeature(testdataaddFeature, false);
        AddFeatureUtils.validateSuccessResponse(responseaddFeature);
        AddFeatureUtils.fetchResponsefromDbandCompare(testdataaddFeature, false);

        HashMap<String, String> testdatafetchFeature = new HashMap<String, String>();
        testdatafetchFeature = FetchFeatureUtils.testData(FetchFeatureUtils.E2E_FEATURE_ID);
        Response responseFetchFeature = FetchFeatureUtils.getResponse(testdatafetchFeature, false);
        FetchFeatureUtils.validateSuccessResponse(responseFetchFeature);

        HashMap<Object, Object> testdataaddPackage = new HashMap<Object, Object>();
        testdataaddPackage = AddPackageUtils.addPackageTestData(AddPackageUtils.E2E_PACKAGE_ID);
        Response responseaddPackage = AddPackageUtils.addPackage(testdataaddPackage, false);
        AddPackageUtils.validateSuccessResponse(responseaddPackage);

        HashMap<Object, Object> testdataFetchPackage = new HashMap<Object, Object>();
        testdataFetchPackage = FetchPackageUtils.testData(FetchPackageUtils.E2E_FETCH_PACKAGE);

        Response response = given().relaxedHTTPSValidation()
                .param("packageId", testdataFetchPackage.get(FetchPackageUtils.FEATURE_ID))
                .param("featureId", testdataFetchPackage.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdataFetchPackage.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdataFetchPackage.get(FetchPackageUtils.END_DATE))
                .param("states", testdataFetchPackage.get(FetchPackageUtils.STATUS))
                .header("X-TENANT", testdataFetchPackage.get(FetchPackageUtils.XTENANT))
                .header("X-Request-Id", testdataFetchPackage.get(FetchPackageUtils.XREQUESTID)).when()
                .get(CommonData.FETCH_PACKAGE);
        FetchPackageUtils.validateSuccessResponse(response);

        HashMap<String, String> testdataFetchLicense = new HashMap<String, String>();
        testdataFetchLicense = FetchLicenseUtils.testData(FetchLicenseUtils.E2E_SCENARIO);
        String requestbodyFetchLicense = FetchLicenseUtils.xmlMapping(testdataFetchLicense);
        Response responseFetchLicense = FetchLicenseUtils.getFetchLicenseResponse(requestbodyFetchLicense,
                testdataFetchLicense, false);
        FetchLicenseUtils.validateSuccessResponse(responseFetchLicense);

        Map<String, String> testdataUpdatePackage = new HashMap<String, String>();
        testdataUpdatePackage = testData(UpdatePackageUtils.E2E_SCENARIO);
        UpdatePackage requestdataUpdatePackage = new UpdatePackage();
        requestdataUpdatePackage.setPackageId(testdataUpdatePackage.get(UpdatePackageUtils.PACKAGE_ID));
        requestdataUpdatePackage.setState(testdataUpdatePackage.get(UpdatePackageUtils.STATUS));
        String requestbodyromJson = UpdatePackageUtils.jsonMapping(requestdataUpdatePackage);

        String requestbodyUpdatePackage = '[' + requestbodyromJson + ']';

        Response responseUpdatePackage = given().relaxedHTTPSValidation().body(requestbodyUpdatePackage)
                .header("X-TENANT", testdataUpdatePackage.get(UpdatePackageUtils.TENANT_ID))
                .header("X-Request-Id", testdataUpdatePackage.get(UpdatePackageUtils.X_REQUEST_ID)).when()
                .post(CommonData.UPDATE_PACKAGE);
        UpdatePackageUtils.validateSuccessResponse(responseUpdatePackage);

        HashMap<String, String> testdataFetchLicense1 = new HashMap<String, String>();
        testdataFetchLicense1 = FetchLicenseUtils.testData(FetchLicenseUtils.E2E_SCENARIO);
        String requestbodyFetchLicense1 = FetchLicenseUtils.xmlMapping(testdataFetchLicense1);
        Response responseFetchLicense1 = FetchLicenseUtils.getFetchLicenseResponse(requestbodyFetchLicense1,
                testdataFetchLicense1, false);
        FetchLicenseUtils.validateMalformedRequest(responseFetchLicense1);

    }

}
