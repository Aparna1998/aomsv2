/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.deletePackage;

import org.testng.annotations.Test;
import com.tomtom.aomsv2.commonControls.CommonData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations of Tenant Id field for Delete Package API and verifying
 * the response through ELB/Nodes.
 * 
 * @author palvadi
 */
public class TenantValidations {

    private static final Logger LOGGER = LoggerFactory.getLogger("DeletePackage.DeletePackageTenantValidations");
    HashMap<Object, Object> testdata;

    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    
    
    @Test(groups = "DeletePackage")
    public void deletePackageTenantIsNull() throws IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException, InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageTenantIsNull\"********************");

        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_VALID_DATA, false);
        Response response = DeletePackageUtils
                .deletePackageTenantisNull(DeletePackageUtils.DELETE_PACKAGE_TENANT_IS_NULL);
        DeletePackageUtils.validateClientUnauthorizedResponse(response);
        testdata = null;
    }

    @Test(groups = "DeletePackage")
    public void deletePackageTenantisMissing() throws IOException {
        LOGGER.info("********************Starting TestCase \"deletePackageTenantisMissing\"********************");

        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_VALID_DATA, false);
        Response response = DeletePackageUtils
                .deletePackageTenantisMissing(DeletePackageUtils.DELETE_PACKAGE_VALID_DATA);
        DeletePackageUtils.validateBadRequestResponse(response);
        testdata = null;
    }

    @Test(groups = "DeletePackage")
    public void deletePackageTenantis256Characters() throws IOException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageTenantis256Characters\"********************");

        testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_TENANT_IS_256_CHARACTERS);
        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_TENANT_IS_256_CHARACTERS, false);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_TENANT_IS_256_CHARACTERS, false, testdata);
        DeletePackageUtils.validateSuccessResponse(response);
        testdata = null;

    }

    @Test(groups = "DeletePackage")
    public void deletePackageTenantisgreaterthan256Characters() throws IOException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageTenantisgreaterthan256Characters\"********************");

        testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_TENANT_IS_256_CHARACTERS);
        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_TENANT_IS_256_CHARACTERS, false);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_TENANT_IS_256_CHARACTERS, false, testdata);
        DeletePackageUtils.validateSuccessResponse(response);
        testdata = null;
    }

    @Test(groups = "DeletePackage")
    public void deletePackageTenantis255Characters() throws IOException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageTenantis255Characters\"********************");

        testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_TENANT_IS_255_CHARACTERS);
        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_TENANT_IS_255_CHARACTERS, false);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_TENANT_IS_255_CHARACTERS, false, testdata);
        DeletePackageUtils.validateSuccessResponse(response);
        testdata = null;
    }

    @Test(groups = "DeletePackage")
    public void deletePackageTenantNotInDatabase() throws IOException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageTenantNotInDatabase\"");

        testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_TENANT_NOT_IN_DATABASE);
        Response response = DeletePackageUtils.deletePackage(DeletePackageUtils.DELETE_PACKAGE_TENANT_NOT_IN_DATABASE,
                false, testdata);
        DeletePackageUtils.validateClientUnauthorizedResponse(response);
        testdata = null;
    }

    @Test(groups = "DeletePackage")
    public void deletePackageTenantNotMapped() throws IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException, InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageTenantNotInDatabase\"");

        testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_TENANT_NOT_MAPPED);
        Response response = DeletePackageUtils.deletePackage(DeletePackageUtils.DELETE_PACKAGE_TENANT_NOT_MAPPED,
                false, testdata);
        DeletePackageUtils.validateBadRequestResponse(response);
        testdata = null;
    }   
}
