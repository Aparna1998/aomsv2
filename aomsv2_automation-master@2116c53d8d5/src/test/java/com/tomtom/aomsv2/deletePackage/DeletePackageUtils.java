/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.deletePackage;

import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.addPackage.AddPackageUtils;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.commonControls.Messages;
import com.tomtom.aomsv2.commonControls.ResponseCodes;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Random;

/**
 * This class has the common methods for Delete Package API which are referenced by all other test cases.
 * 
 * @author palvadi
 */
public class DeletePackageUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger("DeletePackage.DeletePackageUtils");
    /* Variables defined for test data Hash Map of the method 'testData' */
    public static final String PACKAGE_ID_ADD_PACKAGE = "PackageId";
    public static final String FEATURE_ID_ADD_PACKAGE = "FeatureId";
    public static final String STATE = "State";
    public static final String METADATA = "Metadata";
    public static final String SIZE = "size";
    public static final String X_REQUEST_ID = "X_RequestId";
    public static final String CHECKSUM_TYPE = "Checksum_type";
    public static final String CHECKSUM_VALUE = "Checksum_value";
    public static final String URL = "URL";
    public static final String TENANT_ADD_PACKAGE = "Tenant_AddPackage";
    public static final String X_REQUEST_ID_ADD_PACKAGE = "XRequestId";
    public static final String PARTNUMBER = "PartNumber";

    public static final String PACKAGE_ID = "Package";
    public static final String FEATURE_ID = "Feature";
    public static final String TENANT_ID = "Tena";

    /*
     * Variables defined for corresponding row numbers present in Add Feature Test data sheet
     */
    public static final int ADD_PACKAGE_VALID_DATA = 1;
    public static final int ADD_PACKAGE_PACKAGE_ID_IS_256_CHARACTERS = 2;
    public static final int ADD_PACKAGE_PACKAGE_ID_IS_255_CHARACTERS = 3;
    public static final int ADD_PACKAGE_FEATURE_ID_IS_256_CHARACTERS = 4;
    public static final int ADD_PACKAGE_FEATURE_ID_IS_255_CHARACTERS = 5;
    public static final int ADD_PACKAGE_TENANT_IS_256_CHARACTERS = 6;
    public static final int ADD_PACKAGE_TENANT_IS_255_CHARACTERS = 7;
    public static final int ADD_PACKAGE_PACKAGE_ID_DELETION_DOES_NOT_DELETE_FEATURE_ID = 8;
    public static final int DELETE_PACKAGE_VALID_DATA = 1;
    public static final int DELETE_PACKAGE_PACKAGE_ID_IS_NULL = 2;
    public static final int DELETE_PACKAGE_PACKAGE_ID_IS_256_CHARACTERS = 3;
    public static final int DELETE_PACKAGE_PACKAGE_ID_IS_255_CHARACTERS = 4;
    public static final int DELETE_PACKAGE_PACKAGE_ID_NOT_IN_DATABASE = 5;
    public static final int DELETE_PACKAGE_FEATURE_ID_IS_NULL = 6;
    public static final int DELETE_PACKAGE_FEATURE_ID_IS_256_CHARACTERS = 7;
    public static final int DELETE_PACKAGE_FEATURE_ID_IS_255_CHARACTERS = 8;
    public static final int DELETE_PACKAGE_FEATURE_ID_NOT_IN_DATABASE = 9;
    public static final int DELETE_PACKAGE_TENANT_IS_NULL = 10;
    public static final int DELETE_PACKAGE_TENANT_IS_256_CHARACTERS = 11;
    public static final int DELETE_PACKAGE_TENANT_IS_255_CHARACTERS = 12;
    public static final int DELETE_PACKAGE_TENANT_NOT_IN_DATABASE = 13;
    public static final int DELETE_PACKAGE_TENANT_NOT_MAPPED = 14;
    public static final int DELETE_PACKAGE_X_REQUEST_ID_IS_NULL = 15;
    public static final int DELETE_PACKAGE_X_REQUEST_ID_IS_256_CHARACTERS = 16;
    public static final int DELETE_PACKAGE_X_REQUEST_ID_IS_255_CHARACTERS = 17;
    public static final int DELETE_PACKAGE_PACKAGE_ID_GREATER_THAN_256_CHARACTERS = 18;
    public static final int DELETE_PACKAGE_FEATURE_ID_GREATER_THAN_256_CHARACTERS = 19;
    public static final int DELETE_PACKAGE_E2E_SCENARI0 = 20;
    public static final int DELETE_PACKAGE_FEATURE_AND_PACKAGE_ID_NOT_AVAILABLE_IN_DB = 21;
    public static final int DELETE_PACKAGE_PACKAGE_ID_DELETION_DOES_NOT_DELETE_FEATURE_ID = 22;
    public static final int DELETE_PACKAGE_PACKAGE_ID_AND_FEATURE_ID_DOES_NOT_BELONG_TO_TENANT = 23;
    public static final String SUCCESSRESPONSE = "successresponse";
    public static final String MALFORMEDRESPONSE = "malformedresponse";
    public static final String CLIENTUNAUTHORIZED = "clientunauthorized";
    public static final String FEATUREIDALREADYPRESENT = "featurealreadypresent";
    public static Object HTTPSTATUSCODE = "HTTPSTATUSCODE";
    public static Object HTTPSTATUSMESSAGE = "HTTPSTATUSMESSAGE";
    public static final Object LOGFILENAME = "/var/log/aomsv2/DeletePackage.log";
    public static Object CATEGORY = "CATEGORY";

    public static HashMap<Object, Object> deletePackageTestData(int Rownum) throws IOException {
        /* This method fetches the testdata from DeletePackage excel and inputs it to a HashMap */
        HashMap<Object, Object> hm = new HashMap<Object, Object>();

        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream("testData/DeletePackage.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("DeletePackage");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell packageId = currentRow.getCell(1);
        String pckgIdStr = packageId.toString();
        if (pckgIdStr.equals("NULL")) {
            hm.put(PACKAGE_ID, null);
        } else {
            hm.put(PACKAGE_ID, pckgIdStr);
        }

        XSSFCell featureId = currentRow.getCell(2);
        String featureIdStr = featureId.toString();
        if (featureIdStr.equals("NULL")) {
            hm.put(FEATURE_ID, null);
        } else {
            hm.put(FEATURE_ID, featureIdStr);
        }

        XSSFCell tenantCell = currentRow.getCell(3);
        String tenant = tenantCell.toString();
        if (tenant.equals("NULL")) {
            hm.put(TENANT_ID, null);
        } else {
            hm.put(TENANT_ID, tenant);
        }

        XSSFCell xrequestIdCell = currentRow.getCell(4);
        String xRequestId = xrequestIdCell.toString();
        if (xRequestId.equals("NULL")) {
            hm.put(X_REQUEST_ID, null);
        } else {
            Random rd = new Random();
            int randInt1 = rd.nextInt(100000);
            String sALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder salt = new StringBuilder();
            Random rnd = new Random();
            while (salt.length() < 18) {
                int index = (int) (rnd.nextFloat() * sALTCHARS.length());
                salt.append(sALTCHARS.charAt(index));
            }
            String saltStr = salt.toString();
            String xRequestId_random = randInt1 + saltStr;
            hm.put(X_REQUEST_ID, xRequestId_random);
        }
        wb.close();
        return hm;
    }

    public static HashMap<Object, Object> addPackageTestData(int Rownum) throws IOException {
        /* This method fetches the testdata from DeletePackage excel for Add Package and inputs it to a HashMap */
        
        HashMap<Object, Object> hm = new HashMap<Object, Object>();
        FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY + CommonData.DELETE_PACKAGE_TEST_DATA_PATH);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("DeletePackage_forAddingPackage");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell packageId = currentRow.getCell(1);
        String pckgIdStr = packageId.toString();
        if (pckgIdStr.equals("NULL")) {
            hm.put(PACKAGE_ID_ADD_PACKAGE, null);
        } else {
            hm.put(PACKAGE_ID_ADD_PACKAGE, pckgIdStr);
        }

        XSSFCell featureId = currentRow.getCell(2);
        String featureIdStr = featureId.toString();
        if (featureIdStr.equals("NULL")) {
            hm.put(FEATURE_ID_ADD_PACKAGE, null);
        } else {
            hm.put(FEATURE_ID_ADD_PACKAGE, featureIdStr);
        }

        XSSFCell stateCell = currentRow.getCell(3);
        String state = stateCell.toString();
        if (state.equals("NULL")) {
            hm.put(STATE, null);
        }
        hm.put(STATE, state);

        XSSFCell metadataCell = currentRow.getCell(4);
        String metadata = metadataCell.toString();
        if (metadata.equals("NULL")) {
            hm.put(METADATA, null);
        } else {
            hm.put(METADATA, metadata);
        }

        XSSFCell checksumType = currentRow.getCell(5);
        String checksumtype = checksumType.toString();

        if (checksumtype.equals("NULL")) {
            hm.put(CHECKSUM_TYPE, null);
        } else {
            hm.put(CHECKSUM_TYPE, checksumtype);
        }

        XSSFCell checksumValue = currentRow.getCell(6);
        String checksumvalue = checksumValue.toString();
        if (checksumvalue.equals("NULL")) {
            hm.put(CHECKSUM_VALUE, null);
        } else {
            hm.put(CHECKSUM_VALUE, checksumvalue);
        }

        XSSFCell url = currentRow.getCell(7);
        String Url = url.toString();
        if (Url.equals("NULL")) {
            hm.put(URL, null);
        }
        hm.put(URL, Url);

        XSSFCell sizeCell = currentRow.getCell(8);
        String size = sizeCell.toString();
        if (size.equals("NULL")) {
            hm.put(SIZE, null);
        } else {
            hm.put(SIZE, size);
        }

        XSSFCell partNumberCell = currentRow.getCell(9);
        String partnumber = partNumberCell.toString();
        if (partnumber.equals("0")) {
            hm.put(PARTNUMBER, null);
        } else {
            hm.put(PARTNUMBER, partnumber);
        }

        XSSFCell tenantCell = currentRow.getCell(10);
        String tenant = tenantCell.toString();
        if (tenant.equals("NULL")) {
            hm.put(TENANT_ADD_PACKAGE, null);
        } else {
            hm.put(TENANT_ADD_PACKAGE, tenant);
        }

        XSSFCell xrequestIdCell = currentRow.getCell(11);
        String XRequestid = xrequestIdCell.toString();
        if (XRequestid.equals("NULL")) {
            hm.put(X_REQUEST_ID_ADD_PACKAGE, null);
        } else {
            hm.put(X_REQUEST_ID_ADD_PACKAGE, XRequestid);
        }

        wb.close();
        return hm;
    }

    public static Object addPackage(int RowNumber, boolean apigeeFlag) throws IOException {
        /* This methods forms the request body and triggers the request for AddPackage API */
        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(RowNumber);
        String requestbodyStr = AddPackageUtils.jsonMapping(addPackageTestData);
        String requestbody = '[' + requestbodyStr + ']';
        LOGGER.debug("The Request Body is:" + requestbody);
        if (apigeeFlag) {
            given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_PACKAGE);
        } else {
            Object tenantid = addPackageTestData.get(DeletePackageUtils.TENANT_ADD_PACKAGE);
            given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", tenantid)
                    .header("X-Request-Id", X_REQUEST_ID_ADD_PACKAGE).when().put(CommonData.ADD_PACKAGE);
        }
        Object packageId = addPackageTestData.get(AddPackageUtils.PACKAGE_ID);
        return packageId;
    }

    public static Response deletePackage(int testdata, boolean apigeeFlag, HashMap<Object, Object> hm)
            throws IOException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, InterruptedException {
        /* This methods forms the request body and triggers the request for DeletePackage API */
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = hm;
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY)
                    .param("packageId", dm.get(PACKAGE_ID)).param("featureId", dm.get(FEATURE_ID)).when()
                    .delete(CommonData.DELETE_PACKAGE);

        } else {
            response = given().relaxedHTTPSValidation().param("packageId", dm.get(PACKAGE_ID))
                    .param("featureId", dm.get(FEATURE_ID)).header("X-TENANT", dm.get(TENANT_ID))
                    .header("X-Request-Id", dm.get(X_REQUEST_ID)).when().delete(CommonData.DELETE_PACKAGE);

            LOGGER.debug("Tenant Id is " + dm.get(TENANT_ID));
            LOGGER.debug("X-Request-Id is " + dm.get(X_REQUEST_ID));
        }
        LOGGER.debug("The packageId set for deletion is " + dm.get(PACKAGE_ID));
        LOGGER.debug("The FeatureId set for deletion is " + dm.get(FEATURE_ID));

        return response;
    }


    public static Response deletePackagePackageIdisMissing(int testdata, boolean apigeeFlag) throws IOException {
        /* This methods forms the request body and triggers the request for DeletePackage API when Package Id is missing*/
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deletePackageTestData(testdata);
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY)
                    .param("featureId", dm.get(FEATURE_ID)).when().delete(CommonData.DELETE_PACKAGE);

        } else {
            response = given().relaxedHTTPSValidation().param("featureId", dm.get(FEATURE_ID))
                    .header("X-TENANT", dm.get(TENANT_ID)).header("X-Request-Id", dm.get(X_REQUEST_ID)).when()
                    .delete(CommonData.DELETE_PACKAGE);

            LOGGER.debug("Tenant Id is " + dm.get(TENANT_ID));
            LOGGER.debug("X-Request-Id is " + dm.get(X_REQUEST_ID));
        }
        LOGGER.debug("The PackageId is not set for the request ");
        LOGGER.debug("The FeatureId set for deletion is " + dm.get(FEATURE_ID));

        return response;
    }

    public static Response deletePackageFeatureIdisMissing(int testdata, boolean apigeeFlag) throws IOException {
        /* This methods forms the request body and triggers the request for DeletePackage API when Feature Id is missing*/

        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deletePackageTestData(testdata);
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY)
                    .param("packageId", dm.get(PACKAGE_ID)).when().delete(CommonData.DELETE_PACKAGE);

        } else {
            response = given().relaxedHTTPSValidation().param("packageId", dm.get(PACKAGE_ID))
                    .header("X-TENANT", dm.get(TENANT_ID)).header("X-Request-Id", dm.get(X_REQUEST_ID)).when()
                    .delete(CommonData.DELETE_PACKAGE);

            LOGGER.debug("Tenant Id is " + dm.get(TENANT_ID));
            LOGGER.debug("X-Request-Id is " + dm.get(X_REQUEST_ID));
        }

        LOGGER.debug("The packageId set for deletion is " + dm.get(PACKAGE_ID));
        LOGGER.debug("The FeatureId is not set for the request");

        return response;
    }

    public static Response deletePackageXRequestIdisMissing(int testdata) throws IOException {
        /* This methods forms the request body and triggers the request for DeletePackage API when XRequestId is missing*/

        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deletePackageTestData(testdata);
        Response response = given().relaxedHTTPSValidation().param("packageId", dm.get(PACKAGE_ID))
                .param("featureId", dm.get(FEATURE_ID)).header("X-TENANT", dm.get(TENANT_ID)).when()
                .delete(CommonData.DELETE_PACKAGE);
        LOGGER.debug("The packageId set for deletion is " + dm.get(PACKAGE_ID));
        LOGGER.debug("The FeatureId set for deletion is " + dm.get(FEATURE_ID));
        LOGGER.debug("Tenant Id is " + dm.get(TENANT_ID));
        LOGGER.debug("The XRequestId is not set for the request");
        return response;
    }

    public static Response deletePackageTenantisMissing(int testdata) throws IOException {
        /* This methods forms the request body and triggers the request for DeletePackage API when TenantId is missing*/

        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deletePackageTestData(testdata);
        Response response = given().relaxedHTTPSValidation().param("packageId", dm.get(PACKAGE_ID))
                .param("featureId", dm.get(FEATURE_ID)).header("X-Request-Id", dm.get(X_REQUEST_ID)).when()
                .delete(CommonData.DELETE_PACKAGE);

        LOGGER.debug("The packageId set for deletion is " + dm.get(PACKAGE_ID));
        LOGGER.debug("The FeatureId set for deletion is " + dm.get(FEATURE_ID));
        LOGGER.debug("The TenantId is not set for the request");
        LOGGER.debug("X-Request-Id is " + dm.get(X_REQUEST_ID));
        return response;
    }

    public static Response deletePackageTenantisNull(int testdata) throws IOException {
        /* This methods forms the request body and triggers the request for DeletePackage API when TenantId is NULL*/

        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deletePackageTestData(testdata);
        Response response = given().relaxedHTTPSValidation().param("packageId", dm.get(PACKAGE_ID))
                .param("featureId", dm.get(FEATURE_ID)).header("X-TENANT", "null")
                .header("X-Request-Id", dm.get(X_REQUEST_ID)).when().delete(CommonData.DELETE_PACKAGE);

        LOGGER.debug("The packageId set for deletion is " + dm.get(PACKAGE_ID));
        LOGGER.debug("The FeatureId set for deletion is " + dm.get(FEATURE_ID));
        LOGGER.debug("Tenant Id is " + dm.get(TENANT_ID));
        LOGGER.debug("X-Request-Id is " + dm.get(X_REQUEST_ID));
        return response;
    }

    public static Response deletePackageHeadersAreEmpty(int testdata) throws IOException {
        /* This methods forms the request body and triggers the request for DeletePackage API when headers are empty*/

        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deletePackageTestData(testdata);
        Response response = given().relaxedHTTPSValidation().param("packageId", dm.get(PACKAGE_ID))
                .param("featureId", dm.get(FEATURE_ID)).when().delete(CommonData.DELETE_PACKAGE);

        LOGGER.debug("The packageId set for deletion is " + dm.get(PACKAGE_ID));
        LOGGER.debug("The FeatureId set for deletion is " + dm.get(FEATURE_ID));
        LOGGER.debug("Headers are not set");

        return response;
    }

    public static Response deletePackageQueryParamsNotPresent(int testdata) throws IOException {
        /* This methods forms the request body and triggers the request for DeletePackage API when query params are not present*/

        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deletePackageTestData(testdata);
        Response response = given().relaxedHTTPSValidation().header("X-TENANT", dm.get(DeletePackageUtils.TENANT_ID))
                .header("X-Request-Id", dm.get(X_REQUEST_ID)).when().delete(CommonData.DELETE_PACKAGE);

        LOGGER.debug("Tenant Id is " + dm.get(TENANT_ID));
        LOGGER.debug("X-Request-Id is " + dm.get(X_REQUEST_ID));
        LOGGER.debug("Query Params are not set");

        return response;
    }

    public static Response deletePackageXRequestIdisNull(int testdata) throws IOException {
        /* This methods forms the request body and triggers the request for DeletePackage API when XRequest Id is NULL */

        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deletePackageTestData(testdata);
        Response response = given().relaxedHTTPSValidation().param("packageId", dm.get(PACKAGE_ID))
                .param("featureId", dm.get(FEATURE_ID)).header("X-TENANT", dm.get(TENANT_ID))
                .header("X-Request-Id", "null").when().delete(CommonData.DELETE_PACKAGE);

        LOGGER.debug("The packageId set for deletion is " + dm.get(PACKAGE_ID));
        LOGGER.debug("The FeatureId set for deletion is " + dm.get(FEATURE_ID));
        LOGGER.debug("Tenant Id is " + dm.get(TENANT_ID));
        LOGGER.debug("X-Request-Id is " + dm.get(X_REQUEST_ID));
        return response;
    }

    public static void validateSuccessResponse(Response response) throws IOException {
        /* This methods validates against success response codes and messages*/

        response.then().assertThat().statusCode(ResponseCodes.DELETE_PACKAGE_DELETED_SUCCESSFULLY).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.DELETE_PACKAGE_SUCCESS);

        LOGGER.debug("The response is " + Messages.DELETE_PACKAGE_SUCCESS);
        LOGGER.debug("The ContentType is ContentType.JSON");
        LOGGER.debug("The ContentLength is 0");
    }

    public static void validateBadRequestResponse(Response response) throws IOException {
        /* This methods validates against bad request response codes and messages*/
        response.then().assertThat().statusCode(ResponseCodes.DELETE_PACKAGE_BAD_REQUEST_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.DELETE_PACKAGE_BAD_REQUEST);

        LOGGER.debug("The response is " + Messages.DELETE_PACKAGE_BAD_REQUEST);
        LOGGER.debug("The ContentType is ContentType.JSON");
        LOGGER.debug("The ContentLength is 0");
    }

    public static void validateClientUnauthorizedResponse(Response response) throws IOException {
        /* This methods validates against client unauthorized response codes and messages*/
        response.then().assertThat().statusCode(ResponseCodes.DELETE_PACKAGE_CLIENT_UNAUTHORIZED).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat()
                .statusLine(Messages.DELETE_PACKAGE_CLIENT_UNAUTHORIZED);

        LOGGER.debug("The response is " + Messages.DELETE_PACKAGE_BAD_REQUEST);
        LOGGER.debug("The ContentType is ContentType.JSON");
        LOGGER.debug("The ContentLength is 0");
    }
    
    public static void verifylogsforSuccessResponse(Object packageId, Object xRequestId) throws MalformedURLException,
    IOException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
    KeyStoreException, CertificateException {



try {

    HTTPSTATUSCODE = "HTTPSTATUSCODE: 204";
    HTTPSTATUSMESSAGE = "Package is deleted successfully";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"" + packageId + "\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"INFO\"}},{\"match\":{\"message\":\""
            + HTTPSTATUSMESSAGE + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The URL parameters is " + urlParameters);
    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 

}

public static void verifylogsforBadRequest(Object packageId, Object xRequestId, String logMessage)
    throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
    NoSuchAlgorithmException, KeyStoreException, CertificateException {


try {

    HTTPSTATUSCODE = "HTTPSTATUSCODE: 400";
    HTTPSTATUSMESSAGE = "Bad Request";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"" + logMessage
            + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
            + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The URL parameters is " + urlParameters);
    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 


}

public static void verifylogsforClientUnauthorizedResponse(Object packageId, Object xRequestId)
    throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
    NoSuchAlgorithmException, KeyStoreException, CertificateException {


try {

    HTTPSTATUSCODE = "HTTPSTATUSCODE: 403";
    HTTPSTATUSMESSAGE = "Client Unauthorized";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"" + packageId + "\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\""
            + HTTPSTATUSMESSAGE + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The URL parameters is " + urlParameters);
    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 


}
public static Object getPackageId(HashMap<Object, Object> hm) {

    Object PackageId = hm.get(DeletePackageUtils.PACKAGE_ID);
    return PackageId;

}

public static Object getXRequestId(HashMap<Object, Object> hm) {

    Object XRequestId = hm.get(DeletePackageUtils.X_REQUEST_ID);
    return XRequestId;

}

}