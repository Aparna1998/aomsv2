/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.deletePackage;

import org.testng.annotations.Test;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.fetchFeature.FetchFeatureUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;

/**
 * This class has the test cases having different Miscellaneous scenarios for Delete Package API and verifying the
 * response through ELB/Nodes.
 * 
 * @author palvadi
 */

public class MiscellaneousScenarios {

    private static final Logger LOGGER = LoggerFactory.getLogger("DeletePackage.Miscellaneous");
    HashMap<Object, Object> testdata;

    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    
    
    @Test(groups = "DeletePackage")
    public void packageIdDeletionDoesnotDeleteFeatureId() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("********************Starting TestCase \"packageIdDeletionDoesnotDeleteFeatureId\"********************");
        testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_PACKAGE_ID_DELETION_DOES_NOT_DELETE_FEATURE_ID);
        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_PACKAGE_ID_DELETION_DOES_NOT_DELETE_FEATURE_ID,
                false);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_PACKAGE_ID_DELETION_DOES_NOT_DELETE_FEATURE_ID, false, testdata);
        DeletePackageUtils.validateSuccessResponse(response);
        HashMap<String, String> hm = new HashMap<String, String>();
        hm = FetchFeatureUtils.testData(FetchFeatureUtils.PACKAGE_CHECKING_FEATURE_ID_IS_NOT_DELETED);
        Response response_fetchfeature = FetchFeatureUtils.getResponse(hm, false);
        FetchFeatureUtils.validateSuccessResponse(response_fetchfeature);
        testdata = null;

    }

    @Test(groups = "DeletePackage")
    public void packageIdDeletionDoesnotDeleteTenant() throws IOException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            InterruptedException {
        LOGGER.info("********************Starting TestCase \"packageIdDeletionDoesnotDeleteTenant\"********************");
        testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_PACKAGE_ID_DELETION_DOES_NOT_DELETE_FEATURE_ID);
        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_PACKAGE_ID_DELETION_DOES_NOT_DELETE_FEATURE_ID,
                false);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_PACKAGE_ID_DELETION_DOES_NOT_DELETE_FEATURE_ID, false, testdata);
        DeletePackageUtils.validateSuccessResponse(response);
        HashMap<String, String> hm = new HashMap<String, String>();
        hm = FetchFeatureUtils.testData(FetchFeatureUtils.PACKAGE_CHECKING_FEATURE_ID_IS_NOT_DELETED);
        Response response_fetchfeature = FetchFeatureUtils.getResponse(hm, false);
        FetchFeatureUtils.validateSuccessResponse(response_fetchfeature);
        testdata = null;

    }

    @Test(groups = "DeletePackage")
    public void packageIdandFeatureIdAssociatedWithDifferentTenant() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("********************Starting TestCase \"packageIdandFeatureIdAssociatedWithDifferentTenant\"********************");

        testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_PACKAGE_ID_AND_FEATURE_ID_DOES_NOT_BELONG_TO_TENANT);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_PACKAGE_ID_AND_FEATURE_ID_DOES_NOT_BELONG_TO_TENANT, false, testdata);
        DeletePackageUtils.validateBadRequestResponse(response);
        testdata = null;

    }

    @Test(groups = "DeletePackage")
    public void featureAndPackageNotAvailableInDatabase() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("********************Starting TestCase \"featureAndPackageNotAvailableInDatabase\"********************");

        testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_FEATURE_AND_PACKAGE_ID_NOT_AVAILABLE_IN_DB);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_FEATURE_AND_PACKAGE_ID_NOT_AVAILABLE_IN_DB, false, testdata);
        DeletePackageUtils.validateSuccessResponse(response);
        testdata = null;

    }

    @Test(groups = "DeletePackage")
    public void headersAreEmpty() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("********************Starting TestCase \"headersAreEmpty\"********************");

        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_VALID_DATA, false);
        Response response = DeletePackageUtils
                .deletePackageHeadersAreEmpty(DeletePackageUtils.DELETE_PACKAGE_VALID_DATA);
        DeletePackageUtils.validateBadRequestResponse(response);

    }

    @Test(groups = "DeletePackage")
    public void requestBodyIsEmpty() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("********************Starting TestCase \"requestBodyIsEmpty\"********************");
        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_VALID_DATA, false);
        Response response = DeletePackageUtils
                .deletePackageQueryParamsNotPresent(DeletePackageUtils.DELETE_PACKAGE_VALID_DATA);
        DeletePackageUtils.validateBadRequestResponse(response);
    }

}
