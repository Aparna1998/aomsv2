/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.addPackage;

import org.testng.annotations.Test;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackageTenantValidations;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackageTestData;

import com.tomtom.aomsv2.commonControls.CommonData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for Tenant field for Add Package API and verifying the
 * response through ELB/Nodes.
 * 
 * @author palvadi
 */
public class TenantIdValidations {
    
    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    

    private static final Logger LOGGER = LoggerFactory.getLogger("AddPackage.HeaderValidations");
    HashMap<Object, Object> addPackageTestData;

    @Test(groups = "AddPackage")
    public void addPackageWhenTenantisNull() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWhenTenantisNull\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.TENANT_IS_NULL);
        Response response = addPackageTenantValidations(addPackageTestData);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWhenTenantisNullAsString() throws IOException, InterruptedException {
        LOGGER.info("********************Starting TestCase \"addPackageWhenTenantisNullAsString\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.TENANT_NULL_AS_STRING);
        Response response = addPackageTenantValidations(addPackageTestData);
        AddPackageUtils.validateClientUnauthorizedResponse(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWhenTenantisMissing() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWhenTenantisMissing\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.TENANT_IS_MISSING);
        Response response = addPackageTenantValidations(addPackageTestData);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWhenTenantNotInDatabase() throws IOException, InterruptedException {
        LOGGER.info("********************Starting TestCase \"addPackageWhenTenantNotInDatabase\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.TENANT_NOT_IN_DATABASE);
        Response response = addPackageTenantValidations(addPackageTestData);
        AddPackageUtils.validateClientUnauthorizedResponse(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWhenTenantGreaterThan256Characters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWhenTenantGreaterThan256Characters\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.TENANT_IS_GREATER_THAN_256_CHARACTERS);
        Response response = addPackageTenantValidations(addPackageTestData);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWhenTenantis256Characters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWhenTenantis256Characters\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.TENANT_IS_256_CHARACTERS);
        Response response = addPackageTenantValidations(addPackageTestData);
        AddPackageUtils.validateSuccessResponse(response);
        addPackageTestData = null;
    }   

    @Test(groups = "AddPackage")
    public void addPackageTenantis255Characters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageTenantis255Characters\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.TENANT_IS_255_CHARACTERS);
        Response response = addPackageTenantValidations(addPackageTestData);
        AddPackageUtils.validateSuccessResponse(response);
        addPackageTestData = null;

    }

    @Test(groups = "AddPackage")
    public void addPackageTenanthasSpecialCharacters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageTenanthasSpecialCharacters\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.TENANT_HAS_SPECIAL_CHARACTERS);
        Response response = addPackageTenantValidations(addPackageTestData);
        AddPackageUtils.validateSuccessResponse(response);

    }

}