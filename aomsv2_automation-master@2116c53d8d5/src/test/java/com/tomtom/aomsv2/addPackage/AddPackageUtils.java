/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.addPackage;

import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.commonControls.Base;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.commonControls.Messages;
import com.tomtom.aomsv2.commonControls.ResponseCodes;
import com.tomtom.aomsv2.request.entity.AddPackage;
import com.tomtom.aomsv2.request.entity.Checksum;
import com.tomtom.aomsv2.request.entity.DownloadInfo;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.xml.sax.SAXException;

import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

/**
 * This class has the common methods for Add Package API which are referenced by all the test cases.
 * 
 * @author palvadi
 */
public final class AddPackageUtils extends Base{

    private static final Logger LOGGER = LoggerFactory.getLogger("AddPackage.AddPackage_Utils");

    /*
     * Variables defined for corresponding row numbers present in Add Package Test data sheet
     */
    public static final int IN_USE_PACKAGE = 1;
    public static final int OUT_OF_USE_PACKAGE = 2;
    public static final int DEPRECATED_PACKAGE = 3;
    public static final int FEATURE_HAS_SPECIAL_CHARACTERS = 5;
    public static final int FEATURE_IS_255_CHARACTERS = 6;
    public static final int FEATURE_IS_256_CHARACTERS = 7;
    public static final int FEATURE_IS_GREATER_THAN_256_CHARACTERS = 8;
    public static final int FEATURE_IS_NULL = 9;
    public static final int FEATURE_ID_WITHOUT_QUOTES = 10;
    public static final int FEATURE_ID_NOT_IN_DATABASE = 11;
    public static final int FEATURE_DOES_NOT_BELONG_TO_TENANT = 12;
    public static final int FEATURE_ID_IS_MISSING = 4;
    public static final int TENANT_HAS_SPECIAL_CHARACTERS = 13;
    public static final int TENANT_IS_255_CHARACTERS = 14;
    public static final int TENANT_IS_256_CHARACTERS = 15;
    public static final int TENANT_IS_GREATER_THAN_256_CHARACTERS = 16;
    public static final int TENANT_IS_NULL = 17;
    public static final int TENANT_NOT_IN_DATABASE = 18;
    public static final int TENANT_IS_MISSING = 19;
    public static final int X_REQUEST_ID_HAS_SPECIAL_CHARACTERS = 20;
    public static final int X_REQUEST_ID_IS_255_CHARACTERS = 21;
    public static final int X_REQUEST_ID_IS_256_CHARACTERS = 22;
    public static final int X_REQUEST_ID_IS_GREATER_THAN_256_CHARACTERS = 23;
    public static final int X_REQUEST_ID_IS_NULL = 24;
    public static final int X_REQUEST_ID_NOT_IN_DATABASE = 25;
    public static final int X_REQUEST_ID_IS_MISSING = 26;
    public static final int PACKAGE_ID_HAS_SPECIAL_CHARACTERS = 27;
    public static final int PACKAGE_ID_IS_255_CHARACTERS = 28;
    public static final int PACKAGE_ID_IS_256_CHARACTERS = 29;
    public static final int PACKAGE_ID_IS_GREATER_THAN_256_CHARACTERS = 30;
    public static final int PACKAGE_ID_IS_NULL = 31;
    public static final int PACKAGE_ID_IS_MISSING = 33;
    public static final int PACKAGE_ID_IN_DATABASE = 32;
    public static final int STATUS_IS_GIVEN_IN_WRONG_FORMAT_FOR_INUSE = 34;
    public static final int STATUS_IS_GIVEN_IN_WRONG_FORMAT_FOR_OUTOFUSE = 35;
    public static final int STATUS_IS_GIVEN_IN_WRONG_FORMAT_FOR_DEPRECATED = 36;
    public static final int STATUS_IS_GREATER_THAN_256_CHARACTERS = 37;
    public static final int STATUS_IS_NOT_AMONG_THREE_PRE_DEFINED = 38;
    public static final int STATUS_IS_NULL = 39;
    public static final int STATUS_GIVEN_WITHOUT_QUOTES = 40;
    public static final int STATUS_IS_256_CHARACTERS = 41;
    public static final int STATUS_IS_255_CHARACTERS = 42;
    public static final int STATUS_IS_MISSING = 83;
    public static final int CHECKSUM_TYPE_HAS_SPECIAL_CHARACTERS = 44;
    public static final int CHECKSUM_TYPE_IS_255_CHARACTERS = 45;
    public static final int CHECKSUM_TYPE_IS_256_CHARACTERS = 46;
    public static final int CHECKSUM_TYPE_IS_GREATER_THAN_256_CHARACTERS = 47;
    public static final int CHECKSUM_TYPE_IS_NULL = 48;
    public static final int CHECKSUM_TYPE_IS_MISSING = 49;
    public static final int CHECKSUM_VALUE_HAS_SPECIAL_CHARACTERS = 51;
    public static final int CHECKSUM_VALUE_IS_2047_CHARACTERS = 52;
    public static final int CHECKSUM_VALUE_IS_2048_CHARACTERS = 53;
    public static final int CHECKSUM_VALUE_IS_GREATER_THAN_1024_CHARACTERS = 54;
    public static final int CHECKSUM_VALUE_IS_NULL = 55;
    public static final int CHECKSUM_VALUE_IS_MISSING = 56;
    public static final int PARTNUMBER_HAS_SPECIAL_CHARACTERS = 58;
    public static final int PARTNUMBER_IS_ONE_BILLION_BILLION = 59;
    public static final int PARTNUMBER_IS_GREATER_THAN_ONE_BILLION_BILLION = 60;
    public static final int PARTNUMBER_IS_NULL = 61;
    public static final int TWO_VALID_PARTNUMBERS = 62;
    public static final int ONE_INVALID_PARTNUMBER = 63;
    public static final int PARTNUMBER_IS_MISSING = 57;
    public static final int URL_IS_GREATER_THAN_2048_CHARACTERS = 68;
    public static final int URL_IS_2048_CHARACTERS = 69;
    public static final int URL_IS_2047_CHARACTERS = 70;
    public static final int URL_IS_INVALID_FORMAT_1 = 71;
    public static final int URL_IS_INVALID_FORMAT_2 = 72;
    public static final int URL_IS_NULL = 73;
    public static final int URL_IS_MISSING = 74;
    public static final int SIZE_IS_2_BILLION = 75;
    public static final int SIZE_GREATER_THAN_2_BILLION = 76;
    public static final int SIZE_IS_HAVING_SPECIAL_CHARACTERS = 77;
    public static final int SIZE_IS_NULL = 78;
    public static final int SIZE_IS_ZERO = 79;
    public static final int SIZE_IS_MISSING = 85;
    public static final int METADATA_IS_NULL = 81;
    public static final int METADATA_IS_INVALID = 80;
    public static final int METADATA_MULTIPLE_CONTENT_ITEMS = 83;
    public static final int TENANT_NULL_AS_STRING = 84;
    public static final int METADATA_IS_GREATER_SIZE = 86;
    public static final int E2E_PACKAGE_ID = 87;
    /* Variables defined for test data Hash Map of the method 'testData' */
    public static final String PACKAGE_ID = "PackageId";
    public static final String FEATURE_ID = "FeatureId";
    public static final String STATE = "State";
    public static final String METADATA = "Metadata";
    public static final String PARTNUMBER = "PARTNUMBER";
    public static final String SIZE = "size";
    public static final String X_REQUEST_ID = "X_RequestId";
    public static final String CHECKSUM_TYPE = "Checksum_type";
    public static final String CHECKSUM_VALUE = "Checksum_value";
    public static final String URL = "URL";
    public static final String TENANT = "Tenant";

    public static final String SUCCESSRESPONSE = "successresponse";
    public static final String MALFORMEDRESPONSE = "malformedresponse";
    public static final String CLIENTUNAUTHORIZED = "clientunauthorized";
    public static final String FEATUREIDALREADYPRESENT = "featurealreadypresent";
    public static Object XRequestId = "xrequestid";
    public static Object Packageid = "packageId";
    public static Object HTTPSTATUSCODE = "httpstatuscode";
    public static Object HTTPSTATUSMESSAGE = "httpstatusmessage";
    public static final Object LOGFILENAME = "/var/log/aomsv2/AddPackage.log";
    public static final Object X_REQUEST_ID_RANDOM = "X_REQUEST_ID_RANDOM";
    public static final String METADATA_STRING = "<tns:metadata generationDate=\"2018-03-30T08:00:00\" schemaName=\"cpt-metadata-2.0.0\" schemaVersion=\"2.0.0\" type=\"TomTom_Content_Package_Metadata\" xmlns:tns=\"http://www.tomtom.com/ns/cpt/metadata/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.tomtom.com/ns/cpt/metadata/2 cpt-metadata-2.0.0.xsd \"><tns:package xsi:type=\"tns:ContentMapPackageType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CP-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:packageFormat>TTPKG</tns:packageFormat><tns:packageNumberOfFiles>1</tns:packageNumberOfFiles><tns:packageTotalSizeInBytes>2576441475</tns:packageTotalSizeInBytes><tns:packageUnpackedSizeInBytes>2575576261</tns:packageUnpackedSizeInBytes><tns:group xsi:type=\"tns:ContentMapGroupType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CG-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:groupTotalSizeInBytes>2575562034</tns:groupTotalSizeInBytes><tns:groupStorageSizeInBytes>2575564800</tns:groupStorageSizeInBytes><tns:item xsi:type=\"tns:ContentMapTTCItemType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CI-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:itemTotalSizeInBytes>2575560372</tns:itemTotalSizeInBytes><tns:itemStorageSizeInBytes>2575560704</tns:itemStorageSizeInBytes><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapItemFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList><tns:mapCensoringNumber>NotApplicable</tns:mapCensoringNumber><tns:mapPublicationNumber>NotApplicable</tns:mapPublicationNumber><tns:releaseNumber>1005</tns:releaseNumber><tns:releaseBuildNumber>8804</tns:releaseBuildNumber><tns:databaseFormatVersion>1003</tns:databaseFormatVersion><tns:fileName>Latam-101472.meta</tns:fileName><tns:encryptionServer>Durham-1.0</tns:encryptionServer></tns:item><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapGroupFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList></tns:group><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapPackageFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList></tns:package></tns:metadata>";
    public static Object CATEGORY = "category";
    public static Response response;

    
    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }

    @AfterMethod
    public void printline() throws InterruptedException {
        LOGGER.info("-------------------------------------------------------------------------------------------------------------------------------------------");
    }
    
    public static HashMap<Object, Object> addPackageTestData(int Rownum) throws IOException {
        /* This method gets input from AddPackage test data Excel sheet and puts in HashMap  */
        HashMap<Object, Object> hm = new HashMap<Object, Object>();
        Random rd = new Random();
        String sALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * sALTCHARS.length());
            salt.append(sALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();

        int randInt1 = rd.nextInt(10000);

        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream("testData/AddPackage.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("AddPackage");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell packageId = currentRow.getCell(1);
        String pckgId = packageId.toString();
        if (pckgId.equals("NULL")) {
            hm.put(PACKAGE_ID, null);
        } else if (pckgId.equals("IamInDatabase")) {
            hm.put(PACKAGE_ID, "IamInDatabase");
        } else if (pckgId.equals("MISSING")) {
            hm.put(PACKAGE_ID, "MISSING");
        } else {
            String packageIdStr = pckgId + randInt1 + saltStr;
            hm.put(PACKAGE_ID, packageIdStr);
        }

        XSSFCell featureId = currentRow.getCell(2);
        String featureIdStr = featureId.toString();
        if (featureIdStr.equals("NULL")) {
            hm.put(FEATURE_ID, null);
        } else {
            hm.put(FEATURE_ID, featureIdStr);

        }

        XSSFCell stateCell = currentRow.getCell(3);
        String state = stateCell.toString();
        if (state.equals("NULL")) {
            hm.put(STATE, null);
        }
        hm.put(STATE, state);

        XSSFCell metadataCell = currentRow.getCell(4);
        String metadata = metadataCell.toString();
        if (metadata.equals("NULL")) {
            hm.put(METADATA, null);
        } else {
            hm.put(METADATA, metadata);
        }

        XSSFCell checksumType = currentRow.getCell(5);
        String checksumtype = checksumType.toString();

        if (checksumtype.equals("NULL")) {
            hm.put(CHECKSUM_TYPE, null);
        } else {
            hm.put(CHECKSUM_TYPE, checksumtype);
        }

        XSSFCell checksumValue = currentRow.getCell(6);
        String checksumvalue = checksumValue.toString();
        if (checksumvalue.equals("NULL")) {
            hm.put(CHECKSUM_VALUE, null);
        } else {
            hm.put(CHECKSUM_VALUE, checksumvalue);
        }

        XSSFCell url = currentRow.getCell(7);
        String Url = url.toString();

        hm.put(URL, Url);

        XSSFCell sizeCell = currentRow.getCell(8);
        String size = sizeCell.toString();

        boolean matchessize = Pattern.matches("[$&+,:;=?@#|'<>-^*()%!]+", size);

        if (size.equals("NULL")) {
            hm.put(SIZE, null);
        } else if (size.equals("0.0")) {
            hm.put(SIZE, 0);
        } else if (matchessize == true) {
            hm.put(SIZE, size);
        } else if (size.equals("MISSING")) {
            hm.put(SIZE, size);
        } else {
            long sizelong = Long.parseLong(size);
            hm.put(SIZE, sizelong);
        }

        XSSFCell partNumberCell = currentRow.getCell(9);

        String partnumber = partNumberCell.toString();
        boolean matchespartnumber = Pattern.matches("[$&+,:;=?@#|'<>-^*()%!]+", partnumber);

        if (partnumber.equals("NULL")) {
            hm.put(PARTNUMBER, null);
        } else if (matchespartnumber == true) {
            hm.put(PARTNUMBER, partnumber);
        } else if (partnumber.equals("MISSING")) {
            hm.put(PARTNUMBER, partnumber);
        } else if (partnumber.equals("DECIMAL")) {
            hm.put(PARTNUMBER, "234.00");
        } else {
            long partNumber = Long.parseLong(partnumber);
            hm.put(PARTNUMBER, partNumber);
        }

        XSSFCell tenantCell = currentRow.getCell(10);
        String tenant = tenantCell.toString();

        hm.put(TENANT, tenant);

        XSSFCell xrequestidCell = currentRow.getCell(11);
        String XRequestid = xrequestidCell.toString();

        String xRequestId_random = randInt1 + saltStr;

        hm.put(X_REQUEST_ID, XRequestid);
        hm.put(X_REQUEST_ID_RANDOM, xRequestId_random);
        wb.close();
        return hm;
    }

    public static String jsonMapping(HashMap<Object, Object> testdata) {
        /* This method maps the testdata to JSON request body */
        List<JSONObject> list;

        Checksum cs = new Checksum(); 
        if (testdata.get(AddPackageUtils.CHECKSUM_TYPE) == null) {
            cs.setType("");
        } else if (testdata.get(AddPackageUtils.CHECKSUM_TYPE).equals("MISSING")) {
            cs.setType(null);
        } else {
            cs.setType(testdata.get(AddPackageUtils.CHECKSUM_TYPE));
        }

        if (testdata.get(AddPackageUtils.CHECKSUM_VALUE) == null) {
            cs.setValue("");
        } else if (testdata.get(AddPackageUtils.CHECKSUM_VALUE).equals("MISSING")) {
            cs.setValue(null);
        } else {
            cs.setValue(testdata.get(AddPackageUtils.CHECKSUM_VALUE));
        }

        List<Checksum> checksumlist = new ArrayList<Checksum>();
        checksumlist.add(cs);

        DownloadInfo di = new DownloadInfo();
        di.setChecksum(cs);

        if (testdata.get(AddPackageUtils.PARTNUMBER) == null) {
            di.setPartNumber("");
        } else if (testdata.get(AddPackageUtils.PARTNUMBER).equals("MISSING")) {
            di.setPartNumber(null);
        } else {
            di.setPartNumber(testdata.get(PARTNUMBER));
        }

        if (testdata.get(AddPackageUtils.SIZE) == null) {
            di.setSize("");
        } else if (testdata.get(AddPackageUtils.SIZE).equals("MISSING")) {
            di.setSize(null);
        } else if (testdata.get(AddPackageUtils.SIZE).equals("123")) {
            di.setSize(1234);
        } else {
            di.setSize(testdata.get(SIZE));
        }

        if (testdata.get(URL) == "null") {
            di.setUrl("");
        } else if (testdata.get(URL).equals("MISSING")) {
            di.setUrl(null);
        } else {
            di.setUrl(testdata.get(URL));

        }

        List<DownloadInfo> downloadinfolist = new ArrayList<DownloadInfo>();
        downloadinfolist.add(di);

        AddPackage requestdata = new AddPackage();

        if (testdata.get(PACKAGE_ID) == null) {
            requestdata.setPackageId("");
        } else if (testdata.get(PACKAGE_ID).equals("MISSING")) {
            requestdata.setPackageId(null);
        } else {
            requestdata.setPackageId(testdata.get(PACKAGE_ID));
        }

        if (testdata.get(FEATURE_ID) == null) {
            requestdata.setFeatureId("");
        } else if (testdata.get(FEATURE_ID).equals("MISSING")) {
            requestdata.setFeatureId(null);
        } else {
            requestdata.setFeatureId(testdata.get(FEATURE_ID));
        }

        if (testdata.get(STATE) == null) {
            requestdata.setState("");
        } else if (testdata.get(STATE).equals("MISSING")) {
            requestdata.setState(null);
        } else {
            requestdata.setState(testdata.get(STATE));
        }

        if (testdata.get(METADATA) == null) {
            requestdata.setMetadata("");
        } else if (testdata.get(METADATA).equals("MISSING")) {
            requestdata.setMetadata(null);
        } else {
            requestdata.setMetadata(METADATA_STRING);
        }
        requestdata.setDownloadinfo(downloadinfolist);

        JSONObject jsonobject = new JSONObject(requestdata);
        list = new ArrayList<JSONObject>();
        list.add(jsonobject);
        String reqbody = list.toString();
        int strlength = reqbody.length();
        String requestbody = reqbody.substring(1, strlength - 1);

        return requestbody;
    }


    public static Response addPackage(HashMap<Object, Object> testdata, boolean apigeeFlag) {
        
        /* This method is for triggering the request and fetching the response  */
        String requestBodyStr = jsonMapping(testdata);
        String requestbody = '[' + requestBodyStr + ']';
        LOGGER.debug("The request body is  " + requestbody);
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_PACKAGE);
            LOGGER.debug("The TenantId set for the request is" + testdata.get(TENANT));
            LOGGER.debug("The XRequestId set for the request is " + testdata.get(X_REQUEST_ID));

        }

        return response;
    }

    public static Response addPackagewhenRepeated(HashMap<Object, Object> testdata, String requestbody,
            boolean apigeeFlag) {
        
        /* This method is for triggering the request and fetching the response when any parameter is repeated */
        
        LOGGER.debug("The request body is  " + requestbody);
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set for the request is" + testdata.get(TENANT));
            LOGGER.debug("The XRequestId set for the request is " + testdata.get(X_REQUEST_ID));

        }

        return response;

    }

    public static Response addPackageTenantValidations(HashMap<Object, Object> testdata) {
        
        /* This method is for triggering the request for different Tenant Validations  */
        
        String requestBodyStr = jsonMapping(testdata);
        String requestbody = '[' + requestBodyStr + ']';
        LOGGER.debug("The request body is  " + requestbody);

        String tenantId = (String) testdata.get(TENANT);
        switch (tenantId) {

        case "NULL":

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", "")
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId is set Null ");
            LOGGER.debug("The XRequestId set for the request is " + testdata.get(X_REQUEST_ID));
            break;

        case "NULLASSTRING":

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", "null")
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set for the request is: 'null'");
            LOGGER.debug("The XRequestId set for the request is " + testdata.get(X_REQUEST_ID));
            break;

        case "MISSING":

            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId is not set");
            LOGGER.debug("The XRequestId set for the request is " + testdata.get(X_REQUEST_ID));
            break;

        default:

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID_RANDOM)).when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set for the request is" + testdata.get(TENANT));
            LOGGER.debug("The XRequestId set for the request is " + testdata.get(X_REQUEST_ID_RANDOM));
            break;

        }

        return response;

    }

    public static Response addPackageXRequestIdValidations(HashMap<Object, Object> testdata) {
        
        /* This method is for triggering the request for different XRequest Validations  */
        
        String requestBodyStr = jsonMapping(testdata);
        String requestbody = '[' + requestBodyStr + ']';
        LOGGER.debug("The request body is  " + requestbody);

        String xrequestId = (String) testdata.get(X_REQUEST_ID);
        switch (xrequestId) {

        case "NULL":

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id", "").when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set for the request is" + testdata.get(TENANT));
            LOGGER.debug("The XRequestId is set as Null");
            break;

        case "NULLASSTRING":

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id", "null").when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set for the request is" + testdata.get(TENANT));
            LOGGER.debug("The XRequestId is set as 'null'");
            break;

        case "MISSING":

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set for the request is" + testdata.get(TENANT));
            LOGGER.debug("The XRequestId is not set");
            break;

        case "GREATERTHAN256CHARACTERS":

            response = given()
                    .relaxedHTTPSValidation()
                    .body(requestbody)
                    .header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id",
                            "Hdsafdsafjgsadhfgadshfhdsahhjjsdfisdfj;sadfj;oasdfojsfkjdskfjakdsfjklsadjflkjdsafldsjfadsfjklsadjflksajdlkfjsalkdjflksadjflkjdslkfjlkdsajflksadjflkjdsafiewroiweoruewoiuroiewuroiweuoiruewoiruoiewuroiewkrewnndkjffjkahkjfhaksjdhfkjahsfkjhdsakjfhdkjashfkjajdskfjakfk;ldasfkdaskjfhkjdahkjdsfdsdff")
                    .when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set for the request is" + testdata.get(TENANT));
            LOGGER.debug("The XRequestId is: Hdsafdsafjgsadhfgadshfhdsahhjjsdfisdfj;sadfj;oasdfojsfkjdskfjakdsfjklsadjflkjdsafldsjfadsfjklsadjflksajdlkfjsalkdjflksadjflkjdslkfjlkdsajflksadjflkjdsafiewroiweoruewoiuroiewuroiweuoiruewoiruoiewuroiewkrewnndkjffjkahkjfhaksjdhfkjahsfkjhdsakjfhdkjashfkjajdskfjakfk;ldasfkdaskjfhkjdahkjdsfdsdff");
            break;

        default:

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id", testdata.get(AddPackageUtils.X_REQUEST_ID_RANDOM)).when()
                    .put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set for the request is" + testdata.get(TENANT));
            LOGGER.debug("The XRequestId set for the request is " + testdata.get(X_REQUEST_ID_RANDOM));
            break;
        }

        return response;

    }

    public static void validateSuccessResponse(Response response) {
        
        /* This method validates the success response codes and messages from the response  */

        response.then().assertThat().statusCode(ResponseCodes.ADD_PACKAGE_SUCCESS_RESPONSE_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.ADD_PACKAGE_CREATED);

        LOGGER.debug("The Response is " + Messages.ADD_PACKAGE_CREATED);
        LOGGER.debug("Content Type is " + ContentType.JSON);

    }

    public static void validateMalformedRequest(Response response) {
        
        /* This method validates the bad request response codes and messages from the response  */
        
        response.then().assertThat().statusCode(ResponseCodes.ADD_FEATURE_MALFORMED_REQUEST_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.ADD_PACKAGE_MALFORMED_REQUEST);

        LOGGER.debug("The Response is " + Messages.ADD_PACKAGE_MALFORMED_REQUEST);

    }

    public static void validateFeatureNotFound(Response response) {
        
        /* This method validates the feature not founf response codes and messages from the response  */
        
        response.then().assertThat().statusCode(ResponseCodes.FETCH_FEATURE_NO_SUCH_FEATURE_FOUND).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.ADD_PACKAGE_FEATURE_ID_NOT_FOUND);

        LOGGER.debug("The Response is " + Messages.ADD_PACKAGE_FEATURE_ID_NOT_FOUND);
        LOGGER.debug("Content Type is " + ContentType.JSON);

    }

    public static void validatePackageAleadyExists(Response response) {
        
        /* This method validates the package already exists response codes and messages from the response  */
        
        response.then().assertThat().statusCode(ResponseCodes.ADD_PACKAGE_ALREADY_EXISTS).and().assertThat()
                .contentType(ContentType.JSON).and().statusLine(Messages.ADD_PACKAGE_ALREADY_EXISTS);

        LOGGER.debug("The Response is " + Messages.ADD_PACKAGE_ALREADY_EXISTS);
        LOGGER.debug("Content Type is " + ContentType.JSON);

    }

    public static void validateClientUnauthorizedResponse(Response response) {
        
        /* This method validates the client unauthorized response codes and messages from the response  */
        
        response.then().assertThat().statusCode(ResponseCodes.ADD_PACKAGE_CLIENT_UNAUTHORIZED).and().assertThat()
                .contentType(ContentType.JSON).and().statusLine(Messages.ADD_PACKAGE_CLIENT_UNAUTHORIZED);

        LOGGER.debug("The Response is " + Messages.ADD_PACKAGE_CLIENT_UNAUTHORIZED);
        LOGGER.debug("Content Type is " + ContentType.JSON);

    }

    public static Response addPackageTwoPartNumbers(HashMap<Object, Object> testdata, boolean apigeeFlag) {
        
        /* This method creates the request body for package with two part numbers and triggers the request   */

        List<JSONObject> list;
        Checksum cs = new Checksum();
        if (testdata.get(AddPackageUtils.CHECKSUM_TYPE) == null) {
            cs.setType(null);
        } else {
            cs.setType(testdata.get(AddPackageUtils.CHECKSUM_TYPE));
        }

        if (testdata.get(AddPackageUtils.CHECKSUM_VALUE) == null) {
            cs.setType(null);
        } else {
            cs.setValue(testdata.get(AddPackageUtils.CHECKSUM_VALUE));
        }

        List<Checksum> checksumlist = new ArrayList<Checksum>();
        checksumlist.add(cs);

        DownloadInfo di = new DownloadInfo();
        di.setChecksum(cs);
        di.setPartNumber(testdata.get(PARTNUMBER));
        di.setSize(testdata.get(SIZE));
        di.setUrl(testdata.get(URL));

        List<DownloadInfo> downloadinfolist1 = new ArrayList<DownloadInfo>();
        List<DownloadInfo> downloadinfolist2 = new ArrayList<DownloadInfo>();
        downloadinfolist1.add(di);
        downloadinfolist1.add(di);
        List<DownloadInfo> downloadinfolist = new ArrayList<DownloadInfo>();
        downloadinfolist.addAll(downloadinfolist1);
        downloadinfolist.addAll(downloadinfolist2);

        AddPackage requestdata = new AddPackage();
        if (testdata.get(PACKAGE_ID) == null) {
            requestdata.setPackageId(null);
        } else {
            requestdata.setPackageId(testdata.get(PACKAGE_ID));
        }

        if (testdata.get(FEATURE_ID) == null) {
            requestdata.setFeatureId(null);
        } else {
            requestdata.setFeatureId(testdata.get(FEATURE_ID));
        }

        if (testdata.get(STATE) == null) {
            requestdata.setState(null);
        } else {
            requestdata.setState(testdata.get(STATE));
        }
        requestdata.setMetadata(METADATA_STRING);
        requestdata.setDownloadinfo(downloadinfolist);

        JSONObject jsonobject = new JSONObject(requestdata);
        list = new ArrayList<JSONObject>();
        list.add(jsonobject);
        String reqbody = list.toString();
        int strlength = reqbody.length();
        String requestbody = '[' + reqbody.substring(1, strlength - 1) + ']';

        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set for the request is" + testdata.get(TENANT));
            LOGGER.debug("The XRequestId set for the request is " + testdata.get(X_REQUEST_ID));
        }

        return response;
    }

    public static Response addPackageWithTwoDifferentPartNumberValues(HashMap<Object, Object> testdata,
            boolean apigeeFlag) {
        
        /* This method creates the request body for package with two part numbers and triggers the request   */

        
        List<JSONObject> list;
        Random rd = new Random();
        int randomnum = rd.nextInt(10000);
        Checksum cs = new Checksum();
        if (testdata.get(AddPackageUtils.CHECKSUM_TYPE) == null) {
            cs.setType(null);
        } else {
            cs.setType(testdata.get(AddPackageUtils.CHECKSUM_TYPE));
        }

        if (testdata.get(AddPackageUtils.CHECKSUM_VALUE) == null) {
            cs.setType(null);
        } else {
            cs.setValue(testdata.get(AddPackageUtils.CHECKSUM_VALUE));
        }

        List<Checksum> checksumlist = new ArrayList<Checksum>();
        checksumlist.add(cs);

        DownloadInfo di1 = new DownloadInfo();
        di1.setChecksum(cs);
        di1.setPartNumber(testdata.get(PARTNUMBER));
        di1.setSize(randomnum);
        di1.setUrl(testdata.get(URL));

        DownloadInfo di2 = new DownloadInfo();
        di2.setChecksum(cs);
        int partnumber2 = randomnum + randomnum;
        di2.setPartNumber(partnumber2);
        di2.setSize(testdata.get(AddPackageUtils.SIZE));
        di2.setUrl(testdata.get(URL));
        List<DownloadInfo> downloadinfolist1 = new ArrayList<DownloadInfo>();
        List<DownloadInfo> downloadinfolist2 = new ArrayList<DownloadInfo>();
        downloadinfolist1.add(di1);
        downloadinfolist1.add(di2);
        List<DownloadInfo> downloadinfolist = new ArrayList<DownloadInfo>();
        downloadinfolist.addAll(downloadinfolist1);
        downloadinfolist.addAll(downloadinfolist2);
        AddPackage requestdata = new AddPackage();
        if (testdata.get(PACKAGE_ID) == null) {
            requestdata.setPackageId(null);
        } else {
            requestdata.setPackageId(testdata.get(PACKAGE_ID));
        }

        if (testdata.get(FEATURE_ID) == null) {
            requestdata.setFeatureId(null);
        } else {
            requestdata.setFeatureId(testdata.get(FEATURE_ID));
        }

        if (testdata.get(STATE) == null) {
            requestdata.setState(null);
        } else {
            requestdata.setState(testdata.get(STATE));
        }
        requestdata.setMetadata(METADATA_STRING);
        requestdata.setDownloadinfo(downloadinfolist);

        JSONObject jsonobject = new JSONObject(requestdata);
        list = new ArrayList<JSONObject>();
        list.add(jsonobject);
        String reqbody = list.toString();
        int strlength = reqbody.length();
        String requestbody = '[' + reqbody.substring(1, strlength - 1) + ']';

        LOGGER.debug("The request body is " + requestbody);

        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set for the request is" + testdata.get(TENANT));
            LOGGER.debug("The XRequestId set for the request is " + testdata.get(X_REQUEST_ID));
        }

        return response;
    }

    public static Response setInvalidMetaData1(HashMap<Object, Object> testdata, boolean apigeeFlag) {
        
        /* This method creates the request body for invalid metadata   */

        
        List<JSONObject> list;
        Random rd = new Random();
        int randomnum = rd.nextInt(10000);
        Checksum cs = new Checksum();
        if (testdata.get(AddPackageUtils.CHECKSUM_TYPE) == null) {
            cs.setType(null);
        } else {
            cs.setType(testdata.get(AddPackageUtils.CHECKSUM_TYPE));
        }

        if (testdata.get(AddPackageUtils.CHECKSUM_VALUE) == null) {
            cs.setType(null);
        } else {
            cs.setValue(testdata.get(AddPackageUtils.CHECKSUM_VALUE));
        }

        List<Checksum> checksumlist = new ArrayList<Checksum>();
        checksumlist.add(cs);

        DownloadInfo di = new DownloadInfo();
        di.setChecksum(cs);
        di.setPartNumber(randomnum);
        di.setSize(randomnum);
        di.setUrl(testdata.get(URL));
        List<DownloadInfo> downloadinfolist = new ArrayList<DownloadInfo>();
        downloadinfolist.add(di);

        AddPackage requestdata = new AddPackage();
        if (testdata.get(PACKAGE_ID) == null) {
            requestdata.setPackageId(null);
        } else {
            requestdata.setPackageId(testdata.get(PACKAGE_ID));
        }

        if (testdata.get(FEATURE_ID) == null) {
            requestdata.setFeatureId(null);
        } else {
            requestdata.setFeatureId(testdata.get(FEATURE_ID));
        }

        if (testdata.get(STATE) == null) {
            requestdata.setState(null);
        } else {
            requestdata.setState(testdata.get(STATE));
        }
        String metadata = (String) testdata.get(METADATA);
        int length = metadata.length();
        String invalidMetadata = metadata.substring(1, length);
        requestdata.setMetadata(invalidMetadata);
        requestdata.setDownloadinfo(downloadinfolist);

        JSONObject jsonobject = new JSONObject(requestdata);
        list = new ArrayList<JSONObject>();
        list.add(jsonobject);
        String reqbody = list.toString();
        int strlength = reqbody.length();
        String requestbody = '[' + reqbody.substring(1, strlength - 1) + ']';

        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_PACKAGE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id", testdata.get(AddPackageUtils.X_REQUEST_ID)).when()
                    .put(CommonData.ADD_PACKAGE);
        }

        return response;
    }

    public static Response setInvalidMetaData(HashMap<Object, Object> testdata, boolean apigeeFlag) {
        
        /* This method creates the request body for invalid metadata */


        List<JSONObject> list;
        Random rd = new Random();
        int randomnum = rd.nextInt(10000);
        Checksum cs = new Checksum();
        if (testdata.get(AddPackageUtils.CHECKSUM_TYPE) == null) {
            cs.setType(null);

        } else {
            cs.setType(testdata.get(AddPackageUtils.CHECKSUM_TYPE));

        }

        if (testdata.get(AddPackageUtils.CHECKSUM_VALUE) == null) {
            cs.setType(null);
        } else {
            cs.setValue(testdata.get(AddPackageUtils.CHECKSUM_VALUE));
        }

        List<Checksum> checksumlist = new ArrayList<Checksum>();
        checksumlist.add(cs);

        DownloadInfo di = new DownloadInfo();
        di.setChecksum(cs);
        di.setPartNumber(randomnum);
        di.setSize(randomnum);
        di.setUrl(testdata.get(URL));
        List<DownloadInfo> downloadinfolist = new ArrayList<DownloadInfo>();
        downloadinfolist.add(di);

        AddPackage requestdata = new AddPackage();
        if (testdata.get(PACKAGE_ID) == null) {
            requestdata.setPackageId(null);
        } else {
            requestdata.setPackageId(testdata.get(PACKAGE_ID));
        }

        if (testdata.get(FEATURE_ID) == null) {
            requestdata.setFeatureId(null);
        } else {
            requestdata.setFeatureId(testdata.get(FEATURE_ID));
        }

        if (testdata.get(STATE) == null) {
            requestdata.setState(null);
        } else {
            requestdata.setState(testdata.get(STATE));
        }
        requestdata.setMetadata(testdata.get(METADATA_STRING) + "aDSFADSF");
        requestdata.setDownloadinfo(downloadinfolist);

        JSONObject jsonobject = new JSONObject(requestdata);
        list = new ArrayList<JSONObject>();
        list.add(jsonobject);
        String reqbody = list.toString();
        int strlength = reqbody.length();
        String requestbody = '[' + reqbody.substring(1, strlength - 1) + ']';

        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_PACKAGE);
        } else {

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(TENANT))
                    .header("X-Request-Id", testdata.get(AddPackageUtils.X_REQUEST_ID)).when()
                    .put(CommonData.ADD_PACKAGE);
        }

        return response;
    }

    public static boolean validateXMLSchema(String xsdPath, String xmlPath) throws SAXException, IOException {
        
        /* This method validates the metadata XML against XSD given as part of requirement*/

        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));
            LOGGER.debug("Metadata xml is validated against cpt metadata xsd file");
        } catch (IOException e) {
            LOGGER.debug("IO Exception in validateXMLSchema is: " + e.getMessage());
            e.printStackTrace();
            return false;
        } catch (SAXException ex) {
            LOGGER.debug("SAX Exception in validateXMLSchema is:" + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public static void verifylogsforMalformedResponse(Object packageId, Object xRequestId)
            throws UnrecoverableKeyException, KeyManagementException, MalformedURLException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, IOException {



        try {

            HTTPSTATUSCODE = "HttpStatusCode: 400";
            HTTPSTATUSMESSAGE = "Bad Request";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + packageId
                    + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + " \"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
                    + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The search query is: " + urlParameters);

            CommonMethods.verifyLogsfromKibana(urlParameters);
        } catch (Exception e) {
            e.printStackTrace();
        } 

    }

    public static void verifylogsforClientUnauthorized(Object packageId, Object xRequestId)
            throws UnrecoverableKeyException, KeyManagementException, MalformedURLException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, IOException {



        try {

            HTTPSTATUSCODE = "HttpStatusCode: 403";
            HTTPSTATUSMESSAGE = "Client Unauthorized";
            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + packageId
                    + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + " \"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
                    + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The search query is: " + urlParameters);

            CommonMethods.verifyLogsfromKibana(urlParameters);
            } catch (Exception e) {
                e.printStackTrace();
            } 


    }

    public static void verifylogsforPackageAlreadyPresent(Object packageId, Object xRequestId)
            throws UnrecoverableKeyException, KeyManagementException, MalformedURLException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, IOException {

        try {
            HTTPSTATUSCODE = "HttpStatusCode: 409";
            HTTPSTATUSMESSAGE = "Package with the given ID already exists";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + packageId
                    + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + " \"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
                    + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The search query is: " + urlParameters);

            CommonMethods.verifyLogsfromKibana(urlParameters);
            } catch (Exception e) {
                e.printStackTrace();
            } 

    }

    public static void verifylogsforSuccessResponse(Object packageId, Object xRequestId) throws MalformedURLException,
            IOException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException {


        try {

            XRequestId = packageId;
            Packageid = xRequestId;

            HTTPSTATUSCODE = "HttpStatusCode: 204";
            HTTPSTATUSMESSAGE = "Package is created successfully";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + packageId
                    + "\"}},{\"match\":{\"message\":\"INFO\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
                    + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The search query is: " + urlParameters);
            CommonMethods.verifyLogsfromKibana(urlParameters);
            } catch (Exception e) {
                e.printStackTrace();
            } 


    }
    
    public static Object getPackageId(HashMap<Object, Object> hm) {

        Object PackageId = hm.get(AddPackageUtils.PACKAGE_ID);
        return PackageId;

    }

    public static Object getXRequestId(HashMap<Object, Object> hm) {

        Object XRequestId = hm.get(AddPackageUtils.X_REQUEST_ID);
        return XRequestId;

    }

}