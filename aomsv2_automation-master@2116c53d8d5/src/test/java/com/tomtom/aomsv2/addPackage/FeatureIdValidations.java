/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.addPackage;

import org.testng.annotations.Test;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackage;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackageTestData;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.validateSuccessResponse;

import com.tomtom.aomsv2.commonControls.CommonData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for Feature Id field for Add Package API and verifying the
 * response through ELB/Nodes.
 * 
 * @author palvadi
 */
public class FeatureIdValidations {
    
    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    

    private static final Logger LOGGER = LoggerFactory.getLogger("AddPackage.FeatureIdValidations");
    HashMap<Object, Object> addPackageTestData;


    @Test(groups = "AddPackage")
    public void addPackageFeatureIdNotInDatabase() throws IOException {

        LOGGER.info("********************Starting TestCase \"addPackageFeatureIdNotInDatabase\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null; 
    }

    @Test(groups = "AddPackage")
    public void addPackageWhenFeatureIdisNull() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWhenFeatureIdisNull\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.FEATURE_IS_NULL);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null; 
    }

    @Test(groups = "AddPackage")
    public void addPackageWhenFeatureIdGreaterThan256Characters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWhenFeatureIdGreaterThan256Characters\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.FEATURE_IS_GREATER_THAN_256_CHARACTERS);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null; 

    }

    @Test(groups = "AddPackage")
    public void addPackageWhenFeatureIdis256Characters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWhenFeatureIdis256Characters\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.FEATURE_IS_256_CHARACTERS);
        Response response = addPackage(addPackageTestData, false);
        validateSuccessResponse(response);
        addPackageTestData = null; 

    }

    @Test(groups = "AddPackage")
    public void addPackageFeatureIdis255Characters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageFeatureIdis255Characters\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.FEATURE_IS_255_CHARACTERS);
        Response response = addPackage(addPackageTestData, false);
        validateSuccessResponse(response);
        addPackageTestData = null; 

    }

    @Test(groups = "AddPackage")
    public void addPackageFeatureIdhasSpecialCharacters() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("********************Starting TestCase \"addPackageFeatureIdhasSpecialCharacters\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.FEATURE_HAS_SPECIAL_CHARACTERS);
        Response response = addPackage(addPackageTestData, false);
        validateSuccessResponse(response);
        addPackageTestData = null; 

    }

    @Test(groups = "AddPackage")
    public void addPackageFeaturedoesNotBelongToTenant() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageFeaturedoesNotBelongToTenant\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.FEATURE_DOES_NOT_BELONG_TO_TENANT);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;

    }

    @Test(groups = "AddPackage")
    public void addPackageFeaturedIsMissing() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageFeaturedIsMissing\"********************");

        addPackageTestData = addPackageTestData(AddPackageUtils.FEATURE_ID_IS_MISSING);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;

    }

}
