/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.addPackage;

import org.testng.annotations.Test;
import com.tomtom.aomsv2.commonControls.CommonData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for Package Id field for Add Package API and verifying the
 * response through ELB/Nodes.
 * 
 * @author palvadi
 */
public class PartNumberValidations {

    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    
    
    private static final Logger LOGGER = LoggerFactory.getLogger("AddPackage.PartNumberValidations");
    HashMap<Object, Object> addPackageTestData;

    @Test(groups = "AddPackage")
    public void addPackageWithTwoSamePartNumbers() throws IOException {
        LOGGER.info("********************Starting TestCase \"AddPackagewithTwoSamePartNumbers\"********************");
        addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.IN_USE_PACKAGE);
        Response response = AddPackageUtils.addPackageTwoPartNumbers(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWithTwoDifferentPartNumbers() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("********************Starting TestCase \"AddPackagewithTwoDifferentPartNumbers\"********************");
        addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.TWO_VALID_PARTNUMBERS);
        Response response = AddPackageUtils.addPackageWithTwoDifferentPartNumberValues(addPackageTestData, false);
        AddPackageUtils.validateSuccessResponse(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberNotanInteger() throws IOException {
        LOGGER.info("********************Starting TestCase \"AddPackagewithPartNumbernotanInteger\"********************");

        addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.ONE_INVALID_PARTNUMBER);
        Response response = AddPackageUtils.addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberasNull() throws IOException {
        LOGGER.info("********************Starting TestCase \"AddPackagewithPartNumberasNull\"********************");

        addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.PARTNUMBER_IS_NULL);
        Response response = AddPackageUtils.addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberis1BillionBillionCharacters() throws IOException {
        LOGGER.info("********************Starting TestCase \"AddPackagewithPartNumberis1BillionBillionCharacters\"********************");

        addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.PARTNUMBER_IS_ONE_BILLION_BILLION);
        Response response = AddPackageUtils.addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberisgreaterthan1BillionBillionCharacters() throws IOException {
        LOGGER.info("********************Starting TestCase \"AddPackagewithPartNumberis1BillionBillionCharacters\"********************");

        addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.PARTNUMBER_IS_GREATER_THAN_ONE_BILLION_BILLION);
        Response response = AddPackageUtils.addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberisSpecialCharacters() throws IOException {
        LOGGER.info("********************Starting TestCase \"AddPackagewithPartNumberisSpecialCharacters\"********************");

        addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.PARTNUMBER_HAS_SPECIAL_CHARACTERS);
        Response response = AddPackageUtils.addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;
    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberIsMissing() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWithPartNumberIsMissing\"********************");

        addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.PARTNUMBER_IS_MISSING);
        Response response = AddPackageUtils.addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);
        addPackageTestData = null;
    }

}
