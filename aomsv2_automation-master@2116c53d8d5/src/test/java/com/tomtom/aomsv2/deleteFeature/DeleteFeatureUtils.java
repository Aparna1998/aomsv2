/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.deleteFeature;

import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.addPackage.AddPackageUtils;
import com.tomtom.aomsv2.commonControls.Base;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.commonControls.Messages;
import com.tomtom.aomsv2.commonControls.ResponseCodes;
import com.tomtom.aomsv2.request.entity.AddFeatureRequestNew;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Random;

/**
 * This class has the common methods for Delete Feature API which are referenced by all other test cases.
 * 
 * @author palvadi
 */
public class DeleteFeatureUtils extends Base {

    private static final Logger LOGGER = LoggerFactory.getLogger("DeleteFeature.DeleteFeatureUtils");

    /* Variables defined for test data Hash Map of the method 'testData' */
    public static final Object FEATURE_ID_DELETE_FEATURE = "Feature";
    public static final Object TENANT_ID_DELETE_FEATURE = "Tena";
    public static final Object X_REQUEST_ID_DELETE_FEATURE = "XRequest";

    public static final String FEATURE_ID_ADD_FEATURE = "FeatureId";
    public static final Object DESCRIPTION_ADD_FEATURE = "Description";
    public static final Object TENANT_ID_ADD_FEATURE = "X_Tenant_Id";
    public static final Object X_REQUEST_ID_ADD_FEATURE = "X_Request_Id";

    public static final String PACKAGE_ID_ADD_PACKAGE = "PackageId";
    public static final String FEATURE_ID_ADD_PACKAGE = "FeatureId";
    public static final String STATE = "State";
    public static final String METADATA = "Metadata";
    public static final int PARTNUMBER = 1;
    public static final String SIZE = "size";
    public static final String X_REQUEST_ID = "X_RequestId";
    public static final Object CHECKSUM_TYPE = "Checksum_type";
    public static final Object CHECKSUM_VALUE = "Checksum_value";
    public static final Object URL = "URL";
    public static final int SIZE_VALUE = 1;
    public static final String TENANT = "Tenant";
    public static final String X_REQUEST_ID_ADD_PACKAGE = "XRequestId";
    public static final String TENANT_ID = "df";

    /*
     * Variables defined for corresponding row numbers present in Add Feature Test data sheet
     */
    public static final int ADD_PACKAGE_PACKAGE_AND_FEATURE_LINKED_CASE = 1;
    public static final int ADD_PACKAGE_VERIFY_PACKAGE_AND_CONTENT_ITEM_ARE_DELETED = 2;

    public static final int ADD_FEATURE_VALID_DATA = 1;
    public static final int ADD_FEATURE_FEATURE_ID_IS_256_CHARACTERS = 2;
    public static final int ADD_FEATURE_FEATURE_ID_IS_255_CHARACTERS = 3;
    public static final int ADD_FEATURE_TENANT_IS_256_CHARACTERS = 4;
    public static final int ADD_FEATURE_TENANT_IS_255_CHARACTERS = 5;
    public static final int ADD_FEATURE_PACKAGE_AND_FEATURE_LINKED_CASE = 6;
    public static final int ADD_FEATURE_VERIFY_PACKAGE_AND_CONTENT_ITEM_ARE_DELETED = 7;

    public static final int DELETE_FEATURE_VALID_DATA = 1;
    public static final int DELETE_FEATURE_FEATURE_ID_IS_NULL = 2;
    public static final int DELETE_FEATURE_FEATURE_ID_IS_256_CHARACTERS = 3;
    public static final int DELETE_FEATURE_FEATURE_ID_IS_255_CHARACTERS = 4;
    public static final int DELETE_FEATURE_FEATURE_ID_NOT_IN_DATABASE = 5;
    public static final int DELETE_FEATURE_FEATURE_ID_IS_GREATER_THAN_256_CHARACTERS = 6;
    public static final int DELETE_FEATURE_TENANT_IS_NULL = 7;
    public static final int DELETE_FEATURE_TENANT_IS_256_CHARACTERS = 8;
    public static final int DELETE_FEATURE_TENANT_IS_255_CHARACTERS = 9;
    public static final int DELETE_FEATURE_TENANT_GREATER_THAN_256_CHARACTERS = 10;
    public static final int DELETE_FEATURE_TENANT_NOT_IN_DATABASE = 11;
    public static final int DELETE_FEATURE_TENANT_NOT_MAPPED = 12;
    public static final int DELETE_FEATURE_X_REQUEST_ID_IS_NULL = 13;
    public static final int DELETE_FEATURE_X_REQUEST_ID_IS_256_CHARACTERS = 14;
    public static final int DELETE_FEATURE_X_REQUEST_ID_IS_255_CHARACTERS = 15;
    public static final int DELETE_FEATURE_X_REQUEST_ID_IS_GREATER_THAN_256_CHARACTERS = 16;
    public static final int DELETE_FEATURE_PACKAGE_AND_FEATURE_LINKED_CASE = 17;
    public static final int DELETE_FEATURE_VERIFY_PACKAGE_AND_CONTENT_ITEM_ARE_DELETED = 18;

    public static final String SUCCESSRESPONSE = "successresponse";
    public static final String MALFORMEDRESPONSE = "malformedresponse";
    public static final String CLIENTUNAUTHORIZED = "clientunauthorized";
    public static final String FEATUREIDALREADYPRESENT = "featurealreadypresent";
    public static Object XRequestId = "xrequestid";
    public static Object FeatureId = "FeatureId";
    public static Object HTTPSTATUSCODE = "HTTPSTATUSCODE";
    public static Object HTTPSTATUSMESSAGE = "HTTPSTATUSMESSAGE";
    public static final Object LOGFILENAME = "/var/log/aomsv2/DeleteFeature.log";
    public static Object CATEGORY = "category";

    public static HashMap<Object, Object> deleteFeatureTestData(int Rownum) throws IOException {
        
        /* This method gets input from DeleteFeature test data Excel sheet and puts in HashMap  */
        HashMap<Object, Object> hm = new HashMap<Object, Object>();

        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream("testData/DeleteFeature.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("DeleteFeature");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell featureId = currentRow.getCell(1);
        String featureIdStr = featureId.toString();
        if (featureIdStr.equals("NULL")) {
            hm.put(FEATURE_ID_DELETE_FEATURE, null);
        } else {
            hm.put(FEATURE_ID_DELETE_FEATURE, featureIdStr);
        }

        XSSFCell tenantCell = currentRow.getCell(2);
        String tenant = tenantCell.toString();
        if (tenant.equals("null")) {
            hm.put(TENANT_ID_DELETE_FEATURE, null);
        } else {
            hm.put(TENANT_ID_DELETE_FEATURE, tenant);
        }

        XSSFCell xrequestidCell = currentRow.getCell(3);
        String XRequestid = xrequestidCell.toString();
        if (XRequestid.equals("NULL")) {
            hm.put(X_REQUEST_ID_DELETE_FEATURE, null);
        } else {
            Random rd = new Random();
            int randInt1 = rd.nextInt(100000);
            String sALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder salt = new StringBuilder();
            Random rnd = new Random();
            while (salt.length() < 18) {
                int index = (int) (rnd.nextFloat() * sALTCHARS.length());
                salt.append(sALTCHARS.charAt(index));
            }
            String saltStr = salt.toString();
            String xRequestId_random = randInt1 + saltStr;
            hm.put(X_REQUEST_ID_DELETE_FEATURE, xRequestId_random);

        }

        wb.close();
        return hm;

    }


    public static HashMap<Object, Object> addFeatureTestData(int Rownum) throws IOException {

        /* This method gets input from AddFeature test data Excel sheet in DeleteFeature and puts in HashMap  */
        HashMap<Object, Object> hm = new HashMap<Object, Object>();

        FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY + CommonData.DELETE_FEATURE_TEST_DATA_PATH);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("DeleteFeature_forAddingFeature");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell featureId = currentRow.getCell(1);
        String featureIdStr = featureId.toString();
        if (featureIdStr.equals("NULL")) {
            hm.put(FEATURE_ID_ADD_FEATURE, null);
        } else {
            hm.put(FEATURE_ID_ADD_FEATURE, featureIdStr);
        }

        XSSFCell descriptionCell = currentRow.getCell(2);
        String description = descriptionCell.toString();
        if (description.equals("NULL")) {
            hm.put(DESCRIPTION_ADD_FEATURE, null);
        } else {
            hm.put(DESCRIPTION_ADD_FEATURE, description);

        }

        XSSFCell xTenantId = currentRow.getCell(3);
        String xTenantIdStr = xTenantId.toString();

        if (xTenantIdStr.equals("NULL")) {
            hm.put(TENANT_ID_ADD_FEATURE, null);
        } else {
            hm.put(TENANT_ID_ADD_FEATURE, xTenantIdStr);
        }

        XSSFCell xRequestID = currentRow.getCell(4);
        String xRequestIdStr = xRequestID.toString();
        if (xRequestIdStr.equals("NULL")) {
            hm.put(X_REQUEST_ID_ADD_FEATURE, null);
        } else {
            hm.put(X_REQUEST_ID_ADD_FEATURE, xRequestIdStr);
        }
        wb.close();
        return hm;

    }

    public static void addFeature(int RowNumber, boolean apigeeFlag) throws IOException {
        
        /* This method triggers requests and adds the feature which are prerequisite data for DeleteFeature  testcases*/
        
        LOGGER.debug("Adding Feature");
        HashMap<Object, Object> AddFeatureTestData = new HashMap<Object, Object>();
        AddFeatureTestData = addFeatureTestData(RowNumber);
        AddFeatureRequestNew requestdata = new AddFeatureRequestNew();
        requestdata.setFeatureId(AddFeatureTestData.get(DeleteFeatureUtils.FEATURE_ID_ADD_FEATURE));
        requestdata.setDescription(AddFeatureTestData.get(DeleteFeatureUtils.DESCRIPTION_ADD_FEATURE));
        String requestBodyStr = AddFeatureUtils.jsonMapping(requestdata);
        String requestbody = '[' + requestBodyStr + ']';
        if (apigeeFlag) {
            given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_FEATURE);
        } else {
            given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", AddFeatureTestData.get(DeleteFeatureUtils.TENANT_ID_ADD_FEATURE))
                    .header("X-Request-Id", AddFeatureTestData.get(DeleteFeatureUtils.X_REQUEST_ID_ADD_FEATURE)).when()
                    .put(CommonData.ADD_FEATURE);
            LOGGER.debug("The TenantId set is " + AddFeatureTestData.get(DeleteFeatureUtils.TENANT_ID_ADD_FEATURE));
            LOGGER.debug("The XRequestId set is " + AddFeatureTestData.get(DeleteFeatureUtils.X_REQUEST_ID_ADD_FEATURE));

        }

    }

    public static Response deleteFeature(int testdata, boolean apigeeFlag, HashMap<Object, Object> hm)
            throws IOException {
        
        /* This method triggers request for Delete Feature API and fetches the response  */
        LOGGER.debug("Deleting Feature");
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = hm;
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY)
                    .param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE)).when().delete(CommonData.DELETE_FEATURE);

            LOGGER.debug("The FeatureId set for DeleteFeature API is:" + dm.get(FEATURE_ID_DELETE_FEATURE));

        } else {
            response = given().relaxedHTTPSValidation().param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE))
                    .header("X-TENANT", dm.get(TENANT_ID_DELETE_FEATURE))
                    .header("X-Request-Id", dm.get(X_REQUEST_ID_DELETE_FEATURE)).when()
                    .delete(CommonData.DELETE_FEATURE);

            LOGGER.debug("The FeatureId set for DeleteFeature API is:" + dm.get(FEATURE_ID_DELETE_FEATURE));
            LOGGER.debug("The TenantId set for DeleteFeature API is:" + dm.get(TENANT_ID_DELETE_FEATURE));
            LOGGER.info("Feature Deleted");
        }

        return response;
    }

    public static Response deleteFeatureWhenRequestBodyIsEmpty(int testdata, boolean apigeeFlag,
            HashMap<Object, Object> hm) throws IOException {
        
        /* This method triggers request for Delete Feature API when requestbody is empty and fetches the response  */
        
        LOGGER.debug("Deleting Feature");
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = hm;

        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY).when()
                    .delete(CommonData.DELETE_FEATURE);
            LOGGER.debug("The FeatureId is not set");
        } else {
            response = given().relaxedHTTPSValidation().header("X-TENANT", dm.get(TENANT_ID_DELETE_FEATURE))
                    .header("X-Request-Id", dm.get(X_REQUEST_ID_DELETE_FEATURE)).when()
                    .delete(CommonData.DELETE_FEATURE);
            LOGGER.debug("The FeatureId is not set");
            LOGGER.debug("The TenantId set for DeleteFeature API is:" + dm.get(TENANT_ID_DELETE_FEATURE));
            LOGGER.debug("The XRequestId set for DeleteFeature API is: " + dm.get(X_REQUEST_ID_DELETE_FEATURE));
        }

        return response;
    }

    public static Response deleteFeatureHeadersAreEmpty(int testdata, boolean apigeeFlag, HashMap<Object, Object> hm)
            throws IOException {
        
        /* This method triggers request for Delete Feature API when request headers are empty and fetches the response  */
        
        LOGGER.debug("Deleting Feature");
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = hm;
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY)
                    .param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE)).when().delete(CommonData.DELETE_FEATURE);
            LOGGER.debug("The FeatureId is not set");
        } else {
            response = given().relaxedHTTPSValidation().param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE))
                    .header("X-TENANT", dm.get(TENANT_ID_DELETE_FEATURE))
                    .header("X-Request-Id", dm.get(X_REQUEST_ID_DELETE_FEATURE)).when()
                    .delete(CommonData.DELETE_FEATURE);
            LOGGER.debug("The FeatureId is not set");
            LOGGER.debug("The TenantId set for DeleteFeature API is:" + dm.get(TENANT_ID_DELETE_FEATURE));
            LOGGER.debug("The XRequestId set for DeleteFeature API is:" + dm.get(X_REQUEST_ID_DELETE_FEATURE));
        }

        LOGGER.info("Feature Deleted");
        return response;
    }

    public static Response deleteFeatureFeatureIdisMissing(int testdata, boolean apigeeFlag, HashMap<Object, Object> hm)
            throws IOException {
        
        /* This method triggers request for Delete Feature API when FeatureId is missing and fetches the response  */
        
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = hm;
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY).when()
                    .delete(CommonData.DELETE_FEATURE);
            LOGGER.debug("Feature id is not given in the Request");
        } else {
            response = given().relaxedHTTPSValidation().header("X-TENANT", dm.get(TENANT_ID_DELETE_FEATURE))
                    .header("X-Request-Id", dm.get(X_REQUEST_ID_DELETE_FEATURE)).when()
                    .delete(CommonData.DELETE_FEATURE);
            LOGGER.debug("Feature id is not given in the Request");
            LOGGER.debug("The TenantId set for DeleteFeature API is:" + dm.get(TENANT_ID_DELETE_FEATURE));
            LOGGER.debug("The XRequestId set for DeleteFeature API is: " + dm.get(X_REQUEST_ID_DELETE_FEATURE));
        }

        return response;
    }

    public static Response deleteFeatureFeatureIdisEmpty(int testdata, boolean apigeeFlag, HashMap<Object, Object> hm)
            throws IOException {
        
        /* This method triggers request for Delete Feature API when FeatureId is empty and fetches the response  */
        
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = hm;
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY)
                    .param("featureId", "").when().delete(CommonData.DELETE_FEATURE);
            LOGGER.debug("Feature id is empty in the Request");
        } else {
            response = given().relaxedHTTPSValidation().param("featureId", "")
                    .header("X-TENANT", dm.get(TENANT_ID_DELETE_FEATURE))
                    .header("X-Request-Id", dm.get(X_REQUEST_ID_DELETE_FEATURE)).when()
                    .delete(CommonData.DELETE_FEATURE);
            LOGGER.debug("Feature id is empty in the Request");
            LOGGER.debug("The TenantId set for DeleteFeature API is:" + dm.get(TENANT_ID_DELETE_FEATURE));
            LOGGER.debug("The XRequestId set for DeleteFeature API is: " + dm.get(X_REQUEST_ID_DELETE_FEATURE));

        }

        return response;
    }

    public static Response deleteFeatureXRequestIdisMissing(int testdata, HashMap<Object, Object> hm)
            throws IOException {
        
        /* This method triggers request for Delete Feature API when XRequestId is missing and fetches the response  */
        
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = hm;
        Response response = given().relaxedHTTPSValidation().param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE))
                .header("X-TENANT", dm.get(TENANT_ID_DELETE_FEATURE)).when().delete(CommonData.DELETE_FEATURE);

        LOGGER.debug("The FeatureId set for DeleteFeature API is:" + dm.get(FEATURE_ID_DELETE_FEATURE));
        LOGGER.debug("The TenantId set for DeleteFeature API is:" + dm.get(TENANT_ID_DELETE_FEATURE));
        LOGGER.debug("The XRequestId is not given in the Request");

        return response;
    }

    public static Response deleteFeatureXRequestIdisEmpty(HashMap<Object, Object> testdata) throws IOException {
        
        /* This method triggers request for Delete Feature API when XRequestId is empty and fetches the response  */
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = testdata;
        Response response = given().relaxedHTTPSValidation().param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE))
                .header("X-TENANT", dm.get(TENANT_ID_DELETE_FEATURE)).header("X-Request-Id", "").when()
                .delete(CommonData.DELETE_FEATURE);

        LOGGER.debug("The FeatureId set for DeleteFeature API is:" + dm.get(FEATURE_ID_DELETE_FEATURE));
        LOGGER.debug("The TenantId set for DeleteFeature API is:" + dm.get(TENANT_ID_DELETE_FEATURE));
        LOGGER.debug("The XRequestId is empty in the Request");

        return response;
    }

    public static Response deleteFeatureTenantIdisEmpty(int testdata, HashMap<Object, Object> hm) throws IOException {
        
        /* This method triggers request for Delete Feature API when TenantId is empty and fetches the response  */
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = hm;
        Response response = given().relaxedHTTPSValidation().param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE))
                .header("X-TENANT", " ").header("X-Request-Id", dm.get(X_REQUEST_ID_DELETE_FEATURE)).when()
                .delete(CommonData.DELETE_FEATURE);

        LOGGER.debug("The FeatureId set for DeleteFeature API is:" + dm.get(FEATURE_ID_DELETE_FEATURE));
        LOGGER.debug("The TenantId is empty in the Request");
        LOGGER.debug("The XRequestId set for DeleteFeature API is:" + dm.get(X_REQUEST_ID_DELETE_FEATURE));

        return response;
    }

    public static Response deleteFeatureTenantisMissing(int testdata, HashMap<Object, Object> hm) throws IOException {
       
        /* This method triggers request for Delete Feature API when TenantId is missing and fetches the response  */
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = hm;
        Response response = given().relaxedHTTPSValidation().param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE))
                .header("X-Request-Id", dm.get(X_REQUEST_ID_DELETE_FEATURE)).when().delete(CommonData.DELETE_FEATURE);

        LOGGER.debug("The FeatureId set for DeleteFeature API is:" + dm.get(FEATURE_ID_DELETE_FEATURE));
        LOGGER.debug("The TenantId is not given in the Request");
        LOGGER.debug("The XRequestId set for DeleteFeature API is:" + dm.get(X_REQUEST_ID_DELETE_FEATURE));

        return response;
    }

    public static Response deleteFeatureTenantisNull(int testdata, HashMap<Object, Object> dm) throws IOException {
        
        /* This method triggers request for Delete Feature API when TenantId is NULL and fetches the response  */
        HashMap<Object, Object> hm = new HashMap<Object, Object>();
        hm = dm;
        Response response = given().relaxedHTTPSValidation().param("featureId", hm.get(FEATURE_ID_DELETE_FEATURE))
                .header("X-TENANT", "null").header("X-Request-Id", hm.get(X_REQUEST_ID_DELETE_FEATURE)).when()
                .delete(CommonData.DELETE_FEATURE);

        LOGGER.debug("The FeatureId set for DeleteFeature API is" + hm.get(FEATURE_ID_DELETE_FEATURE));
        LOGGER.debug("The TenantId is given as Null");
        LOGGER.debug("The XRequestId set for DeleteFeature API is" + hm.get(X_REQUEST_ID_DELETE_FEATURE));

        return response;
    }

    public static Response deleteFeatureXRequestIdisNull(int testdata) throws IOException {
        /* This method triggers request for Delete Feature API when XRequestId is NULL and fetches the response  */
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deleteFeatureTestData(testdata);
        Response response = given().relaxedHTTPSValidation().param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE))
                .header("X-TENANT", dm.get(TENANT_ID_DELETE_FEATURE)).header("X-Request-Id", "null").when()
                .delete(CommonData.DELETE_FEATURE);

        LOGGER.debug("The FeatureId set for DeleteFeature API is" + dm.get(FEATURE_ID_DELETE_FEATURE));
        LOGGER.debug("The TenantId set for DeleteFeature API is" + dm.get(TENANT_ID_DELETE_FEATURE));
        LOGGER.debug("The XRequestId is given as Null in the Request");

        return response;
    }

    public static void validateSuccessResponse(Response response) throws IOException {
        /* This method validates the response according to success reponse codes and messages  */
        response.then().assertThat().statusCode(ResponseCodes.DELETE_FEATURE_DELETED_SUCCESSFULLY).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.DELETE_FEATURE_SUCCESS);

        LOGGER.debug("The Response is " + Messages.DELETE_FEATURE_SUCCESS);
        LOGGER.debug("Content Type is " + ContentType.JSON);

    }

    public static void validateBadRequestResponse(Response response) throws IOException {
        /* This method validates the response according to bad request reponse codes and messages  */
        response.then().assertThat().statusCode(ResponseCodes.DELETE_FEATURE_BAD_REQUEST_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.DELETE_PACKAGE_BAD_REQUEST);

        LOGGER.debug("The Response is " + Messages.DELETE_PACKAGE_BAD_REQUEST);
        LOGGER.debug("Content Type is " + ContentType.JSON);
    }

    public static void validateClientUnauthorizedResponse(Response response) throws IOException {
        /* This method validates the response according to client unauthorized reponse codes and messages  */
        response.then().assertThat().statusCode(ResponseCodes.DELETE_FEATURE_CLIENT_UNAUTHORIZED).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat()
                .statusLine(Messages.DELETE_PACKAGE_CLIENT_UNAUTHORIZED);
    }

    // Add Package Code
    public static HashMap<Object, Object> addPackageTestData(int Rownum) throws IOException {
        /* This method takes the testdata from excel to add packages as prerequisite data  */
        HashMap<Object, Object> hm = new HashMap<Object, Object>();

        FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY + CommonData.DELETE_FEATURE_TEST_DATA_PATH);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("DeleteFeature_AddPackage");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell packageId = currentRow.getCell(1);
        String pckgIdStr = packageId.toString();
        if (pckgIdStr.equals("NULL")) {
            hm.put(PACKAGE_ID_ADD_PACKAGE, null);
        } else {
            hm.put(PACKAGE_ID_ADD_PACKAGE, pckgIdStr);
        }

        XSSFCell featureId = currentRow.getCell(2);
        String featureIdStr = featureId.toString();
        if (featureIdStr.equals("NULL")) {
            hm.put(FEATURE_ID_ADD_PACKAGE, null);
        } else {
            hm.put(FEATURE_ID_ADD_PACKAGE, featureIdStr);
        }

        XSSFCell stateCell = currentRow.getCell(3);
        String state = stateCell.toString();
        if (state.equals("NULL")) {
            hm.put(STATE, null);
        }
        hm.put(STATE, state);

        XSSFCell metadataCell = currentRow.getCell(4);
        String metadata = metadataCell.toString();
        if (metadata.equals("NULL")) {
            hm.put(METADATA, null);
        } else {
            hm.put(METADATA, metadata);
        }

        XSSFCell checksumType = currentRow.getCell(5);
        String checksumtype = checksumType.toString();

        if (checksumtype.equals("NULL")) {
            hm.put(CHECKSUM_TYPE, null);
        } else {
            hm.put(CHECKSUM_TYPE, checksumtype);
        }

        XSSFCell checksumValue = currentRow.getCell(6);
        String checksumvalue = checksumValue.toString();
        if (checksumvalue.equals("NULL")) {
            hm.put(CHECKSUM_VALUE, null);
        } else {
            hm.put(CHECKSUM_VALUE, checksumvalue);
        }

        XSSFCell url = currentRow.getCell(7);
        String Url = url.toString();
        if (Url.equals("NULL")) {
            hm.put(URL, null);
        }
        hm.put(URL, Url);

        XSSFCell sizeCell = currentRow.getCell(8);
        String size = sizeCell.toString();
        if (size.equals("NULL")) {
            hm.put(SIZE_VALUE, null);
        } else {
            hm.put(SIZE_VALUE, size);
        }

        XSSFCell partNumberCell = currentRow.getCell(9);
        String partnumber = partNumberCell.toString();
        if (partnumber.equals("0")) {
            hm.put(PARTNUMBER, null);
        } else {
            hm.put(PARTNUMBER, partnumber);
        }

        XSSFCell tenantCell = currentRow.getCell(10);
        String tenant = tenantCell.toString();
        if (tenant.equals("NULL")) {
            hm.put(TENANT, null);
        } else {
            hm.put(TENANT, tenant);
        }

        XSSFCell xrequestidCell = currentRow.getCell(11);
        String xRequestId = xrequestidCell.toString();
        if (xRequestId.equals("NULL")) {
            hm.put(X_REQUEST_ID_ADD_PACKAGE, null);
        } else {
            hm.put(X_REQUEST_ID_ADD_PACKAGE, xRequestId);
        }
        wb.close();
        return hm;

    }

    public static Object addPackage(int rowNumber, boolean apigeeFlag) throws IOException {
        /* This method creates the add package request and triggers ADD Package API and returns package Id created  */
        HashMap<Object, Object> AddPackageTestData = new HashMap<Object, Object>();
        AddPackageTestData = addPackageTestData(rowNumber);
        String requestBodyStr = AddPackageUtils.jsonMapping(AddPackageTestData);
        String requestbody = '[' + requestBodyStr + ']';
        Object tenantid = AddPackageTestData.get(AddPackageUtils.TENANT);
        if (apigeeFlag) {
            given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_PACKAGE);
        } else {
            given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", tenantid)
                    .header("X-Request-Id", "sasd").when().put(CommonData.ADD_PACKAGE);

            LOGGER.debug("The TenantId set is: " + tenantid);
            LOGGER.debug("The XRequestId set is: sasd");
        }
        Object packageId = AddPackageTestData.get(AddPackageUtils.PACKAGE_ID);

        return packageId;
    }
    
    public static void verifylogsforSuccessResponse(Object featureId, Object xRequestId) throws MalformedURLException,
    IOException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
    KeyStoreException, CertificateException {


try {

    HTTPSTATUSCODE = "HTTPSTATUSCODE: 204";
    HTTPSTATUSMESSAGE = "Feature is deleted successfully";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"" + featureId
            + "\"}},{\"match\":{\"message\":\"INFO\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
            + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The search query is: " + urlParameters);

    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 

}

public static void verifylogsforBadRequest(Object Featureid, Object xRequestId, String logMessage)
    throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
    NoSuchAlgorithmException, KeyStoreException, CertificateException {



try {

    HTTPSTATUSCODE = "HTTPSTATUSCODE: 400";
    HTTPSTATUSMESSAGE = "Bad Request";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"Response Body:" + logMessage
            + " \"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + " \"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
            + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The search query is: " + urlParameters);
    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 


}

public static void verifylogsClientUnauthorizedResponse(Object featureId, Object xRequestId)
    throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
    NoSuchAlgorithmException, KeyStoreException, CertificateException {



try {

    HTTPSTATUSCODE = "HTTPSTATUSCODE: 403";
    HTTPSTATUSMESSAGE = "Client Unauthorized";

    String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
            + "\"}},{\"match\":{\"message\":\"" + featureId
            + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
            + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
            + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

    LOGGER.info("The search query is: " + urlParameters);

    CommonMethods.verifyLogsfromKibana(urlParameters);
    } catch (Exception e) {
        e.printStackTrace();
    } 


}

public static Object getFeatureId(HashMap<Object, Object> hm) {

    Object FeatureId = hm.get(DeleteFeatureUtils.FEATURE_ID_DELETE_FEATURE);
    return FeatureId;

}

public static Object getXRequestId(HashMap<Object, Object> hm) {

    Object XRequestId = hm.get(DeleteFeatureUtils.X_REQUEST_ID_DELETE_FEATURE);
    return XRequestId;

}



}
