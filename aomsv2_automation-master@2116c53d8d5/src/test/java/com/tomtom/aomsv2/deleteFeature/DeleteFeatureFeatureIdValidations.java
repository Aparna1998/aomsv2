/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.deleteFeature;

import org.testng.annotations.Test;
import com.tomtom.aomsv2.commonControls.CommonData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for Feature Id field for Delete Feature API and verifying
 * the response through ELB/Nodes.
 * 
 * @author palvadi
 */
public class DeleteFeatureFeatureIdValidations {

    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    
    
    private static final Logger LOGGER = LoggerFactory.getLogger("DeleteFeature.DeleteFeature_FeatureIdValidations");
    HashMap<Object, Object> testdata;


    @Test(groups = "DeleteFeature")
    public void deleteFeatureIdValid() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("********************Starting TestCase \"deleteFeatureIdValid\"********************");

        testdata= DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_VALID_DATA);
        DeleteFeatureUtils.addFeature(DeleteFeatureUtils.ADD_FEATURE_VALID_DATA, false);
        Response response = DeleteFeatureUtils.deleteFeature(DeleteFeatureUtils.DELETE_FEATURE_VALID_DATA, false,
                testdata);
        DeleteFeatureUtils.validateSuccessResponse(response);
        testdata=null;

    }

    @Test(groups = "DeleteFeature")
    public void deleteFeatureIdis256Characters() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("********************Starting TestCase \"deleteFeatureIdis256Characters\"********************");

        testdata= DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_FEATURE_ID_IS_256_CHARACTERS);
        DeleteFeatureUtils.addFeature(DeleteFeatureUtils.ADD_FEATURE_FEATURE_ID_IS_256_CHARACTERS, false);
        Response response = DeleteFeatureUtils.deleteFeature(
                DeleteFeatureUtils.DELETE_FEATURE_FEATURE_ID_IS_256_CHARACTERS, false, testdata);
        DeleteFeatureUtils.validateSuccessResponse(response);
        testdata=null;
    }

    @Test(groups = "DeleteFeature")
    public void deleteFeatureIdis255Characters() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("********************Starting TestCase \"deleteFeatureIdis255Characters\"********************");

        testdata = DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_FEATURE_ID_IS_255_CHARACTERS);
        DeleteFeatureUtils.addFeature(DeleteFeatureUtils.ADD_FEATURE_FEATURE_ID_IS_255_CHARACTERS, false);
        Response response = DeleteFeatureUtils.deleteFeature(
                DeleteFeatureUtils.DELETE_FEATURE_FEATURE_ID_IS_255_CHARACTERS, false, testdata);
        DeleteFeatureUtils.validateSuccessResponse(response);
        testdata=null;
    }

    @Test(groups = "DeleteFeature")
    public void deleteFeatureIdisGreaterThan256Characters() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("********************Starting TestCase \"deleteFeatureIdisGreaterThan256Characters\"********************");

        testdata = DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_FEATURE_ID_IS_GREATER_THAN_256_CHARACTERS);

        Response response = DeleteFeatureUtils.deleteFeature(
                DeleteFeatureUtils.DELETE_FEATURE_FEATURE_ID_IS_GREATER_THAN_256_CHARACTERS, false, testdata);
        DeleteFeatureUtils.validateBadRequestResponse(response);
        testdata=null;
    }

    @Test(groups = "DeleteFeature")
    public void deleteFeatureIdisNull() throws IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException, InterruptedException {
        LOGGER.info("********************Starting TestCase \"deleteFeatureIdisNull\"********************");

        testdata = DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_FEATURE_ID_IS_NULL);
        Response response = DeleteFeatureUtils.deleteFeature(DeleteFeatureUtils.DELETE_FEATURE_FEATURE_ID_IS_NULL,
                false, testdata);
        DeleteFeatureUtils.validateBadRequestResponse(response);
        testdata=null;
    }

    @Test(groups = "DeleteFeature")
    public void deleteFeatureIdisEmpty() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("********************Starting TestCase \"deleteFeatureIdisEmpty\"********************");

        HashMap<Object, Object> testdata = DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_VALID_DATA);

        Response response = DeleteFeatureUtils.deleteFeatureFeatureIdisEmpty(
                DeleteFeatureUtils.DELETE_FEATURE_VALID_DATA, false, testdata);
        DeleteFeatureUtils.validateBadRequestResponse(response);
        testdata=null;
    }

}
