package com.tomtom.aomsv2.fetchLicensemopar;
/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

import org.testng.annotations.Test;
import com.tomtom.aomsv2.fetchLicensemopar.FetchLicenseUtilsMopar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.xml.bind.JAXBException;

/**
 * This class has the test cases having different validations of Subject Id field for Fetch License API and verifying
 * the response through ELB/Nodes.
 * 
 * @author palvadi
 */
public class SubjectAliasIdValidations {

  
   

        private Logger LOGGER = LoggerFactory.getLogger("FetchLicense.SubjectIdValidations");
        HashMap<String, String> testdata;
     

        @Test(groups = "FetchLicenseMopar")
        public void fetchLicenseSubjectAliasIdgreaterthan256Characters() throws IOException, SQLException,
                ClassNotFoundException, JAXBException, UnrecoverableKeyException, KeyManagementException,
                NoSuchAlgorithmException, KeyStoreException, CertificateException, InterruptedException {

            LOGGER.info("********************Starting TestCase \"fetchLicenseSubjectAliasIdgreaterthan50Characters\"********************");

            testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.SUBJECT_ALIAS_ID_GREATER_THAN_256_CHARACTERS);
            String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
            Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, false);
            FetchLicenseUtilsMopar.validateMalformedRequest(response);

        }

        @Test(groups = "FetchLicenseMopar")
        public void fetchLicenseSubjectALiasIdis256Characters() throws IOException, SQLException, ClassNotFoundException,
                JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
                NoSuchAlgorithmException, KeyStoreException, CertificateException {

            LOGGER.info("********************Starting TestCase \"fetchLicenseSubjectAliasIdis50Characters\"********************");

            testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.SUBJECT_ALIAS_ID_IS_256_CHARACTERS);
            String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
            Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, false);
            FetchLicenseUtilsMopar.validateSuccessResponse(response);

        }

        @Test(groups = "FetchLicenseMopar")
        public void fetchLicenseSubjectAliasIdis255Characters() throws IOException, SQLException, ClassNotFoundException,
                JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
                NoSuchAlgorithmException, KeyStoreException, CertificateException {

            LOGGER.info("********************Starting TestCase \"fetchLicenseSubjectAliasIdis49Characters\"********************");

            testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.SUBJECT_ALIAS_ID_IS_255_CHARACTERS);
            String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
            Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, false);
            FetchLicenseUtilsMopar.validateSuccessResponse(response);

        }

        @Test(groups = "FetchLicenseMopar")
        public void fetchLicenseSubjectAliasIdisNullAsString() throws IOException, SQLException, ClassNotFoundException,
                JAXBException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
                KeyStoreException, CertificateException, InterruptedException {

            LOGGER.info("********************Starting TestCase \"fetchLicenseSubjectAliasIdisNullAsString\"********************");

            testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.SUBJECT_ALIAS_ID_IS_NULL_AS_STRING);
            String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
            Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, false);
            FetchLicenseUtilsMopar.validateSuccessResponse(response);

        }

        @Test(groups = "FetchLicenseMopar")
        public void fetchLicenseSubjectAliasIdisNull() throws IOException, SQLException, ClassNotFoundException, JAXBException,
                InterruptedException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
                KeyStoreException, CertificateException {

            LOGGER.info("********************Starting TestCase \"fetchLicenseSubjectAliasIdisNull\"********************");

            testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.SUBJECT_ALIAS_ID_IS_NULL);
            String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
            Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, false);
            FetchLicenseUtilsMopar.validateMalformedRequest(response);

        }
        

    }
 
