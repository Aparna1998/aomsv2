/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.fetchLicensemopar;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.commonControls.Base;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.commonControls.Messages;
import com.tomtom.aomsv2.commonControls.ResponseCodes;
import com.tomtom.aomsv2.jaxb.FetchLicenseMopar;
import com.tomtom.aomsv2.fetchLicensemopar.FetchLicenseUtilsMopar;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Random;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * This class has the common methods for Fetch License API which are referenced by all other test cases.
 * 
 * @author palvadi
 */
public class FetchLicenseUtilsMopar extends Base {

    private static Logger LOGGER = LoggerFactory.getLogger("FetchLicense.FetchLicense_Utils");

    public static String xmltag = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    public static String rootelement = "<request-license-in" + " "
            + "xmlns=\"http://www.tomtom.com/ns/aoms/license/1\"" + " "
            + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" + " "
            + "xsi:schemaLocation=\"http://www.tomtom.com/ns/aoms/license/1 schema.xsd\"\tversion=\"string\">";
    public static String endrootelement = "</request-license-in>";

    public static Response response = null;

    /*
     * Variables defined for corresponding row numbers present in Fetch License Test data sheet
     */
    public static final int VALID_DATA = 1;
    public static final int PACKAGE_ID_NOT_MAPPED_TO_CONTENT_ITEM_ID = 2;
    public static final int INUSE_PACKAGE = 3;
    public static final int OUTOFUSE_PACKAGE = 4;
    public static final int DEPRECATED_PACKAGE = 5;
    public static final int PACKAGEID_IS_NULL = 6;
    public static final int PACKAGE_ID_GREATER_THAN_256_CHARACTERS = 7;
    public static final int PACKAGE_ID_IS_256_CHARACTERS = 8;
    public static final int PACKAGE_ID_IS_255_CHARACTERS = 9;
    public static final int PACKAGE_ID_NOT_IN_DATABASE = 10;
    public static final int CONTENT_ITEM_IS_NULL = 11;
    public static final int CONTENTITEMISGREATERTHAN256CHARACTERS = 12;
    public static final int CONTENT_ITEM_IS_256_CHARACTERS = 13;
    public static final int CONTENT_ITEM_IS_255_CHARACTERS = 14;
    public static final int CONTENT_ITEM_ID_IS_NOT_IN_DATABASE = 15;
    public static final int PACKAGE_HAVING_MULTIPLE_CONTENT_ITEM_IDS = 16;
    public static final int TWO_PACKAGEIDS_IN_THE_REQUEST = 18;
    public static final int TWO_CONTENT_ITEM_IDS_IN_THE_REQUEST = 17;
    public static final int SUBJECT_ID_GREATER_THAN_50_CHARACTERS = 19;
    public static final int SUBJECT_ID_IS_49_CHARACTERS = 20;
    public static final int SUBJECT_ID_IS_50_CHARACTERS = 21;
    public static final int SUBJECT_ID_IS_NULL = 22;
    public static final int TENANT_DOES_NOT_BELONG_TO_PACKAGE = 23;
    public static final int REQUEST_PARAMS_HAVING_TRAILING_AND_LEADING_SPACES = 24;
    public static final int TENANT_ID_GREATER_THAN_256_CHARACTERS = 25;
    public static final int TENANT_ID_IS_NULL = 26;
    public static final int TENANT_ID_IS_256_CHARACTERS = 27;
    public static final int TENANT_ID_IS_255_CHARACTERS = 28;
    public static final int XREQUEST_ID_GREATR_THAN_256_CHARACTERS = 29;
    public static final int XREQUEST_ID_IS_NULL = 30;
    public static final int XREQUEST_ID_IS_256_CHARACTERS = 31;
    public static final int XREQUEST_ID_IS_255_CHARACTERS = 32;
    public static final int CONTENT_ITEM_IS_OF_POI_TYPE = 33;
    public static final int TENANT_ID_HAVING_NULL_AS_STRING = 35;
    public static final int XREQUEST_ID_HAVING_NULL_AS_STRING = 36;
    public static final int TENANT_ID_NOT_IN_DATABASE = 37;
    public static final int CONTENT_ITEM_ID_IS_NULL_AS_STRING = 38;
    public static final int PACKAGE_ID_IS_NULL_AS_STRING = 39;
    public static final int TENANT_ID_HAVING_SPECIAL_CHARACTERS = 40;
    public static final int XREQUEST_ID_HAVING_SPECIAL_CHARACTERS = 41;
    public static final int SUBJECT_ID_IS_NULL_AS_STRING = 42;
    public static final int PACKAGE_ID_AND_CONTENT_ITEM_ID_ARE_NOT_LINKED = 43;
    public static final int E2E_SCENARIO = 44;
    public static final int SUBJECT_ALIAS_ID_GREATER_THAN_256_CHARACTERS = 45;
    public static final int SUBJECT_ALIAS_ID_IS_255_CHARACTERS = 46;
    public static final int SUBJECT_ALIAS_ID_IS_256_CHARACTERS = 47;
    public static final int SUBJECT_ALIAS_ID_IS_NULL = 48;
    public static final int SUBJECT_ALIAS_ID_IS_NULL_AS_STRING = 49;
    
    /* Variables defined for test data Hash Map of the method 'testData' */
    public static final String SUBJECTID = "SubjectID";
    public static final String PACKAGEID = "Packageid";
    public static final String CONTENTITEMID = "ContentItemId";
    public static final String TENANTID = "TenantID";
    public static final String XREQUESTID = "XRequestID";
    public static final String SUBJECTID_ALIAS = "SubjectId_Alias";
    public static final String SUCCESSRESPONSE = "successresponse";
    public static final String MALFORMEDRESPONSE = "malformedresponse";
    public static final String CLIENTUNAUTHORIZED = "clientunauthorized";
    public static final String FEATUREIDALREADYPRESENT = "featurealreadypresent";
    public static Object HTTPSTATUSCODE = "HTTPSTATUSCODE";
    public static Object HTTPSTATUSMESSAGE = "HTTPSTATUSMESSAGE";
    public static final Object LOGFILENAME = "/var/log/aomsv2/FetchLicense.log";

   
    public static Object CATEGORY = "CATEGORY";

    @Test
    public static HashMap<String, String> testData(int Rownum) throws IOException {
        /* This method takes the testdata from TestData Prerequisite excel and puts in Hash Map*/

        HashMap<String, String> hm = new HashMap<String, String>();

        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream("testData/FetchLicense.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("FetchLicense");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell packageIdcell = currentRow.getCell(1);
        String packageIdStr = packageIdcell.toString();
        if (packageIdStr.equals("NULL")) {
            hm.put(PACKAGEID, null);
        } else {
            hm.put(PACKAGEID, packageIdStr);
        }

        XSSFCell contentitemidCell = currentRow.getCell(2);
        String contentitemIdStr = contentitemidCell.toString();
        if (contentitemIdStr.equals("NULL")) {
            hm.put(CONTENTITEMID, null);
        } else {
            hm.put(CONTENTITEMID, contentitemIdStr);
        }

        XSSFCell subjectidCell = currentRow.getCell(3);
        String subjectId = subjectidCell.toString();
        if (subjectId.equals("NULL")) {
            hm.put(SUBJECTID, null);
        } else {
            hm.put(SUBJECTID, subjectId);
        }
        
        XSSFCell SubjectAliasIdCell = currentRow.getCell(4);
        String SubjectAliasId = SubjectAliasIdCell.toString();
        if (SubjectAliasId.equals("NULL")) {
            hm.put(SUBJECTID_ALIAS, null);
        } else {
            hm.put(SUBJECTID_ALIAS, SubjectAliasId);
        }
     

        XSSFCell tenantidCell = currentRow.getCell(5);
        String tenantId = tenantidCell.toString();
        if (tenantId.equals("NULL")) {
            hm.put(TENANTID, null);
        } else {
            hm.put(TENANTID, tenantId);
        }

        XSSFCell xrequestIdCell = currentRow.getCell(6);
        String xRequestId = xrequestIdCell.toString();
        if (xRequestId.equals("NULL")) {
            hm.put(XREQUESTID, null);
        } else {
            Random rd = new Random();
            int randInt1 = rd.nextInt(100000);
            String sALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder salt = new StringBuilder();
            Random rnd = new Random();
            while (salt.length() < 18) {
                int index = (int) (rnd.nextFloat() * sALTCHARS.length());
                salt.append(sALTCHARS.charAt(index));
            }
            String saltStr = salt.toString();
            String xRequestId_random = randInt1 + saltStr;
            hm.put(XREQUESTID, xRequestId_random);
        }

        wb.close();
        fis.close();

        return hm;
    }

    public static String xmlMapping(HashMap<String, String> testdata) throws JAXBException {
        /* This method takes the testdata and maps into XML request body*/
        FetchLicenseMopar fl = new FetchLicenseMopar();
        if (testdata.get(FetchLicenseUtilsMopar.CONTENTITEMID) == null) {
            fl.setContentItemId(null);
        } else if (testdata.get(FetchLicenseUtilsMopar.CONTENTITEMID) == "NULLASSTRING")
            fl.setContentItemId("null");
        else {
            fl.setContentItemId(testdata.get(FetchLicenseUtilsMopar.CONTENTITEMID));
        }

        if (testdata.get(FetchLicenseUtilsMopar.PACKAGEID) == null) {
            fl.setContentPackageId(null);
        } else if (testdata.get(FetchLicenseUtilsMopar.PACKAGEID) == "NULLASSTRING")
            fl.setContentPackageId("null");
        else {
            fl.setContentPackageId(testdata.get(FetchLicenseUtilsMopar.PACKAGEID));
        }

        if (testdata.get(FetchLicenseUtilsMopar.SUBJECTID) == null) {
            fl.setSubjectId(null);
        } else if (testdata.get(FetchLicenseUtilsMopar.SUBJECTID) == "NULLASSTRING") {
            fl.setSubjectId("null");
        } else {
            fl.setSubjectId(testdata.get(FetchLicenseUtilsMopar.SUBJECTID));
        }
        
        if (testdata.get(FetchLicenseUtilsMopar.SUBJECTID_ALIAS) == null) {
            fl.setSubjectAliasId(null);
        } else if (testdata.get(FetchLicenseUtilsMopar.SUBJECTID_ALIAS) == "NULLASSTRING") {
            fl.setSubjectAliasId("null");
        }else {
            fl.setSubjectAliasId(testdata.get(FetchLicenseUtilsMopar.SUBJECTID_ALIAS));
        }
        

        JAXBContext context = JAXBContext.newInstance(FetchLicenseMopar.class);
        Marshaller marshal = context.createMarshaller();
        StringWriter writer = new StringWriter();
        marshal.marshal(fl, writer);
        String xmlbody = writer.toString();
        String mainbody = xmlbody.substring(55, xmlbody.length());
        String requestbody = xmltag + rootelement + mainbody + endrootelement;
        LOGGER.debug("The Request Body is " + requestbody);
        return requestbody;
    }

    public static Response getFetchLicenseResponse(String requestbody, HashMap<String, String> hm, boolean apigeeFlag) {
        /* This method triggers Fetch License API and fetches the response*/
        Object tenant;
        Object xrequestid;

        if (hm.get(TENANTID) == null) {
            tenant = "";
        } else if (hm.get(TENANTID) == "NULLASSTRING") {
            tenant = "null";
        } else {
            tenant = hm.get(TENANTID);
        }

        if (hm.get(XREQUESTID) == null) {
            xrequestid = "";
        } else if (hm.get(XREQUESTID) == "NULLASSTRING") {
            xrequestid = "null";
        } else {
            xrequestid = hm.get(XREQUESTID);
        }

        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_LICENSE_APIKEY).body(requestbody)
                    .when().post(CommonData.FETCH_LICENSE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody).header("X-Request-Id", xrequestid)
                    .header("X-TENANT", tenant).when().post(CommonData.FETCH_LICENSE);
        }

        return response;

    }

    public static Response passTenantTwice(String requestbody, HashMap<String, String> hm) {
        /* This method triggers the Fetch License API with */

        response = given().relaxedHTTPSValidation().body(requestbody).header("X-Request-Id", hm.get(XREQUESTID))
                .header("X-TENANT", hm.get(TENANTID), "X-TENANT", hm.get(TENANTID)).when()
                .post(CommonData.FETCH_LICENSE);

        return response;

    }

    public static Response passXRequestIdTwice(String requestbody, HashMap<String, String> hm) {

        response = given().relaxedHTTPSValidation().body(requestbody)
                .header("X-Request-Id", hm.get(XREQUESTID), "X-Request-Id", hm.get(XREQUESTID))
                .header("X-TENANT", hm.get(XREQUESTID)).when().post(CommonData.FETCH_LICENSE);

        return response;

    }

    public static void validateSuccessResponse(Response response) {
        response.then().assertThat().statusCode(ResponseCodes.FETCH_LICENSE_SUCCESS_RESPONSE_CODE).and().assertThat()
                .and().contentType("application/x-tt-license-type").statusLine(Messages.FETCH_LICENSE_SUCCESS);
        String contentLength = response.getHeader("Content-Length");
        String contentlengthActual = "132";
        boolean check = (contentLength.equals(contentlengthActual));
        assert check == true;

        LOGGER.debug("The response is " + Messages.FETCH_LICENSE_SUCCESS);
        LOGGER.debug("The ContentType is application/xml");
        LOGGER.debug("The ContentLength is" + contentlengthActual);

    }

    public static void validateMalformedRequest(Response response) {

        response.then().assertThat().statusCode(ResponseCodes.FETCH_LICENSE_MALFORMED_REQUEST_CODE).and().assertThat()
                .contentType("application/xml").and().statusLine(Messages.FETCH_LICENSE_MALFORMED_REQUEST);
        String contentLength = response.getHeader("Content-Length");
        String contentlengthActual = "0";
        boolean check = (contentLength.equals(contentlengthActual));
        assert check == true;

        LOGGER.debug("The response is " + Messages.FETCH_LICENSE_MALFORMED_REQUEST);
        LOGGER.debug("The ContentType is application/xml");
        LOGGER.debug("The ContentLength is 0");

    }

    public static void validateClientUnauthorized(Response response) {
        response.then().assertThat().statusCode(ResponseCodes.FETCH_LICENSE_CLIENT_UNAUTHORIZED).and().assertThat()
                .contentType("application/xml").and().statusLine(Messages.FETCH_LICENSE_CLIENT_UNAUTHORIZED);
        String contentLength = response.getHeader("Content-Length");
        String contentlengthActual = "0";
        boolean check = (contentLength.equals(contentlengthActual));
        assert check == true;

        LOGGER.debug("The response is " + Messages.FETCH_LICENSE_CLIENT_UNAUTHORIZED);
        LOGGER.debug("The ContentType is application/xml");
        LOGGER.debug("The ContentLength is 0");

    }

    public static void validateNoSuchPackageFound(Response response) {
        response.then().assertThat().statusCode(ResponseCodes.FETCH_LICENSE_NO_SUCH_PACKAGE_FOUND).and().assertThat()
                .contentType("application/xml").and().statusLine(Messages.FETCH_LICENSE_NO_PACKAGE_FOUND);
        String contentLength = response.getHeader("Content-Length");
        String contentlengthActual = "0";
        boolean check = (contentLength.equals(contentlengthActual));
        assert check == true;

        LOGGER.debug("The response is " + Messages.FETCH_LICENSE_NO_PACKAGE_FOUND);
        LOGGER.debug("The ContentType is application/xml");
        LOGGER.debug("The ContentLength is 0");

    }
    
    public static void verifylogsforSuccessResponse(Object packageId, Object xRequestId, Object tenantId)
            throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        try {

            HTTPSTATUSCODE = "HTTPSTATUSCODE: 200";
            HTTPSTATUSMESSAGE = "OK";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + tenantId + "\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
                    + "\"}},{\"match\":{\"message\":\"INFO\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The Query string is: " + urlParameters);
            CommonMethods.verifyLogsfromKibana(urlParameters);
            } catch (Exception e) {
                e.printStackTrace();
            } 


    }

    public static void verifylogsforBadRequest(Object packageId, Object xRequestId, Object tenantId)
            throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        try {

            HTTPSTATUSCODE = "HTTPSTATUSCODE: 400";
            HTTPSTATUSMESSAGE = "Bad Request";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + tenantId + "\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
                    + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The Query string is: " + urlParameters);
            CommonMethods.verifyLogsfromKibana(urlParameters);
            } catch (Exception e) {
                e.printStackTrace();
            } 

    }

    public static void verifylogsforClientUnauthorized(Object packageId, Object xRequestId, Object tenantId)
            throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        try {
            HTTPSTATUSCODE = "HTTPSTATUSCODE: 403";
            HTTPSTATUSMESSAGE = "Client Unauthorized";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + tenantId + "\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\""
                    + HTTPSTATUSMESSAGE + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The Query string is: " + urlParameters);
            CommonMethods.verifyLogsfromKibana(urlParameters);
            } catch (Exception e) {
                e.printStackTrace();
            } 

    }

    public static void verifylogsforNoSuchPackageFoundResponse(Object packageId, Object xRequestId, Object tenantId)
            throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {



        try {

            HTTPSTATUSCODE = "HTTPSTATUSCODE: 409";
            HTTPSTATUSMESSAGE = "No such package could be found";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + tenantId + "\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\""
                    + HTTPSTATUSMESSAGE + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The Query string is: " + urlParameters);
            CommonMethods.verifyLogsfromKibana(urlParameters);
            } catch (Exception e) {
                e.printStackTrace();
            } 


    }
    
    public static String getPackageId(HashMap<String, String> hm) {

        String PackageId = hm.get(FetchLicenseUtilsMopar.PACKAGEID);
        return PackageId;

    }

    public static String getXRequestId(HashMap<String, String> hm) {

        String XRequestId = hm.get(FetchLicenseUtilsMopar.XREQUESTID);
        return XRequestId;

    }



}
