/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.testDataPrerequisite;

import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.commonControls.Base;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.request.entity.AddFeatureRequestNew;
import com.tomtom.aomsv2.request.entity.AddPackage;
import com.tomtom.aomsv2.request.entity.Checksum;
import com.tomtom.aomsv2.request.entity.DownloadInfo;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.restassured.config.RestAssuredConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class has the common methods for TesData Prerequisite package .
 * 
 * @author palvadi
 */
public class TestDataUtils extends Base {

    private static Logger LOGGER = LoggerFactory.getLogger("testDataPrerequisite.TestDataUtils");

    public static final String FEATUREID_ADDFEATURE = "FeatureId";
    public static final Object DESCRIPTION_ADDFEATURE = "Description";
    public static final Object X_TENANT_ID_ADDFEATURE = "X_Tenant_Id";
    public static final Object X_REQUEST_ID_ADDFEATURE = "X_Request_Id";
    public static final String PACKAGEID_ADDPACKAGE = "PackageId";
    public static final String FEATUREID_ADDPACKAGE = "FeatureId";
    public static final String STATE_ADDPACKAGE = "State";
    public static final String METADATA_ADDPACKAGE = "Metadata";
    public static final String SIZE_ADDPACKAGE = "size";
    public static final Object CHECKSUM_TYPE_ADDPACKAGE = "Checksum_type";
    public static final Object CHECKSUM_VALUE_ADDPACKAGE = "Checksum_value";
    public static final Object URL_ADDPACKAGE = "URL";
    public static final Object TENANT_ADDPACKAGE = "Tenant";
    public static final Object XREQUESTID_ADDPACKAGE = "XRequestId";
    public static final Object PARTNUMBER_ADDPACKAGE = "PARTNUMBER";
    public static final Object FEATURE_ID_DELETE_FEATURE = "FEATURE_ID_DELETE_FEATURE";
    public static final Object TENANT_ID_DELETE_FEATURE = "TENANT_ID_DELETE_FEATURE";
    public static final Object X_REQUEST_ID_DELETE_FEATURE = "X_REQUEST_ID_DELETE_FEATURE";
    public static final Object PACKAGE_ID_DELETE_PACKAGE = "PACKAGE_ID_DELETE_PACKAGE";
    public static final Object FEATURE_ID_DELETE_PACKAGE = "FEATURE_ID_DELETE_PACKAGE";
    public static final Object TENANT_ID_DELETE_PACKAGE = "TENANT_ID_DELETE_PACKAGE";
    public static final Object X_REQUEST_ID_DELETE_PACKAGE = "X_REQUEST_ID_DELETE_PACKAGE";
    public static final String METADATA_STRING_DEV = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><tns:metadata generationDate=\"2018-03-30T08:00:00\" schemaName=\"cpt-metadata-2.0.0\" schemaVersion=\"1.6.2\" type=\"TomTom_Content_Package_Metadata\" xmlns:tns=\"http://www.tomtom.com/ns/cpt/metadata/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.tomtom.com/ns/cpt/metadata/2 cpt-metadata-2.0.0.xsd \"><tns:package xsi:type=\"tns:ContentMapPackageType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CP-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:packageFormat>TTPKG</tns:packageFormat><tns:packageNumberOfFiles>1</tns:packageNumberOfFiles><tns:packageTotalSizeInBytes>2576441475</tns:packageTotalSizeInBytes><tns:packageUnpackedSizeInBytes>2575576261</tns:packageUnpackedSizeInBytes><tns:group xsi:type=\"tns:ContentMapGroupType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CG-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:groupTotalSizeInBytes>2575562034</tns:groupTotalSizeInBytes><tns:groupStorageSizeInBytes>2575564800</tns:groupStorageSizeInBytes><tns:item xsi:type=\"tns:ContentMapTTCItemType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CI-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:itemTotalSizeInBytes>2575560372</tns:itemTotalSizeInBytes><tns:itemStorageSizeInBytes>2575560704</tns:itemStorageSizeInBytes><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapItemFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList><tns:mapCensoringNumber>NotApplicable</tns:mapCensoringNumber><tns:mapPublicationNumber>NotApplicable</tns:mapPublicationNumber><tns:releaseNumber>1005</tns:releaseNumber><tns:releaseBuildNumber>8804</tns:releaseBuildNumber><tns:databaseFormatVersion>1003</tns:databaseFormatVersion><tns:fileName>France.meta</tns:fileName><tns:encryptionServer>Durham-3.0</tns:encryptionServer></tns:item><tns:item xsi:type=\"tns:ContentPOIDealersItemType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CI-TT-FCA.FAMAR.P-POI.DLR.LATAM.TOYOTA.OV2-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:itemTotalSizeInBytes>2575560372</tns:itemTotalSizeInBytes><tns:itemStorageSizeInBytes>2575560704</tns:itemStorageSizeInBytes><tns:region>Latam</tns:region><tns:format>OV2</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL</tns:geoCoverage><tns:releaseType xsi:type=\"tns:POIItemFirstReleaseType\"/><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList><tns:brand>Toyota</tns:brand></tns:item><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapGroupFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList></tns:group><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapPackageFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList></tns:package></tns:metadata>";
    public static final String METADATA_STRING_TEMP = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><tns:metadata generationDate=\"2018-03-30T08:00:00\" schemaName=\"cpt-metadata-2.0.0\" schemaVersion=\"1.6.2\" type=\"TomTom_Content_Package_Metadata\" xmlns:tns=\"http://www.tomtom.com/ns/cpt/metadata/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.tomtom.com/ns/cpt/metadata/2 cpt-metadata-2.0.0.xsd \"><tns:package xsi:type=\"tns:ContentMapPackageType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CP-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:packageFormat>TTPKG</tns:packageFormat><tns:packageNumberOfFiles>1</tns:packageNumberOfFiles><tns:packageTotalSizeInBytes>2576441475</tns:packageTotalSizeInBytes><tns:packageUnpackedSizeInBytes>2575576261</tns:packageUnpackedSizeInBytes><tns:group xsi:type=\"tns:ContentMapGroupType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CG-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:groupTotalSizeInBytes>2575562034</tns:groupTotalSizeInBytes><tns:groupStorageSizeInBytes>2575564800</tns:groupStorageSizeInBytes><tns:item xsi:type=\"tns:ContentMapTTCItemType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CI-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:itemTotalSizeInBytes>2575560372</tns:itemTotalSizeInBytes><tns:itemStorageSizeInBytes>2575560704</tns:itemStorageSizeInBytes><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapItemFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList><tns:mapCensoringNumber>NotApplicable</tns:mapCensoringNumber><tns:mapPublicationNumber>NotApplicable</tns:mapPublicationNumber><tns:releaseNumber>1005</tns:releaseNumber><tns:releaseBuildNumber>8804</tns:releaseBuildNumber><tns:databaseFormatVersion>1003</tns:databaseFormatVersion><tns:fileName>France.meta</tns:fileName><tns:encryptionServer>Durham-3.0</tns:encryptionServer></tns:item><tns:item xsi:type=\"tns:ContentPOIDealersItemType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CI-TT-FCA.FAMAR.P-POI.DLR.LATAM.TOYOTA.OV2-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:itemTotalSizeInBytes>2575560372</tns:itemTotalSizeInBytes><tns:itemStorageSizeInBytes>2575560704</tns:itemStorageSizeInBytes><tns:region>Latam</tns:region><tns:format>OV2</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL</tns:geoCoverage><tns:releaseType xsi:type=\"tns:POIItemFirstReleaseType\"/><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList><tns:brand>Toyota</tns:brand></tns:item><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapGroupFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList></tns:group><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapPackageFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList></tns:package></tns:metadata>";
    public static final String METADATA_STRING_PREPROD = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><tns:metadata generationDate=\"2018-03-30T08:00:00\" schemaName=\"cpt-metadata-2.0.0\" schemaVersion=\"1.6.2\" type=\"TomTom_Content_Package_Metadata\" xmlns:tns=\"http://www.tomtom.com/ns/cpt/metadata/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.tomtom.com/ns/cpt/metadata/2 cpt-metadata-2.0.0.xsd \"><tns:package xsi:type=\"tns:ContentMapPackageType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CP-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:packageFormat>TTPKG</tns:packageFormat><tns:packageNumberOfFiles>1</tns:packageNumberOfFiles><tns:packageTotalSizeInBytes>2576441475</tns:packageTotalSizeInBytes><tns:packageUnpackedSizeInBytes>2575576261</tns:packageUnpackedSizeInBytes><tns:group xsi:type=\"tns:ContentMapGroupType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CG-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:groupTotalSizeInBytes>2575562034</tns:groupTotalSizeInBytes><tns:groupStorageSizeInBytes>2575564800</tns:groupStorageSizeInBytes><tns:item xsi:type=\"tns:ContentMapTTCItemType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CI-TT-FCA.FAMAR.P-MAP.LATAM.TTC-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:itemTotalSizeInBytes>2575560372</tns:itemTotalSizeInBytes><tns:itemStorageSizeInBytes>2575560704</tns:itemStorageSizeInBytes><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapItemFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList><tns:mapCensoringNumber>NotApplicable</tns:mapCensoringNumber><tns:mapPublicationNumber>NotApplicable</tns:mapPublicationNumber><tns:releaseNumber>1005</tns:releaseNumber><tns:releaseBuildNumber>8804</tns:releaseBuildNumber><tns:databaseFormatVersion>1003</tns:databaseFormatVersion><tns:fileName>Latam-101884.meta</tns:fileName><tns:encryptionServer>Durham-3.0</tns:encryptionServer></tns:item><tns:item xsi:type=\"tns:ContentPOIDealersItemType\" version=\"1.0.0\" targetCustomer=\"FCA\" identifier=\"CI-TT-FCA.FAMAR.P-POI.DLR.LATAM.TOYOTA.OV2-1.0.0\" targetProduct=\"FAMAR\" supplier=\"TomTom\" releaseDate=\"2018-03-30T08:00:00\" targetQualifier=\"Production\"><tns:itemTotalSizeInBytes>2575560372</tns:itemTotalSizeInBytes><tns:itemStorageSizeInBytes>2575560704</tns:itemStorageSizeInBytes><tns:region>Latam</tns:region><tns:format>OV2</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL</tns:geoCoverage><tns:releaseType xsi:type=\"tns:POIItemFirstReleaseType\"/><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList><tns:brand>Toyota</tns:brand></tns:item><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapGroupFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList></tns:group><tns:region>Latam</tns:region><tns:format>TTC</tns:format><tns:geoCoverage>ABW AIA ARG ATG BES BHS BLM BLZ BMU BOL BRA BRB CHL COL CRI CUB CUW CYM DMA DOM ECU GLP GRD GTM GUF GUY HND HTI JAM KNA LCA MAF MEX MSR MTQ NIC PAN PER PRI PRY SLV SUR SXM TCA TTO URY VCT VEN VGB VIR</tns:geoCoverage><tns:releaseType xsi:type=\"tns:MapPackageFirstReleaseType\" /><tns:navigationSoftwareCompatibilityList>NotApplicable</tns:navigationSoftwareCompatibilityList></tns:package></tns:metadata>";
    
    public static HashMap<Object, Object> addFeatureTestData(int Rownum) throws IOException {
        /* This method takes the testdata from TestData Prerequisite excel and puts in Hash Map*/
        HashMap<Object, Object> hm = new HashMap<Object, Object>();

        FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY + CommonData.PREREQUISITE_TEST_DATA_PATH);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("AddFeature");

        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell featureId = currentRow.getCell(1);
        String featureIdStr = featureId.toString();

        hm.put(FEATUREID_ADDFEATURE, featureIdStr);

        XSSFCell descriptionCell = currentRow.getCell(2);
        String description = descriptionCell.toString();

        hm.put(DESCRIPTION_ADDFEATURE, description);

        XSSFCell xTenantId = currentRow.getCell(3);
        String xTenantIdStr = xTenantId.toString();

        hm.put(X_TENANT_ID_ADDFEATURE, xTenantIdStr);

        XSSFCell xRequestID = currentRow.getCell(4);
        String xRequestIdStr = xRequestID.toString();

        hm.put(X_REQUEST_ID_ADDFEATURE, xRequestIdStr);
        wb.close();
        return hm;

    }

    public static int getRowCountAddFeature() throws IOException {
        /* This method gets the row count for AddFeature sheet*/
        FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY + CommonData.PREREQUISITE_TEST_DATA_PATH);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("AddFeature");

        int rowCount = testdatasheet.getLastRowNum();
        wb.close();
        fis.close();
        return rowCount;

    }

    public static int getRowCountAddPackage() throws IOException {
        /* This method gets the row count for AddPackage sheet*/
        FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY + CommonData.PREREQUISITE_TEST_DATA_PATH);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("AddPackage");
        int rowCount = testdatasheet.getPhysicalNumberOfRows();
        wb.close();
        fis.close();
        return rowCount;
    }

    public static void addfeature(int RowNumber, boolean apigeeFlag) throws IOException {
        /* This method triggers the request for AddFeature API */
        LOGGER.debug("Adding Feature");
        HashMap<Object, Object> addFeatureTestData = new HashMap<Object, Object>();
        addFeatureTestData = addFeatureTestData(RowNumber);
        AddFeatureRequestNew requestdata = new AddFeatureRequestNew();
        requestdata.setFeatureId(addFeatureTestData.get(TestDataUtils.FEATUREID_ADDFEATURE));
        requestdata.setDescription(addFeatureTestData.get(TestDataUtils.DESCRIPTION_ADDFEATURE));
        String request_body = AddFeatureUtils.jsonMapping(requestdata);
        String requestbody = '[' + request_body + ']';
        if (apigeeFlag) {
            given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_FEATURE);
        } else {
            given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", addFeatureTestData.get(TestDataUtils.X_TENANT_ID_ADDFEATURE))
                    .header("X-Request-Id", addFeatureTestData.get(TestDataUtils.X_REQUEST_ID_ADDFEATURE)).when()
                    .put(CommonData.ADD_FEATURE);
        }

        LOGGER.debug("The TenantId set is: " + addFeatureTestData.get(TestDataUtils.X_TENANT_ID_ADDFEATURE));
        LOGGER.debug("The Feature Id Added is : " + addFeatureTestData.get(TestDataUtils.FEATUREID_ADDFEATURE));
        LOGGER.debug("Feature Added");

    }

    public static HashMap<Object, Object> addPackageTestData(int Rownum) throws IOException {
        /* This method takes the testdata from TestData Prerequisite excel and puts in Hash Map for AddPackage*/
        HashMap<Object, Object> hm = new HashMap<Object, Object>();

        FileInputStream fis = new FileInputStream(CommonData.USER_DIRECTORY + CommonData.PREREQUISITE_TEST_DATA_PATH);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("AddPackage");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell packageId = currentRow.getCell(1);
        String pckgIdStr = packageId.toString();

        hm.put(PACKAGEID_ADDPACKAGE, pckgIdStr);

        XSSFCell featureId = currentRow.getCell(2);
        String featureIdStr = featureId.toString();

        hm.put(FEATUREID_ADDPACKAGE, featureIdStr);

        XSSFCell stateCell = currentRow.getCell(3);
        String state = stateCell.toString();

        hm.put(STATE_ADDPACKAGE, state);

        XSSFCell metadataCell = currentRow.getCell(4);
        String metadata = metadataCell.toString();

        hm.put(METADATA_ADDPACKAGE, metadata);

        XSSFCell checksumType = currentRow.getCell(5);
        String checksumtype = checksumType.toString();

        hm.put(CHECKSUM_TYPE_ADDPACKAGE, checksumtype);

        XSSFCell checksumValue = currentRow.getCell(6);
        String checksumvalue = checksumValue.toString();

        hm.put(CHECKSUM_VALUE_ADDPACKAGE, checksumvalue);

        XSSFCell url = currentRow.getCell(7);
        String Url = url.toString();

        hm.put(URL_ADDPACKAGE, Url);

        XSSFCell sizeCell = currentRow.getCell(8);
        String size = sizeCell.toString();
        hm.put(SIZE_ADDPACKAGE, size);

        XSSFCell partNumberCell = currentRow.getCell(9);
        String partnumber = partNumberCell.toString();

        hm.put(PARTNUMBER_ADDPACKAGE, partnumber);

        XSSFCell tenantCell = currentRow.getCell(10);
        String tenant = tenantCell.toString();

        hm.put(TENANT_ADDPACKAGE, tenant);

        XSSFCell xrequestIdCell = currentRow.getCell(11);
        String xRequestid = xrequestIdCell.toString();

        hm.put(XREQUESTID_ADDPACKAGE, xRequestid);

        wb.close();
        fis.close();

        return hm;

    }

    public static Object addpackage(int RowNumber, boolean apigeeFlag) throws IOException {
        /* This method requests AddPackage API and creates packages*/
        LOGGER.debug("Adding Package");
        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(RowNumber);
        String requestBodyStr = TestDataUtils.jsonMapping(addPackageTestData);
        String requestbody = '[' + requestBodyStr + ']';
        Object tenantid = addPackageTestData.get(TestDataUtils.TENANT_ADDPACKAGE);
        Object packageId = addPackageTestData.get(TestDataUtils.PACKAGEID_ADDPACKAGE);
        if (apigeeFlag) {
            given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_PACKAGE);
        } else {
            given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", tenantid)
                    .header("X-Request-Id", addPackageTestData.get(TestDataUtils.XREQUESTID_ADDPACKAGE)).when()
                    .put(CommonData.ADD_PACKAGE);
        }

        LOGGER.debug("The TenantId set is: " + tenantid);
        LOGGER.debug("The XRequestId set is: " + addPackageTestData.get(TestDataUtils.XREQUESTID_ADDPACKAGE));
        LOGGER.debug("Package Added is:" + packageId);

        return packageId;
    }

    public static HashMap<Object, Object> deleteFeatureTestData(int Rownum) throws IOException {
        /* This method takes the testdata from TestData Prerequisite excel and puts in Hash Map for Deleting Features*/
        HashMap<Object, Object> hm = new HashMap<Object, Object>();

        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream(
                "testData/TestDataPrerequisite.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("AddFeature");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell featureId = currentRow.getCell(1);
        String featureIdStr = featureId.toString();
        hm.put(FEATURE_ID_DELETE_FEATURE, featureIdStr);

        XSSFCell tenantCell = currentRow.getCell(3);
        String tenant = tenantCell.toString();
        hm.put(TENANT_ID_DELETE_FEATURE, tenant);

        XSSFCell xrequestIdCell = currentRow.getCell(4);
        String xRequestIdStr = xrequestIdCell.toString();
        hm.put(X_REQUEST_ID_DELETE_FEATURE, xRequestIdStr);

        wb.close();
        return hm;

    }

    public static void deleteFeature(int testdata, boolean apigeeFlag) throws IOException {
        /* This method triggers requests for Delete Feature API*/
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deleteFeatureTestData(testdata);
        if (apigeeFlag) {
            given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY)
                    .param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE)).when().delete(CommonData.DELETE_FEATURE)
                    .then().assertThat().statusCode(204);
        } else {
            given().relaxedHTTPSValidation().param("featureId", dm.get(FEATURE_ID_DELETE_FEATURE))
                    .header("X-TENANT", dm.get(TENANT_ID_DELETE_FEATURE))
                    .header("X-Request-Id", dm.get(X_REQUEST_ID_DELETE_FEATURE)).when()
                    .delete(CommonData.DELETE_FEATURE).then().assertThat().statusCode(204);
        }
        LOGGER.debug("The FeatureId set for DeleteFeature API is " + dm.get(FEATURE_ID_DELETE_FEATURE));
        LOGGER.debug("The TenantId set for DeleteFeature API is" + dm.get(TENANT_ID_DELETE_FEATURE));
        LOGGER.debug("The XRequestId set for DeleteFeature API is " + dm.get(X_REQUEST_ID_DELETE_FEATURE));

        LOGGER.debug("Feature Deleted");

    }

    public static String jsonMapping(HashMap<Object, Object> testdata) {
        /* This method maps the request body to JSON format for AddPackage API*/
        List<JSONObject> list;

        Checksum cs = new Checksum();
        if (testdata.get(TestDataUtils.CHECKSUM_TYPE_ADDPACKAGE) == null) {
            cs.setType("");
        } else if (testdata.get(TestDataUtils.CHECKSUM_TYPE_ADDPACKAGE).equals("MISSING")) {
            cs.setType(null);
        } else {
            cs.setType(testdata.get(TestDataUtils.CHECKSUM_TYPE_ADDPACKAGE));
        }

        if (testdata.get(TestDataUtils.CHECKSUM_VALUE_ADDPACKAGE) == null) {
            cs.setValue("");
        } else if (testdata.get(TestDataUtils.CHECKSUM_VALUE_ADDPACKAGE).equals("MISSING")) {
            cs.setValue(null);
        } else {
            cs.setValue(testdata.get(TestDataUtils.CHECKSUM_VALUE_ADDPACKAGE));
        }

        List<Checksum> checksumlist = new ArrayList<Checksum>();
        checksumlist.add(cs);

        DownloadInfo di = new DownloadInfo();
        di.setChecksum(cs);

        di.setPartNumber(99);

        di.setSize(123);

        if (testdata.get(TestDataUtils.URL_ADDPACKAGE) == "null") {
            di.setUrl("");
        } else if (testdata.get(TestDataUtils.URL_ADDPACKAGE).equals("MISSING")) {
            di.setUrl(null);
        } else {
            di.setUrl(testdata.get(TestDataUtils.URL_ADDPACKAGE));

        }

        List<DownloadInfo> downloadinfolist = new ArrayList<DownloadInfo>();
        downloadinfolist.add(di);

        AddPackage requestdata = new AddPackage();

        if (testdata.get(TestDataUtils.PACKAGEID_ADDPACKAGE) == null) {
            requestdata.setPackageId("");
        } else if (testdata.get(TestDataUtils.PACKAGEID_ADDPACKAGE).equals("MISSING")) {
            requestdata.setPackageId(null);
        } else {
            requestdata.setPackageId(testdata.get(TestDataUtils.PACKAGEID_ADDPACKAGE));
        }

        if (testdata.get(TestDataUtils.FEATUREID_ADDFEATURE) == null) {
            requestdata.setFeatureId("");
        } else if (testdata.get(TestDataUtils.FEATUREID_ADDFEATURE).equals("MISSING")) {

            requestdata.setFeatureId(null);
        } else {
            requestdata.setFeatureId(testdata.get(TestDataUtils.FEATUREID_ADDFEATURE));
        }

        if (testdata.get(TestDataUtils.STATE_ADDPACKAGE) == null) {
            requestdata.setState("");
        } else if (testdata.get(TestDataUtils.STATE_ADDPACKAGE).equals("MISSING")) {
            requestdata.setState(null);
        } else {
            requestdata.setState(testdata.get(TestDataUtils.STATE_ADDPACKAGE));
        }
        if(CommonData.ENIVRONMENT_RUN.contains("Dev"))
        {
            requestdata.setMetadata(METADATA_STRING_DEV);
            LOGGER.debug("I am taking dev metadata");
        }
        else if (CommonData.ENIVRONMENT_RUN.contains("PreProd"))
        {
            requestdata.setMetadata(METADATA_STRING_PREPROD);
        }
           
      
        requestdata.setDownloadinfo(downloadinfolist);

        JSONObject jsonobject = new JSONObject(requestdata);
        list = new ArrayList<JSONObject>();
        list.add(jsonobject);
        String reqbody = list.toString();
        int strlength = reqbody.length();
        String requestbody = reqbody.substring(1, strlength - 1);

        return requestbody;
    }

    public static HashMap<Object, Object> deletePackageTestData(int Rownum) throws IOException {
        /* This method takes the testdata from TestData Prerequisite excel and puts in Hash Map for DeletePackage*/
        HashMap<Object, Object> hm = new HashMap<Object, Object>();

        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream(
                "testData/TestDataPrerequisite.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("AddPackage");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell packageId = currentRow.getCell(1);
        String pckgIdStr = packageId.toString();
        hm.put(PACKAGE_ID_DELETE_PACKAGE, pckgIdStr);

        XSSFCell featureId = currentRow.getCell(2);
        String featureIdStr = featureId.toString();

        hm.put(FEATURE_ID_DELETE_PACKAGE, featureIdStr);

        XSSFCell tenantCell = currentRow.getCell(10);
        String tenant = tenantCell.toString();

        hm.put(TENANT_ID_DELETE_PACKAGE, tenant);

        XSSFCell xrequestIdCell = currentRow.getCell(11);
        String xRequestIdStr = xrequestIdCell.toString();

        hm.put(X_REQUEST_ID_DELETE_PACKAGE, xRequestIdStr);

        wb.close();
        return hm;
    }

    public static void deletePackage(int testdata, boolean apigeeFlag) throws IOException {
        /* This method triggers request for Delete Package API*/
        HashMap<Object, Object> dm = new HashMap<Object, Object>();
        dm = deletePackageTestData(testdata);
        if (apigeeFlag) {
            given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY)
                    .param("packageId", dm.get(PACKAGE_ID_DELETE_PACKAGE))
                    .param("featureId", dm.get(FEATURE_ID_DELETE_PACKAGE)).when().delete(CommonData.DELETE_PACKAGE);
        } else {
            given().relaxedHTTPSValidation().param("packageId", dm.get(PACKAGE_ID_DELETE_PACKAGE))
                    .param("featureId", dm.get(FEATURE_ID_DELETE_PACKAGE))
                    .header("X-TENANT", dm.get(TENANT_ID_DELETE_PACKAGE))
                    .header("X-Request-Id", dm.get(X_REQUEST_ID_DELETE_PACKAGE)).when()
                    .delete(CommonData.DELETE_PACKAGE);
        }

        LOGGER.debug("The PackageId set for DeletePackage API is " + dm.get(PACKAGE_ID_DELETE_PACKAGE));
        LOGGER.debug("The TenantId set for DeletePackage API is" + dm.get(TENANT_ID_DELETE_PACKAGE));
        LOGGER.debug("The XRequestId set for DeletePackage API is " + dm.get(X_REQUEST_ID_DELETE_PACKAGE));

    }

    public static String getTenantId(int Rownum) throws IOException {
        /* This method fetches the TenantId from Tenants sheet*/
        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream(
                "testData/TestDataPrerequisite.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("Tenants");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell tenantId = currentRow.getCell(1);
        String tenantIdStr = tenantId.toString();
        wb.close();
        return tenantIdStr;

    }

    public static int getRowCountTenants() throws IOException {
        /* This method gets the count of Tenants from Tenants sheet*/
        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream(
                "testData/TestDataPrerequisite.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("Tenants");
        int rowCount = testdatasheet.getLastRowNum();
        wb.close();
        fis.close();
        return rowCount;

    }

}
