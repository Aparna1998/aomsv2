/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.fetchPackage;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.commonControls.CommonData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations of Feature Id field for Fetch Package API and verifying
 * the response through ELB/Nodes.
 * 
 * @author palvadi
 */
public class FeatureIdValidations {

    private Logger LOGGER = LoggerFactory.getLogger("fetchPackage.ValidData");
    HashMap<Object, Object> testdata;

    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    
    @Test(groups = "FetchPackage")
    public void fetchPackageFeatureIdIsNull() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageFeatureIdIsNull\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.FEATURE_ID_MISSING);
        Response response = given().relaxedHTTPSValidation()
                .param("packageid", testdata.get(FetchPackageUtils.PACKAGE_ID)).param("featureId", "")
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .param("status", testdata.get(FetchPackageUtils.STATUS))
                .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                .get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id is set as Null");
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateMalformedResponse(response);
        testdata = null;
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageOnlyFeatureIdisSent() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageOnlyFeatureIdisSent\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.PACKAGE_ID_GREATER_THAN_256_CHARACTERS);
        Response response = given().relaxedHTTPSValidation()
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                .get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateSuccessResponse(response);

        testdata = null;
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageWhenInvalidFeatureisSent() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageWhenInvalidFeatureisSent\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.INVALID_PACKAGE_ID);
        Response response = given().relaxedHTTPSValidation()
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                .get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateResponseisNull(response);
        testdata = null;
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageWhenPackageIdandFeatureIdandStartDatearePresent1() throws IOException,
            ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageWhenPackageIdandFeatureIdandStartDatearePresent1\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.VALID_DATA);
        Response response = given().relaxedHTTPSValidation()
                .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                .get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateSuccessResponse(response);
        testdata = null;
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageWhenFeatureIdandStartDateandEndDatearePresent() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageWhenFeatureIdandStartDateandEndDatearePresent\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.VALID_DATA);
        Response response = given().relaxedHTTPSValidation()
                .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                .get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateSuccessResponse(response);
        testdata = null;

    }

    @Test(groups = "FetchPackage")
    public void fetchPackageFeatureIdGreaterThan256Characters() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageFeatureIdGreaterThan256Characters\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.PACKAGE_ID_GREATER_THAN_256_CHARACTERS);
        Response response = given().relaxedHTTPSValidation()
                .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                .get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateMalformedResponse(response);
        testdata = null;
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageFeatureIdisNotMappedtoPackageId() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageFeatureIdisNotMappedtoPackageId\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.PACKAGE_ID_GREATER_THAN_256_CHARACTERS);
        Response response = given().relaxedHTTPSValidation()
                .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                .get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateMalformedResponse(response);
        testdata = null;
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageFeatureIdisNotPresentInDatabase() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageFeatureIdisNotPresentInDatabase\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.FEATURE_ID_NOT_PRESENT_IN_DATABASE);
        Response response = given().relaxedHTTPSValidation()
                .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                .get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));
        FetchPackageUtils.validateSuccessResponse(response);
        testdata = null;
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageFeatureIdIsSentTwice() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageFeatureIdIsSentTwice\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.FEATURE_ID_NOT_PRESENT_IN_DATABASE);
        Response response = given().relaxedHTTPSValidation()
                .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                .get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is sent twice with value " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateMalformedResponse(response);
        testdata = null;
    }
}
