/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.fetchPackage;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.commonControls.CommonData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations of Start Date field for Fetch Package API and verifying
 * the response through ELB/Nodes.
 * 
 * @author palvadi
 */
public class StartDateValidations {

    private Logger LOGGER = LoggerFactory.getLogger("fetchPackage.ValidData");
    HashMap<Object, Object> testdata;

    @BeforeMethod
    public void startup() throws InterruptedException {
        RestAssured.baseURI = CommonData.BASE_URI;
    }
    
    @Test(groups = "FetchPackage")
    public void fetchPackageStartDateisMissing() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageStartDateisMissing\"********************");
 
            testdata = FetchPackageUtils.testData(FetchPackageUtils.START_DATE_MISSING);
            Response response = given().relaxedHTTPSValidation()
                    .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID)).param("startDate", "")
                    .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                    .param("status", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
            LOGGER.info("The Start Date is set as Null");
            LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);


    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStartDateisGreaterThanEndDate() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageStartDateisGreaterThanEndDate\"********************");
         testdata = FetchPackageUtils.testData(FetchPackageUtils.SATRT_DATE_GREATER_THAN_END_DATE);
            Response response = given().relaxedHTTPSValidation()
                    .param("packageid", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                    .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                    .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
            LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
            LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);

   
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStartDateisInvalid() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageStartDateisInvalid\"********************");
  
            testdata = FetchPackageUtils.testData(FetchPackageUtils.START_DATE_IS_INVALID);
            Response response = given().relaxedHTTPSValidation()
                    .param("packageid", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                    .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                    .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", "testdata.get(FetchPackageUtils.XREQUESTID)").when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
            LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
            LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);

    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStartDateisHavingCharacters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageStartDateisHavingCharacters\"********************");
  
            testdata = FetchPackageUtils.testData(FetchPackageUtils.START_DATE_IS_HAVING_CHARACTERS);
            Response response = given().relaxedHTTPSValidation()
                    .param("packageid", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                    .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                    .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", "testdata.get(FetchPackageUtils.XREQUESTID)").when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
            LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
            LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);

   

    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStartDateandEndDateareSame() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageStartDateandEndDateareSame\"********************");
   
            testdata = FetchPackageUtils.testData(FetchPackageUtils.START_AND_END_DATE_ARE_SAME);
            Response response = given().relaxedHTTPSValidation()
                    .param("packageid", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                    .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                    .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
            LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
            LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);


    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStartDateisinDifferentFormat() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageStartDateisinDifferentFormat\"********************");

            testdata = FetchPackageUtils.testData(FetchPackageUtils.START_DATE_IS_IN_DIFFERENT_FORMAT);
            Response response = given().relaxedHTTPSValidation()
                    .param("packageid", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                    .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                    .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
            LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
            LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);

    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStartDateIsSentTwice() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageStartDateisinDifferentFormat\"********************");

            testdata = FetchPackageUtils.testData(FetchPackageUtils.START_DATE_IS_IN_DIFFERENT_FORMAT);
            Response response = given().relaxedHTTPSValidation()
                    .param("packageid", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                    .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                    .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
            LOGGER.info("The Start Date set is sent twice with value " + testdata.get(FetchPackageUtils.START_DATE));
            LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);

    }

}
