/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2.fetchPackage;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import com.google.gson.Gson;
import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.commonControls.Base;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.commonControls.Messages;
import com.tomtom.aomsv2.commonControls.ResponseCodes;
import com.tomtom.aomsv2.request.entity.Checksum;
import com.tomtom.aomsv2.response.entity.Downloadinfo;
import com.tomtom.aomsv2.response.entity.FetchPackage;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Random;

/**
 * This class has the common methods for Fetch Package API which are referenced by all other test cases.
 * 
 * @author palvadi
 */
public class FetchPackageUtils extends Base {

    private static Logger LOGGER = LoggerFactory.getLogger("fetchPackage.ValidData");

    /*
     * Variables defined for corresponding row numbers present in Add Feature Test data sheet
     */
    public static final int VALID_DATA = 1;
    public static final int FEATURE_ID_MISSING = 2;
    public static final int START_DATE_MISSING = 3;
    public static final int END_DATE_MISSING = 4;
    public static final int STATUS_MISSING = 5;
    public static final int INVALID_PACKAGE_ID = 6;
    public static final int PACKAGE_ID_GREATER_THAN_256_CHARACTERS = 8;
    public static final int SATRT_DATE_GREATER_THAN_END_DATE = 9;
    public static final int START_DATE_IS_INVALID = 10;
    public static final int START_DATE_IS_HAVING_CHARACTERS = 11;
    public static final int START_AND_END_DATE_ARE_SAME = 12;
    public static final int START_DATE_IS_IN_DIFFERENT_FORMAT = 13;
    public static final int END_DATE_IS_INVALID = 14;
    public static final int END_DATE_IS_HAVING_CHARACTERS = 15;
    public static final int END_DATE_IS_IN_DIFFERENT_FORMAT = 16;
    public static final int ONLY_STATES_IS_GIVEN_IN_REQUEST_INUSE = 17;
    public static final int ONLY_STATES_IS_GIVEN_IN_REQUEST_OUT_OF_USE = 17;
    public static final int ONLY_STATES_IS_GIVEN_IN_REQUEST_DEPRECATED = 17;
    public static final int TENANT_IS_INVALID = 18;
    public static final int TENANT_NOT_IN_DATABASE = 18;
    public static final int TENANT_GREATER_THAN_256_CHARACTERS = 20;
    public static final int TENANT_256_CHARACTERS = 21;
    public static final int TENANT_255_CHARACTERS = 22;
    public static final int TENANT_IS_NULL = 23;
    public static final int TENANT_NOT_MAPPED = 24;
    public static final int XREQUEST_GREATER_THAN_256CHARACTERS = 25;
    public static final int XREQUEST_256_CHARACTERS = 26;
    public static final int XREQUEST_255_CHARACTERS = 27;
    public static final int XREQUEST_IS_NULL = 28;
    public static final int INUSE_AND_OUT_OF_USE_IN_SAME_STATES_PARAM = 29;
    public static final int OUT_OF_USE_AND_DEPRECATED_IN_SAME_STATES_PARAM = 30;
    public static final int STATUS_INUSE_INVALID_FORMAT = 31;
    public static final int STATUS_OUT_OF_USE_INVALID_FORMAT = 32;
    public static final int STATUS_DEPRECATED_INVALID_FORMAT = 33;
    public static final int E2E_FETCH_PACKAGE = 34;
    public static final int FEATURE_ID_NOT_PRESENT_IN_DATABASE = 35;
    public static final int PACKAGE_ID_NOT_PRESENT_IN_DATABASE = 36;
    public static final int END_DATE_GREATER_THAN_START_DATE = 37;

    /* Variables defined for test data Hash Map of the method 'testData' */
    public static final String PACKAGE_ID = "PackageId";
    public static final String FEATURE_ID = "FeatureID";
    public static final String START_DATE = "StartDate";
    public static final String END_DATE = "EndDate";
    public static final String STATUS = "Status";
    public static final Object XTENANT = "XTENANT";
    public static final Object XREQUESTID = "XREQUESTID";

    public static final String SUCCESSRESPONSE = "successresponse";
    public static final String MALFORMEDRESPONSE = "malformedresponse";
    public static final String CLIENTUNAUTHORIZED = "clientunauthorized";
    public static final String FEATUREIDALREADYPRESENT = "featurealreadypresent";
    public static Object HTTPSTATUSCODE = "HTTPSTATUSCODE";
    public static Object HTTPSTATUSMESSAGE = "HTTPSTATUSMESSAGE";
    public static final Object LOGFILENAME = "/var/log/aomsv2/FetchPackage.log";
    public static Object CATEGORY = "CATEGORY";

    @Test
    public static HashMap<Object, Object> testData(int Rownum) throws IOException {
        /* This method takes the testdata from TestData Prerequisite excel and puts in Hash Map */
        HashMap<Object, Object> hm = new HashMap<Object, Object>();

        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream("testData/FetchPackage.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("FetchPackage");
        XSSFRow currentRow = testdatasheet.getRow(Rownum);

        XSSFCell packageId = currentRow.getCell(1);
        String pckgIdStr = packageId.toString();
        if (pckgIdStr.equals("NULL")) {
            hm.put(PACKAGE_ID, null);
        } else {
            hm.put(PACKAGE_ID, pckgIdStr);
        }

        XSSFCell featureId = currentRow.getCell(2);
        String featureIdStr = featureId.toString();
        if (featureIdStr.equals("NULL")) {
            hm.put(FEATURE_ID, null);
        } else {
            hm.put(FEATURE_ID, featureIdStr);
        }

        XSSFCell startDate = currentRow.getCell(3);
        String startdateStr = startDate.toString();
        if (startdateStr.equals("NULL")) {
            hm.put(START_DATE, null);
        } else {
            hm.put(START_DATE, startdateStr);
        }

        XSSFCell endDate = currentRow.getCell(4);
        String enddateStr = endDate.toString();
        if (enddateStr.equals("NULL")) {
            hm.put(END_DATE, null);
        } else {
            hm.put(END_DATE, enddateStr);
        }

        XSSFCell statusCell = currentRow.getCell(5);
        String status = statusCell.toString();
        if (status.equals("NULL")) {
            hm.put(STATUS, null);
        } else {
            hm.put(STATUS, status);
        }

        XSSFCell tenantCell = currentRow.getCell(6);
        String tenant = tenantCell.toString();
        if (tenant.equals("NULL")) {
            hm.put(XTENANT, null);
        } else {
            hm.put(XTENANT, tenant);
        }

        XSSFCell xRequestIdCell = currentRow.getCell(7);
        String xRequestId = xRequestIdCell.toString();
        if (xRequestId.equals("NULL")) {
            hm.put(XREQUESTID, null);
        } else {

            Random rd = new Random();
            int randInt1 = rd.nextInt(100000);
            String sALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder salt = new StringBuilder();
            Random rnd = new Random();
            while (salt.length() < 18) {
                int index = (int) (rnd.nextFloat() * sALTCHARS.length());
                salt.append(sALTCHARS.charAt(index));
            }
            String saltStr = salt.toString();
            String xRequestId_random = randInt1 + saltStr;
            hm.put(XREQUESTID, xRequestId_random);

        }

        wb.close();
        fis.close();

        return hm;
    }


    public static void checkDBAndCompare(Response response, boolean apigeeFlag) throws ClassNotFoundException,
            SQLException {
        String queryPackageTable = "select id,metadata,status from cpis.package where id='$PackageId'";
        String queryPackageFeatureTable = "select * from cpis.package where id='$PackageId'";
        String queryPackagePartTable = "select * from cpis.package where id='$PackageId'";

        checkDatabaseandComparefromPackageTable(queryPackageTable, response, apigeeFlag);
        checkDatabaseandComparefromPackageFeatureTable(queryPackageFeatureTable, response, apigeeFlag);
        checkDatabaseandComparefromPackagePartTable(queryPackagePartTable, response, apigeeFlag);
    }

    public static void checkDatabaseandComparefromPackageTable(String Query, Response response, boolean apigeeFlag)
            throws ClassNotFoundException, SQLException {
        String respArray = response.asString();
        String resp = respArray.substring(1, respArray.length() - 1);

        Gson gson = new Gson();
        gson.fromJson(resp, FetchPackage.class);
        FetchPackage fp = new FetchPackage();
        String packageIdResponse = fp.getPackageId();
        String metadataResponse = fp.getMetadata();
        String statusResponse = fp.getState();

        Connection con = null;
        if (apigeeFlag) {
            con = CommonMethods.connectDBCpisSchemaWithPreprodAPIGEE();
        } else {
            con = CommonMethods.connectDBCpisSchema();
        }
        ResultSet rs = getResultfromDatabase(con, Query);
        if (rs.next()) {

            String packageIdDatabase = rs.getString(1);
            LOGGER.debug("The db response for feature id is" + packageIdDatabase);
            AssertJUnit.assertEquals(packageIdDatabase, packageIdResponse, "Package id is verfied");

            String metadata = rs.getString(2);
            LOGGER.debug("The db response for metadata is " + metadata);
            AssertJUnit.assertEquals(metadata, metadataResponse, "metadata is verified");

            String status = rs.getString(3);
            LOGGER.debug("The db response for status is " + status);
            AssertJUnit.assertEquals(status, statusResponse, "status is verified");
        }
    }

    public static void checkDatabaseandComparefromPackageFeatureTable(String Query, Response response,
            boolean apigeeFlag) throws ClassNotFoundException, SQLException {
        String respArray = response.asString();
        String resp = respArray.substring(1, respArray.length() - 1);

        Gson gson = new Gson();
        gson.fromJson(resp, FetchPackage.class);
        FetchPackage fp = new FetchPackage();
        String featureIdResponse = fp.getFeatureId();

        Connection con = null;
        if (apigeeFlag) {
            con = CommonMethods.connectDBCpisSchemaWithPreprodAPIGEE();
        } else {
            con = CommonMethods.connectDBCpisSchema();
        }
        ResultSet rs = getResultfromDatabase(con, Query);
        LOGGER.debug("The resultset is " + rs);
        if (rs.next()) {
            String feature_id_database = rs.getString(1);
            LOGGER.debug("The db response for feature id is" + feature_id_database);
            AssertJUnit.assertEquals(feature_id_database, featureIdResponse, "Feature is verfied");
        }
    }

    public static void checkDatabaseandComparefromPackagePartTable(String Query, Response response, boolean apigeeFlag)
            throws ClassNotFoundException, SQLException {
        String respArray = response.asString();
        String resp = respArray.substring(1, respArray.length() - 1);

        Gson gson = new Gson();
        gson.fromJson(resp, FetchPackage.class);
        // FetchPackage fp = new FetchPackage();
        Downloadinfo di = new Downloadinfo();
        gson.fromJson(resp, Downloadinfo.class);
        Checksum ck = new Checksum();
        gson.fromJson(resp, Checksum.class);
        Object partNumberResponse = di.getPartNumber();
        Object urlResponse = di.getUrl();
        Object checksumvalueResponse = ck.getValue();

        Connection con = null;
        if (apigeeFlag) {
            con = CommonMethods.connectDBCpisSchemaWithPreprodAPIGEE();
        } else {
            con = CommonMethods.connectDBCpisSchema();
        }
        ResultSet rs = getResultfromDatabase(con, Query);

        if (rs.next()) {
            String checksumvalueDatabase = rs.getString(1);
            LOGGER.debug("The db response for checksumvalue is" + checksumvalueDatabase);
            AssertJUnit.assertEquals(checksumvalueDatabase, checksumvalueResponse, "ChecksumValue is verfied");

            String urlDatabase = rs.getString(1);
            LOGGER.debug("The db response for Url is" + urlDatabase);
            AssertJUnit.assertEquals(urlDatabase, urlResponse, "URL is verfied");

            String partNumberDatabase = rs.getString(1);
            LOGGER.debug("The db response for PartNumber is" + partNumberDatabase);
            AssertJUnit.assertEquals(partNumberDatabase, partNumberResponse, "PartNumber is verfied");
        }
    }

    public static ResultSet getResultfromDatabase(Connection conn, String Query) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(Query);
        return rs;
    }

    public static void validateSuccessResponse(Response response) {
        CommonMethods.IsInValidJSONFormat(response);
        response.then().assertThat().statusCode(ResponseCodes.FETCH_PACKAGE_SUCCESS_RESPONSE_CODE).and().assertThat()
                .contentType(ContentType.JSON);       
        String contentLengthStr = response.getHeader("Content-Length");
        int contentlength = Integer.parseInt(contentLengthStr);
        if (contentlength > 0) {
            AssertJUnit.assertTrue(true);
        } else {
            AssertJUnit.assertFalse(false);
        }

    }

 
    public static void validateResponseisNull(Response response) {
        response.then().assertThat().statusCode(ResponseCodes.FETCH_PACKAGE_SUCCESS_RESPONSE_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.FETCH_PACKAGE_SUCCESS);
        String contentLengthStr = response.getHeader("Content-Length");

        int contentlength = Integer.parseInt(contentLengthStr);
        if (contentlength == 2) {
            AssertJUnit.assertTrue(true);
        } else {
            AssertJUnit.assertFalse(false);
        }

    }

    public static void validateMalformedResponse(Response response) {
        response.then().assertThat().statusCode(ResponseCodes.FETCH_PACKAGE_MALFORMED_REQUEST_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.FETCH_PACKAGE_MALFORMED_REQUEST);
        String contentLengthStr = response.getHeader("Content-Length");
        String contentlength = "0";
        if (contentLengthStr.equals(contentlength)) {
            AssertJUnit.assertTrue(true);

        }

        else {
            AssertJUnit.assertFalse(false);
        }
    }

    public static void validateClientUnauthorizedResponse(Response response) {
        response.then().assertThat().statusCode(ResponseCodes.FETCH_PACKAGE_CLIENT_UNAUTHORIZED_CODE).and()
                .assertThat().contentType(ContentType.JSON).and().assertThat()
                .statusLine(Messages.FETCH_PACKAGE_CLIENT_UNAUTHORIZED);
        String contentLengthStr = response.getHeader("Content-Length");
        int contentlength = Integer.parseInt(contentLengthStr);
        if (contentlength == 0) {
            AssertJUnit.assertTrue(true);
        } else {
            AssertJUnit.assertFalse(false);
        }
    }

    public static void checkDBAndCompareForMultiple(Response response, boolean apigeeFlag)
            throws ClassNotFoundException, SQLException {
        String queryPackageTable = "select id,metadata,status from cpis.package where id='$PackageId'";
        String queryPackageFeatureTable = "select * from cpis.package where id='$PackageId'";
        String queryPackagePartTable = "select * from cpis.package where id='$PackageId'";

        checkDatabaseandComparefromPackageTable(queryPackageTable, response, apigeeFlag);
        checkDatabaseandComparefromPackageFeatureTable(queryPackageFeatureTable, response, apigeeFlag);
        checkDatabaseandComparefromPackagePartTable(queryPackagePartTable, response, apigeeFlag);
    }
    
    public static void verifylogsforClientUnauthorizedResponse(Object packageId, Object xRequestId)
            throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {



        try {

            HTTPSTATUSCODE = "HTTPSTATUSCODE: 403";
            HTTPSTATUSMESSAGE = "Client Unauthorized";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"Response Body:" + HTTPSTATUSCODE
                    + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE + "\"}},{\"match\":{\"source\":\""
                    + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The URL parameters is " + urlParameters);
            CommonMethods.verifyLogsfromKibana(urlParameters);
            } catch (Exception e) {
                e.printStackTrace();
            } 

    }

    public static void verifylogsforSuccessResponse(Object packageId, Object xRequestId) throws MalformedURLException,
            IOException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException {



        try {
            HTTPSTATUSCODE = "HTTPSTATUSCODE: 200";
            HTTPSTATUSMESSAGE = "OK";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"Response Body:" + HTTPSTATUSCODE
                    + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE + "\"}},{\"match\":{\"source\":\""
                    + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The URL parameters is " + urlParameters);
            CommonMethods.verifyLogsfromKibana(urlParameters);
            } catch (Exception e) {
                e.printStackTrace();
            } 

    }

    public static void verifylogsforBadRequestResponse(Object packageId, Object xRequestId)
            throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {


        try {

            HTTPSTATUSCODE = "HTTPSTATUSCODE: 400";
            HTTPSTATUSMESSAGE = "Bad Request";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"Response Body:" + HTTPSTATUSCODE
                    + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE + "\"}},{\"match\":{\"source\":\""
                    + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The URL parameters is " + urlParameters);
            CommonMethods.verifyLogsfromKibana(urlParameters);
            } catch (Exception e) {
                e.printStackTrace();
            } 

    }

    public static Object getPackageId(HashMap<Object, Object> hm) {

        Object PackageId = hm.get(FetchPackageUtils.PACKAGE_ID);
        return PackageId;

    }

    public static Object getXRequestId(HashMap<Object, Object> hm) {

        Object XRequestId = hm.get(FetchPackageUtils.XREQUESTID);
        return XRequestId;

    }

}
