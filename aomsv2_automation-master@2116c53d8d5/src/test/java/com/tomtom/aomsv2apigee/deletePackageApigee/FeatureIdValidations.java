/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2apigee.deletePackageApigee;

import com.tomtom.aomsv2.commonControls.BaseAPIGEE;
import com.tomtom.aomsv2.deletePackage.DeletePackageUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations of Feature Id field for Delete Package API and verifying
 * the response through APIGEE.
 * 
 * @author palvadi
 */
public class FeatureIdValidations extends BaseAPIGEE{
    private static final Logger LOGGER = LoggerFactory.getLogger("DeletePackage.DeletePackage_FeatureIdValidations");

 

    @Test(groups = "DeletePackage")
    public void deletePackageFeatureIdIsNull() throws IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException, InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageFeatureIdIsNull\"********************");

        HashMap<Object, Object> testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_FEATURE_ID_IS_NULL);
        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_VALID_DATA, true);
        Response response = DeletePackageUtils.deletePackage(DeletePackageUtils.DELETE_PACKAGE_FEATURE_ID_IS_NULL,
                true, testdata);
        DeletePackageUtils.validateBadRequestResponse(response);

    }

    @Test(groups = "DeletePackage")
    public void deletePackageFeatureIdisMissing() throws IOException {
        LOGGER.info("********************Starting TestCase \"deletePackageFeatureIdisMissing\"********************");

        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_VALID_DATA, true);
        Response response = DeletePackageUtils.deletePackageFeatureIdisMissing(
                DeletePackageUtils.DELETE_PACKAGE_VALID_DATA, true);
        DeletePackageUtils.validateSuccessResponse(response);

    }

    @Test(groups = "DeletePackage")
    public void deletePackageFeatureIdis256Characters() throws IOException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageFeatureIdis256Characters\"********************");

        HashMap<Object, Object> testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_FEATURE_ID_IS_256_CHARACTERS);
        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_FEATURE_ID_IS_256_CHARACTERS, true);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_FEATURE_ID_IS_256_CHARACTERS, true, testdata);
        DeletePackageUtils.validateSuccessResponse(response);

    }

    @Test(groups = "DeletePackage")
    public void deletePackageFeatureis255Characters() throws IOException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageFeatureis255Characters\"********************");

        HashMap<Object, Object> testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_FEATURE_ID_IS_255_CHARACTERS);
        DeletePackageUtils.addPackage(DeletePackageUtils.ADD_PACKAGE_FEATURE_ID_IS_255_CHARACTERS, true);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_FEATURE_ID_IS_255_CHARACTERS, true, testdata);
        DeletePackageUtils.validateSuccessResponse(response);

    }

    @Test(groups = "DeletePackage")
    public void deletePackageFeatureIdisGreaterThan256Characters() throws IOException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageFeatureIdisGreaterThan256Characters\"********************");

        HashMap<Object, Object> testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_FEATURE_ID_GREATER_THAN_256_CHARACTERS);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_FEATURE_ID_GREATER_THAN_256_CHARACTERS, true, testdata);
        DeletePackageUtils.validateBadRequestResponse(response);

    }

    @Test(groups = "DeletePackage")
    public void deletePackageFeatureIdNotInDatabase() throws IOException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            InterruptedException {
        LOGGER.info("********************Starting TestCase \"deletePackageFeatureIdNotInDatabase\"********************");

        HashMap<Object, Object> testdata = DeletePackageUtils
                .deletePackageTestData(DeletePackageUtils.DELETE_PACKAGE_FEATURE_ID_NOT_IN_DATABASE);
        Response response = DeletePackageUtils.deletePackage(
                DeletePackageUtils.DELETE_PACKAGE_FEATURE_ID_NOT_IN_DATABASE, true, testdata);
        DeletePackageUtils.validateBadRequestResponse(response);

    }

}
