/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2apigee.testDataPrerequisiteApigee;

import com.tomtom.aomsv2.commonControls.BaseAPIGEE;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.testDataPrerequisite.TestDataUtils;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import io.restassured.RestAssured;

import java.io.IOException;
import java.sql.SQLException;

/**
 * This class has the test cases having different validations for Description field in Add Fesature Request and
 * verifying the response through APIGEE.
 * 
 * @author palvadi
 */
public class DatabaseInsertionsAndDeletions extends BaseAPIGEE {
  
    private static Logger LOGGER = LoggerFactory.getLogger("testDataPrerequisiteApigee.DatabaseInsertionsAndDeletions");

    @BeforeSuite(alwaysRun = true)
    public void b_AddFeatures() throws ClassNotFoundException, SQLException, IOException {
        RestAssured.baseURI = CommonData.BASE_URI_APIGEE;
        int rowCount = TestDataUtils.getRowCountAddFeature();
        for (int i = 1; i <= rowCount; i++) {
            TestDataUtils.addfeature(i, true);
        }
    }

    @BeforeSuite(alwaysRun = true)
    public void c_AddPackages() throws ClassNotFoundException, SQLException, IOException {
        RestAssured.baseURI = CommonData.BASE_URI_APIGEE;
        int rowCount = TestDataUtils.getRowCountAddPackage();
        for (int i = 1; i < rowCount; i++) {
            TestDataUtils.addpackage(i, true);
        }
    }

    @AfterSuite(alwaysRun = true)
    public void d_deletePackages() throws ClassNotFoundException, SQLException, PSQLException, IOException {
        RestAssured.baseURI = CommonData.BASE_URI_APIGEE;
        int rowCount = TestDataUtils.getRowCountAddPackage();
        LOGGER.info("The row count is " + rowCount);
        for (int i = 1; i < rowCount; i++) {
            TestDataUtils.deletePackage(i, true);
        }

    }

    @AfterSuite(alwaysRun = true)
    public void e_deleteFeatures() throws ClassNotFoundException, SQLException, PSQLException, IOException {
        RestAssured.baseURI = CommonData.BASE_URI_APIGEE;
        int rowCount = TestDataUtils.getRowCountAddFeature();
        for (int i = 1; i <= rowCount; i++) {
            TestDataUtils.deleteFeature(i, true);
        }
    }
}
