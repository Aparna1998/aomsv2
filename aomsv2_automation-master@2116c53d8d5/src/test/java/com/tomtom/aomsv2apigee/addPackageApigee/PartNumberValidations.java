/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2apigee.addPackageApigee;

import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackage;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackageTestData;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackageTwoPartNumbers;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackageWithTwoDifferentPartNumberValues;

import com.tomtom.aomsv2.addPackage.AddPackageUtils;
import com.tomtom.aomsv2.commonControls.BaseAPIGEE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for Package Id field for Add Package API and verifying the
 * response through APIGEE.
 * 
 * @author palvadi
 */
public class PartNumberValidations extends BaseAPIGEE {

    private static final Logger LOGGER = LoggerFactory.getLogger("AddPackage.PartNumberValidations");



    @Test(groups = "AddPackage")
    public void addPackageWithTwoSamePartNumbers() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWithTwoSamePartNumbers\"********************");
        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.IN_USE_PACKAGE);
        Response response = addPackageTwoPartNumbers(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);
    }

    @Test(groups = "AddPackage")
    public void addPackageWithTwoDifferentPartNumbers() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWithTwoDifferentPartNumbers\"********************");
        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.TWO_VALID_PARTNUMBERS);
        Response response = addPackageWithTwoDifferentPartNumberValues(addPackageTestData, true);
        AddPackageUtils.validateSuccessResponse(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberNotanInteger() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWithPartNumberNotanInteger\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.ONE_INVALID_PARTNUMBER);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberasNull() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWithPartNumberasNull\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.PARTNUMBER_IS_NULL);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberis1BillionBillionCharacters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWithPartNumberis1BillionBillionCharacters\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.PARTNUMBER_IS_ONE_BILLION_BILLION);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberisgreaterthan1BillionBillionCharacters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWithPartNumberisgreaterthan1BillionBillionCharacters\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.PARTNUMBER_IS_GREATER_THAN_ONE_BILLION_BILLION);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberisSpecialCharacters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWithPartNumberisSpecialCharacters\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.PARTNUMBER_HAS_SPECIAL_CHARACTERS);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageWithPartNumberIsMissing() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageWithPartNumberIsMissing\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.PARTNUMBER_IS_MISSING);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

}
