/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2apigee.addPackageApigee;

import com.tomtom.aomsv2.addPackage.AddPackageUtils;
import com.tomtom.aomsv2.commonControls.BaseAPIGEE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for CheckSum Type field for Add Package API and verifying
 * the response through APIGEE.
 * 
 * @author palvadi
 */
public class AddingPackagewithDifferentStatus extends BaseAPIGEE {
    private static final Logger LOGGER = LoggerFactory.getLogger("AddPackage.AddingPackagewithDifferentStatus");



    @Test(groups = "AddPackage")
    public void addPackagewithStatusInuse() throws IOException {

        LOGGER.info("********************Starting TestCase \"addPackagewithStatusInuse\"********************");

        HashMap<Object, Object> addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.IN_USE_PACKAGE);
        Response response = AddPackageUtils.addPackage(addPackageTestData, true);
        AddPackageUtils.validateSuccessResponse(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageWithStatusOutOfUse() throws IOException {

        LOGGER.info("********************Starting TestCase \"addPackageWithStatusOutOfUse\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.OUT_OF_USE_PACKAGE);
        Response response = AddPackageUtils.addPackage(addPackageTestData, true);
        AddPackageUtils.validateSuccessResponse(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageWithStatusDeprecated() throws IOException {

        LOGGER.info("********************Starting TestCase \"addPackageWithStatusDeprecated\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = AddPackageUtils.addPackageTestData(AddPackageUtils.DEPRECATED_PACKAGE);
        Response response = AddPackageUtils.addPackage(addPackageTestData, true);
        AddPackageUtils.validateSuccessResponse(response);

    }

}
