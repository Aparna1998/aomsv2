/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2apigee.addPackageApigee;

import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackage;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackageTestData;

import com.tomtom.aomsv2.addPackage.AddPackageUtils;
import com.tomtom.aomsv2.commonControls.BaseAPIGEE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for URL field for Add Package API and verifying the
 * response through APIGEE.
 * 
 * @author palvadi
 */
public class URLvalidations extends BaseAPIGEE {

    private static final Logger LOGGER = LoggerFactory.getLogger("AddPackage.PartNumberValidations");


    @Test(groups = "AddPackage")
    public void addPackageURLGiveninInvalidFormat1() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageURLGiveninInvalidFormat1\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.URL_IS_INVALID_FORMAT_1);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageURLGiveninInvalidFormat2() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageURLGiveninInvalidFormat2\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.URL_IS_INVALID_FORMAT_1);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageURLGreaterThan2048Characters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageURLGreaterThan2048Characters\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.URL_IS_GREATER_THAN_2048_CHARACTERS);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageURL2048Characters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageURL2048Characters\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.URL_IS_2047_CHARACTERS);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateSuccessResponse(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageURL2047Characters() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageURL2047Characters\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.URL_IS_2047_CHARACTERS);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateSuccessResponse(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageURLIsNull() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageURLIsNull\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.URL_IS_NULL);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void addPackageURLIsMissing() throws IOException {
        LOGGER.info("********************Starting TestCase \"addPackageURLIsMissing\"********************");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.URL_IS_MISSING);
        Response response = addPackage(addPackageTestData, true);
        AddPackageUtils.validateMalformedRequest(response);

    }

}
