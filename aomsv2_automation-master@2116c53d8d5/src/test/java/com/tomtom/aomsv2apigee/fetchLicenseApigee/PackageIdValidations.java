/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2apigee.fetchLicenseApigee;

import com.tomtom.aomsv2.commonControls.BaseAPIGEE;
import com.tomtom.aomsv2.fetchLicense.FetchLicenseUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.xml.bind.JAXBException;

/**
 * This class has the test cases having different validations of Package Id field for Fetch License API and verifying
 * the response through APIGEE.
 * 
 * @author palvadi
 */
public class PackageIdValidations extends BaseAPIGEE {

    private Logger LOGGER = LoggerFactory.getLogger("FetchLicense.PackageIdValidations");
    HashMap<String, String> testdata;


    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIdIsInInuse() throws IOException, SQLException, ClassNotFoundException,
            JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePackageIdIsInInuse\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.INUSE_PACKAGE);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateSuccessResponse(response);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIsinOutofUse() throws IOException, SQLException, ClassNotFoundException,
            JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePackageIsinOutofUse\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.OUTOFUSE_PACKAGE);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIsDeprecated() throws IOException, SQLException, ClassNotFoundException,
            JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePackageIsDeprecated\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.DEPRECATED_PACKAGE);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateSuccessResponse(response);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIsNullAsString() throws IOException, SQLException, ClassNotFoundException,
            JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePackageIsNullAsString\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGE_ID_IS_NULL_AS_STRING);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIsNull() throws IOException, SQLException, ClassNotFoundException, JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePackageIsNull\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGEID_IS_NULL);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIsGreaterThan256Characters() throws IOException, SQLException,
            ClassNotFoundException, JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePackageIsGreaterThan256Characters\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGE_ID_GREATER_THAN_256_CHARACTERS);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIs256Characters() throws IOException, SQLException, ClassNotFoundException,
            JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePackageIs256Characters\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGE_ID_IS_256_CHARACTERS);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateSuccessResponse(response);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIs255Characters() throws IOException, SQLException, ClassNotFoundException,
            JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePackageIs255Characters\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGE_ID_IS_255_CHARACTERS);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateSuccessResponse(response);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIdIsNotinDatabase() throws IOException, SQLException, ClassNotFoundException,
            JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePackageIdIsNotinDatabase\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGE_ID_NOT_IN_DATABASE);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePassTwoPackageIds() throws IOException, SQLException, ClassNotFoundException, JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePassTwoPackageIds\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.TWO_PACKAGEIDS_IN_THE_REQUEST);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIdIsMissing() throws IOException, SQLException, ClassNotFoundException,
            JAXBException {

        LOGGER.info("********************Starting TestCase \"fetchLicensePackageIdIsMissing\"********************");

        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.TWO_PACKAGEIDS_IN_THE_REQUEST);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtils.validateMalformedRequest(response);

    }
}
