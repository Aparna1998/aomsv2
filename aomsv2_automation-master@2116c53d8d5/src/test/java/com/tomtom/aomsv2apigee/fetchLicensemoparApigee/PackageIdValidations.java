/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2apigee.fetchLicensemoparApigee;

import com.tomtom.aomsv2.fetchLicensemopar.FetchLicenseUtilsMopar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.xml.bind.JAXBException;

/**
 * This class has the test cases having different validations of Package Id field for Fetch License API and verifying
 * the response through ELB/Nodes.
 * 
 * @author palvadi
 */
public class PackageIdValidations {

    private Logger LOGGER = LoggerFactory.getLogger("FetchLicense.PackageIdValidations");
    HashMap<String, String> testdata;

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPackageIdIsInInuse() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPackageIdIsInInuse\"********************");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.INUSE_PACKAGE);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateSuccessResponse(response);

    }

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPackageIsinOutofUse() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPackageIsinOutofUse\"********************");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.OUTOFUSE_PACKAGE);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPackageIsDeprecated() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPackageIsDeprecated\"********************");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.DEPRECATED_PACKAGE);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateSuccessResponse(response);

    }

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPackageIsNullAsString() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPackageIsNullAsString\"");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.PACKAGE_ID_IS_NULL_AS_STRING);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPackageIsNull() throws IOException, SQLException, ClassNotFoundException, JAXBException,
            InterruptedException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPackageIsNull\"");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.PACKAGEID_IS_NULL);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPackageIsGreaterThan256Characters() throws IOException, SQLException,
            ClassNotFoundException, JAXBException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPackageIsGreaterThan256Characters\"");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.PACKAGE_ID_GREATER_THAN_256_CHARACTERS);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPackageIs256Characters() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPackageIs256Characters\"");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.PACKAGE_ID_IS_256_CHARACTERS);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateSuccessResponse(response);

    }

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPackageIs255Characters() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPackageIs255Characters\"");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.PACKAGE_ID_IS_255_CHARACTERS);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateSuccessResponse(response);

    }

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPackageIdIsNotinDatabase() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, InterruptedException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPackageIdIsNotinDatabase\"");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.PACKAGE_ID_NOT_IN_DATABASE);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPassTwoPackageIds() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPassTwoPackageIds\"");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.TWO_PACKAGEIDS_IN_THE_REQUEST);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateMalformedRequest(response);

    }

    @Test(groups = "FetchLicenseMopar")
    public void fetchLicenseMoparPackageIdIsMissing() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("********************Starting TestCase \"fetchLicenseMoparPackageIdIsMissing\"");

        testdata = FetchLicenseUtilsMopar.testData(FetchLicenseUtilsMopar.TWO_PACKAGEIDS_IN_THE_REQUEST);
        String requestbody = FetchLicenseUtilsMopar.xmlMapping(testdata);
        Response response = FetchLicenseUtilsMopar.getFetchLicenseResponse(requestbody, testdata, true);
        FetchLicenseUtilsMopar.validateMalformedRequest(response);

    }
}
