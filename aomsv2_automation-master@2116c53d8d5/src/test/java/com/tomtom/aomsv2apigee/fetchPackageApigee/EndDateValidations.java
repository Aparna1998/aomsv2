/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2apigee.fetchPackageApigee;

import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.commonControls.BaseAPIGEE;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.fetchPackage.FetchPackageUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations of End Date field for Fetch Package API and verifying the
 * response through APIGEE.
 * 
 * @author palvadi
 */
public class EndDateValidations extends BaseAPIGEE {

    private Logger LOGGER = LoggerFactory.getLogger("fetchPackage.EndDateValidations");
    HashMap<Object, Object> testdata;


    @Test(groups = "FetchPackage")
    public void fetchPackageEndDateisMissing() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageEndDateisMissing\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.END_DATE_MISSING);
        Response response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY)
                .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE)).param("endDate", "")
                .param("status", testdata.get(FetchPackageUtils.STATUS)).when().get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is Null ");
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateMalformedResponse(response);

    }

    @Test(groups = "FetchPackage")
    public void fetchPackageEndDateisInvalid() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageEndDateisInvalid\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.END_DATE_IS_INVALID);
        Response response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY)
                .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .param("status", testdata.get(FetchPackageUtils.STATUS)).when().get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateMalformedResponse(response);

    }

    @Test(groups = "FetchPackage")
    public void fetchPackageEndDateisHavingCharacters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageEndDateisHavingCharacters\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.END_DATE_IS_HAVING_CHARACTERS);
        Response response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY)
                .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .param("status", testdata.get(FetchPackageUtils.STATUS)).when().get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateMalformedResponse(response);

    }

    @Test(groups = "FetchPackage")
    public void fetchPackageEndDateisinDifferentFormat() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageEndDateisinDifferentFormat\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.END_DATE_IS_IN_DIFFERENT_FORMAT);
        Response response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY)
                .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .param("status", testdata.get(FetchPackageUtils.STATUS)).when().get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateMalformedResponse(response);

    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStartDateisGreaterThanEndDate() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageStartDateisGreaterThanEndDate\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.END_DATE_GREATER_THAN_START_DATE);

        Response response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY)
                .param("packageid", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .param("states", testdata.get(FetchPackageUtils.STATUS)).when().get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateMalformedResponse(response);

    }

    @Test(groups = "FetchPackage")
    public void fetchPackageEndDateIsSentTwice() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"fetchPackageStartDateisGreaterThanEndDate\"********************");

        testdata = FetchPackageUtils.testData(FetchPackageUtils.END_DATE_GREATER_THAN_START_DATE);
        Response response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY)
                .param("packageid", testdata.get(FetchPackageUtils.PACKAGE_ID))
                .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                .param("states", testdata.get(FetchPackageUtils.STATUS)).when().get(CommonData.FETCH_PACKAGE);

        LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
        LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
        LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
        LOGGER.info("The End Date set is sent twice with value " + testdata.get(FetchPackageUtils.END_DATE));
        LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
        LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
        LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

        FetchPackageUtils.validateMalformedResponse(response);

    }

}
