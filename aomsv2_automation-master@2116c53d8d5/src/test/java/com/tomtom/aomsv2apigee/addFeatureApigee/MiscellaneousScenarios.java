/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2apigee.addFeatureApigee;

import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.VALID_DATA;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.addFeature;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.addFeatureInvalidRequestBody;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.fetchResponsefromDbandCompare;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.testData;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.validateMalformedResponse;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.validateSuccessResponse;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.verifyDataIsNotInserted;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.commonControls.BaseAPIGEE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has different miscellaneous test cases Add Feature API and verifying the response through APIGEE.
 * 
 * @author palvadi
 */
public class MiscellaneousScenarios extends BaseAPIGEE {

    private static final Logger LOGGER = LoggerFactory.getLogger("addFeature.Miscellaneous");


    @Test(groups = "AddFeature")
    public void addFeatureWithValidData() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureWithValidData\"********************");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = testData(VALID_DATA);
        Response response = addFeature(testdata, true);
        validateSuccessResponse(response);
        fetchResponsefromDbandCompare(testdata, true);

    }

    @Test(groups = "AddFeature")
    public void addFeatureInvalidRequestFormat() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureInvalidRequestFormat\"********************");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = testData(VALID_DATA);
        Response response = addFeatureInvalidRequestBody(testdata, true);
        validateMalformedResponse(response);
        verifyDataIsNotInserted(testdata, true);

    }

    @Test(groups = "AddFeature")
    public void addFeatureNotPassingAnyParametersInTheRequest() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("********************Starting TestCase \"addFeatureNotPassingAnyParametersInTheRequest\"********************");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = testData(AddFeatureUtils.REQUEST_BODY_IS_EMPTY);
        Response response = addFeature(testdata, true);
        validateMalformedResponse(response);
        verifyDataIsNotInserted(testdata, true);

    }

}
