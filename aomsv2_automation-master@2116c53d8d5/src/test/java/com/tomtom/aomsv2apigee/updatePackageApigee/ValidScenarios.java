/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2apigee.updatePackageApigee;

import static io.restassured.RestAssured.given;

import com.tomtom.aomsv2.commonControls.BaseAPIGEE;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.request.entity.UpdatePackage;
import com.tomtom.aomsv2.updatePackage.UpdatePackageUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class has the test cases of Valid Scenarios for Update Package API through APIGEE.
 * 
 * @author palvadi
 */
public class ValidScenarios extends BaseAPIGEE {

    private static final Logger LOGGER = LoggerFactory.getLogger("UpdatePackage.UpdatePackage_ValidScenarios");


    @Test(groups = "UpdatePackage")
    public void oneUpdatePackageInuseToDeprecated() throws IOException {
        LOGGER.info("********************Starting TestCase \"oneUpdatePackageInuseToDeprecated\"********************");

        String packageIdInuse = UpdatePackageUtils.getPackageId("In-use");
        UpdatePackageUtils.updatePackage(packageIdInuse, "Deprecated", true);
        UpdatePackageUtils.updatePackage(packageIdInuse, "In-use", true);

    }

    @Test(groups = "UpdatePackage")
    public void twoUpdatePackageInuseToOutofUse() throws IOException {
        LOGGER.info("********************Starting TestCase \"twoUpdatePackageInuseToOutofUse\"********************");

        String packageIdInuse = UpdatePackageUtils.getPackageId("In-use");
        UpdatePackageUtils.updatePackage(packageIdInuse, "Out-of-use", true);
        UpdatePackageUtils.updatePackage(packageIdInuse, "In-use", true);

    }

    @Test(groups = "UpdatePackage")
    public void threeUpdatePackageInuseToInuse() throws IOException {
        LOGGER.info("********************Starting TestCase \"threeUpdatePackageInuseToInuse\"********************");

        String packageIdInuse = UpdatePackageUtils.getPackageId("In-use");
        UpdatePackageUtils.updatePackageToSameStatus(packageIdInuse, "In-use", true);

    }

    @Test(groups = "UpdatePackage")
    public void fourUpdatePackageOutofUseToInuse() throws IOException {
        LOGGER.info("********************Starting TestCase \"fourUpdatePackageOutofUseToInuse\"********************");

        String packageIdOutOfuse = UpdatePackageUtils.getPackageId("Out-of-use");
        UpdatePackageUtils.updatePackage(packageIdOutOfuse, "In-use", true);
        UpdatePackageUtils.updatePackage(packageIdOutOfuse, "Out-of-use", true);

    }

    @Test(groups = "UpdatePackage")
    public void fiveUpdatePackageOutofUseToDeprecated() throws IOException {
        LOGGER.info("********************Starting TestCase \"fiveUpdatePackageOutofUseToDeprecated\"********************");

        String packageIdOutOfuse = UpdatePackageUtils.getPackageId("Out-of-use");
        UpdatePackageUtils.updatePackage(packageIdOutOfuse, "Deprecated", true);
        UpdatePackageUtils.updatePackage(packageIdOutOfuse, "Out-of-use", true);

    }

    @Test(groups = "UpdatePackage")
    public void sixUpdatePackageOutofUseToOutofUse() throws IOException {
        LOGGER.info("********************Starting TestCase \"sixUpdatePackageOutofUseToOutofUse\"********************");

        String packageIdOutOfuse = UpdatePackageUtils.getPackageId("Out-of-use");
        UpdatePackageUtils.updatePackageToSameStatus(packageIdOutOfuse, "Out-of-use", true);

    }

    @Test(groups = "UpdatePackage")
    public void sevenUpdatePackageDeprecatedToInuse() throws IOException {
        LOGGER.info("********************Starting TestCase \"sevenUpdatePackageDeprecatedToInuse\"********************");

        String packageIdOutOfuse = UpdatePackageUtils.getPackageId("Deprecated");
        UpdatePackageUtils.updatePackage(packageIdOutOfuse, "In-use", true);
        UpdatePackageUtils.updatePackage(packageIdOutOfuse, "Deprecated", true);

    }

    @Test(groups = "UpdatePackage")
    public void eightUpdatePackageDeprecatedToOutofUse() throws IOException {
        LOGGER.info("********************Starting TestCase \"eightUpdatePackageDeprecatedToOutofUse\"********************");

        String packageIdOutOfuse = UpdatePackageUtils.getPackageId("Deprecated");
        UpdatePackageUtils.updatePackage(packageIdOutOfuse, "Out-of-use", true);
        UpdatePackageUtils.updatePackage(packageIdOutOfuse, "Deprecated", true);

    }

    @Test(groups = "UpdatePackage")
    public void nineUpdatePackageDeprecatedToDeprecated() throws IOException {
        LOGGER.info("********************Starting TestCase \"nineUpdatePackageDeprecatedToDeprecated\"********************");

        String packageIdOutOfuse = UpdatePackageUtils.getPackageId("Deprecated");
        UpdatePackageUtils.updatePackageToSameStatus(packageIdOutOfuse, "Deprecated", true);

    }

    @Test(groups = "UpdatePackage")
    public void tenUpdateMultiplePackagesinaSameRequest() throws IOException {
        LOGGER.info("********************Starting TestCase \"tenUpdateMultiplePackagesinaSameRequest\"********************");

        Map<String, String> testdata1 = new HashMap<String, String>();
        testdata1 = UpdatePackageUtils.testData(UpdatePackageUtils.MULTIPLE_PACKAGE_INUSE_PACKAGE1);
        UpdatePackage requestdata1 = new UpdatePackage();
        requestdata1.setPackageId(testdata1.get(UpdatePackageUtils.PACKAGE_ID));
        requestdata1.setState(testdata1.get(UpdatePackageUtils.STATUS));
        String requestbody1FromJson = UpdatePackageUtils.jsonMapping(requestdata1);

        Map<String, String> testdata2 = new HashMap<String, String>();
        testdata2 = UpdatePackageUtils.testData(UpdatePackageUtils.MULTIPLE_PACKAGE_INUSE_PACKAGE2);
        UpdatePackage requestdata2 = new UpdatePackage();
        requestdata2.setPackageId(testdata2.get(UpdatePackageUtils.PACKAGE_ID));
        requestdata2.setState(testdata2.get(UpdatePackageUtils.STATUS));
        String requestbody2FromJson = UpdatePackageUtils.jsonMapping(requestdata2);

        Map<String, String> testdata3 = new HashMap<String, String>();
        testdata3 = UpdatePackageUtils.testData(UpdatePackageUtils.MULTIPLE_PACKAGE_INUSE_PACKAGE3);
        UpdatePackage requestdata3 = new UpdatePackage();
        requestdata3.setPackageId(testdata3.get(UpdatePackageUtils.PACKAGE_ID));
        requestdata3.setState(testdata3.get(UpdatePackageUtils.STATUS));
        String requestbody3FromJson = UpdatePackageUtils.jsonMapping(requestdata3);

        Map<String, String> testdata4 = new HashMap<String, String>();
        testdata4 = UpdatePackageUtils.testData(UpdatePackageUtils.MULTIPLE_PACKAGE_INUSE_PACKAGE4);
        UpdatePackage requestdata4 = new UpdatePackage();
        requestdata4.setPackageId(testdata4.get(UpdatePackageUtils.PACKAGE_ID));
        requestdata4.setState(testdata4.get(UpdatePackageUtils.STATUS));
        String requestbody4FromJson = UpdatePackageUtils.jsonMapping(requestdata4);

        String requestbody = '[' + requestbody1FromJson + ',' + requestbody2FromJson + ',' + requestbody3FromJson + ','
                + requestbody4FromJson + ']';

        Response response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_PACKAGE_APIKEY).body(requestbody).when()
                .post(CommonData.UPDATE_PACKAGE);

        LOGGER.info("The request body is " + requestbody);
        LOGGER.info("The TenantId is " + testdata1.get(UpdatePackageUtils.TENANT_ID));
        LOGGER.info("The X-Request-Id is " + testdata1.get(UpdatePackageUtils.X_REQUEST_ID));
        UpdatePackageUtils.validateSuccessResponse(response);
    }
}