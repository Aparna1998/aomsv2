/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.addFeature;

import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.addFeature;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.fetchResponsefromDbandCompare;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.testData;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.verifyDataIsNotInserted;
import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for Description field for Add Feature API and verifying
 * the response.
 * 
 * @author palvadi
 */
public class DescriptionValidationsLogAutomation {

    private static final Logger LOGGER = LoggerFactory.getLogger("addFeature.AddFeatureDescriptionValidations");


   @Test(groups = "AddFeature")
    public void addFeaturewithDescriptionof1024characters() throws IOException, ClassNotFoundException, SQLException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException, InterruptedException {

        LOGGER.info("Starting TestCase \"addFeaturewithDescriptionof1024characters\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = testData(AddFeatureUtilsLogAutomation.DESCRIPTION_OF_1024_CHARACTERS);
        Response response = addFeature(testdata, false);
        AddFeatureUtilsLogAutomation.validateSuccessResponse(response);
        fetchResponsefromDbandCompare(testdata, false);
        Thread.sleep(40000);
        AddFeatureUtilsLogAutomation.verifyLogsForSuccessResponse(testdata.get(AddFeatureUtilsLogAutomation.FEATURE_ID), testdata.get(AddFeatureUtilsLogAutomation.XREQUESTID));

    }

    @Test(groups = "AddFeature")
    public void addFeaturewithDescriptionof1023characters() throws IOException, ClassNotFoundException, SQLException {

        try {
            LOGGER.info("Starting TestCase \"addFeaturewithDescriptionof1023characters\"");

            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.DESCRIPTION_OF_1023_CHARACTERS);
            Response response = addFeature(testdata, false);
            AddFeatureUtilsLogAutomation.validateSuccessResponse(response);
            fetchResponsefromDbandCompare(testdata, false);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            AddFeatureUtilsLogAutomation.verifyLogsForSuccessResponse(testdata.get(AddFeatureUtilsLogAutomation.FEATURE_ID), testdata.get(AddFeatureUtilsLogAutomation.XREQUESTID));

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());

        }

    }

    @Test(groups = "AddFeature")
    public void addFeaturewithDescriptiongreaterthan1024characters() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"addFeaturewithDescriptiongreaterthan1024characters\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.DESCRIPTION_GREATER_THAN_1024_CHARACTERS);
            Response response = addFeature(testdata, false);
            AddFeatureUtilsLogAutomation.validateMalformedResponse(response);
            verifyDataIsNotInserted(testdata, false);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            String LogMessage = "Value length is exceeding maximum length limit of 1024 for the property : description";
            AddFeatureUtilsLogAutomation.verifyLogsForMalformedResponse(testdata.get(AddFeatureUtilsLogAutomation.FEATURE_ID), testdata.get(AddFeatureUtilsLogAutomation.XREQUESTID), LogMessage);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "AddFeature")
    public void addFeaturewithDescriptionhasspecialcharacters() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"addFeaturewithDescriptionhasspecialcharacters\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.DESCRIPTION_HAS_SPECIAL_CHARACTERS);
            Response response = addFeature(testdata, false);
            AddFeatureUtilsLogAutomation.validateSuccessResponse(response);
            fetchResponsefromDbandCompare(testdata, false);
            LOGGER.info("Slept");
            Thread.sleep(100000);
            LOGGER.info("woke up");
            AddFeatureUtilsLogAutomation.verifyLogsForSuccessResponse(testdata.get(AddFeatureUtilsLogAutomation.FEATURE_ID), testdata.get(AddFeatureUtilsLogAutomation.XREQUESTID));

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }

    }

    @Test(groups = "AddFeature")
    public void addFeaturewithDescriptionisEmpty() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeaturewithDescriptionisEmpty\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.DESCRIPTION_IS_EMPTY);
            Response response = addFeature(testdata, false);
            AddFeatureUtilsLogAutomation.validateSuccessResponse(response);
            LOGGER.info("Slept");
            Thread.sleep(100000);
            LOGGER.info("woke up");
            AddFeatureUtilsLogAutomation.verifyLogsForSuccessResponse(testdata.get(AddFeatureUtilsLogAutomation.FEATURE_ID), testdata.get(AddFeatureUtilsLogAutomation.XREQUESTID));

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }

    }

    @Test(groups = "AddFeature")
    public void addFeaturewithDescriptionisNull() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeaturewithDescriptionisNull\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.DESCRIPTION_IS_NULL);
            Response response = addFeature(testdata, false);
            AddFeatureUtilsLogAutomation.validateMalformedResponse(response);
            verifyDataIsNotInserted(testdata, false);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            String LogMessage = "Value is null for the property : description";
            AddFeatureUtilsLogAutomation.verifyLogsForMalformedResponse(testdata.get(AddFeatureUtilsLogAutomation.FEATURE_ID), testdata.get(AddFeatureUtilsLogAutomation.XREQUESTID), LogMessage);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }
}
