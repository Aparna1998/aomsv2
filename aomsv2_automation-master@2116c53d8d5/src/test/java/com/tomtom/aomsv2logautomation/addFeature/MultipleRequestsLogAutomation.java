/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.addFeature;

import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.jsonMapping;
import static com.tomtom.aomsv2.addFeature.AddFeatureUtils.testData;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.fail;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.request.entity.AddFeatureRequestNew;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class has the test cases having different validations for Multiple Requests in the same request body and
 * verifying the response.
 * 
 * @author palvadi
 */
public class MultipleRequestsLogAutomation {

    private static final Logger LOGGER = LoggerFactory.getLogger("addFeature.AddFeatureMultipleRequests");

 

    @Test(groups = "AddFeature")
    public void addFeatureAddMultipleFeatureidsUsingSingleRequest() throws IOException {

        LOGGER.info("Starting TestCase \"addFeatureAddMultipleFeatureidsUsingSingleRequest\"");
        try {
            Map<String, String> testdata1 = new HashMap<String, String>();
            testdata1 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_FIRST);

            Map<String, String> testdata2 = new HashMap<String, String>();
            testdata2 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_SECOND);

            Map<String, String> testdata3 = new HashMap<String, String>();
            testdata3 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_THIRS);

            Map<String, String> testdata4 = new HashMap<String, String>();
            testdata4 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_FOURTH);

            Map<String, String> testdata5 = new HashMap<String, String>();
            testdata5 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_FIFTH);

            Map<String, String> testdata6 = new HashMap<String, String>();
            testdata6 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_SIXTH);

            AddFeatureRequestNew requestdata1 = new AddFeatureRequestNew();
            requestdata1.setFeatureId(testdata1.get(AddFeatureUtils.FEATURE_ID));
            requestdata1.setDescription(testdata1.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            AddFeatureRequestNew requestdata2 = new AddFeatureRequestNew();
            requestdata2.setFeatureId(testdata2.get(AddFeatureUtils.FEATURE_ID));
            requestdata2.setDescription(testdata2.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            AddFeatureRequestNew requestdata3 = new AddFeatureRequestNew();
            requestdata3.setFeatureId(testdata3.get(AddFeatureUtils.FEATURE_ID));
            requestdata3.setDescription(testdata3.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            AddFeatureRequestNew requestdata4 = new AddFeatureRequestNew();
            requestdata4.setFeatureId(testdata4.get(AddFeatureUtils.FEATURE_ID));
            requestdata4.setDescription(testdata4.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            AddFeatureRequestNew requestdata5 = new AddFeatureRequestNew();
            requestdata5.setFeatureId(testdata5.get(AddFeatureUtils.FEATURE_ID));
            requestdata5.setDescription(testdata5.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            AddFeatureRequestNew requestdata6 = new AddFeatureRequestNew();
            requestdata6.setFeatureId(testdata6.get(AddFeatureUtils.FEATURE_ID));
            requestdata6.setDescription(testdata6.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            String req_body = jsonMapping(requestdata1) + ',' + jsonMapping(requestdata2) + ','
                    + jsonMapping(requestdata3) + ',' + jsonMapping(requestdata4) + ',' + jsonMapping(requestdata5)
                    + ',' + jsonMapping(requestdata6);
            String requestbody = '[' + req_body + ']';
            LOGGER.info("The json request body is " + requestbody);

            Response response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata1.get(AddFeatureUtils.X_TENANT_ID))
                    .header("X-Request-Id", testdata1.get(AddFeatureUtils.X_REQUEST_ID)).when()
                    .put(CommonData.ADD_FEATURE);

            AddFeatureUtils.validateSuccessResponse(response);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }

    }

    @Test(groups = "AddFeature")
    public void addFeatureAddMultipleFeatureidsofSameFeatureId() throws IOException {

        LOGGER.info("Starting TestCase \"addFeatureAddMultipleFeatureidsofSameFeatureId\"");
        try {
            Map<String, String> testdata1 = new HashMap<String, String>();
            testdata1 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_FIRST);

            Map<String, String> testdata2 = new HashMap<String, String>();
            testdata2 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_FIRST);

            Map<String, String> testdata3 = new HashMap<String, String>();
            testdata3 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_FIRST);

            AddFeatureRequestNew requestdata1 = new AddFeatureRequestNew();
            AddFeatureRequestNew requestdata2 = new AddFeatureRequestNew();
            AddFeatureRequestNew requestdata3 = new AddFeatureRequestNew();

            requestdata1.setFeatureId(testdata1.get(AddFeatureUtils.FEATURE_ID));
            requestdata1.setDescription(testdata1.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            requestdata2.setFeatureId(testdata2.get(AddFeatureUtils.FEATURE_ID));
            requestdata2.setDescription(testdata2.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            requestdata3.setFeatureId(testdata3.get(AddFeatureUtils.FEATURE_ID));
            requestdata3.setDescription(testdata3.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            String requestbody1 = jsonMapping(requestdata1);
            LOGGER.info("The json request1 body is " + requestbody1);

            String requestbody2 = jsonMapping(requestdata2);
            LOGGER.info("The json request2 body is " + requestbody2);

            String requestbody3 = jsonMapping(requestdata3);
            LOGGER.info("The json request3 body is " + requestbody3);

            String requestbody = '[' + requestbody1 + ',' + requestbody2 + ',' + requestbody3 + ']';
            LOGGER.info("The json request body is " + requestbody);

            Response response = given().relaxedHTTPSValidation().body("sample")
                    .header("X-TENANT", testdata1.get(AddFeatureUtils.X_TENANT_ID))
                    .header("X-Request-Id", testdata1.get(AddFeatureUtils.X_REQUEST_ID)).when()
                    .put(CommonData.ADD_FEATURE);

            AddFeatureUtils.validateMalformedResponse(response);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }

    }

    @Test(groups = "AddFeature")
    public void addFeatureAddDifferentFeatureIdWithSameDescription() throws IOException {
        LOGGER.info("Starting TestCase \"addFeatureAddDifferentFeatureIdWithSameDescription\"");
        try {
            Map<String, String> testdata1 = new HashMap<String, String>();
            testdata1 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_FIRST);

            Map<String, String> testdata2 = new HashMap<String, String>();
            testdata2 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_SECOND);

            Map<String, String> testdata3 = new HashMap<String, String>();
            testdata3 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_THIRS);

            AddFeatureRequestNew requestdata1 = new AddFeatureRequestNew();
            requestdata1.setFeatureId(testdata1.get(AddFeatureUtils.FEATURE_ID));
            requestdata1.setDescription(testdata1.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            AddFeatureRequestNew requestdata2 = new AddFeatureRequestNew();
            requestdata2.setFeatureId(testdata2.get(AddFeatureUtils.FEATURE_ID));
            requestdata2.setDescription(testdata2.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            AddFeatureRequestNew requestdata3 = new AddFeatureRequestNew();
            requestdata3.setFeatureId(testdata3.get(AddFeatureUtils.FEATURE_ID));
            requestdata3.setDescription(testdata3.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            String requestbody1 = jsonMapping(requestdata1);
            String requestbody2 = jsonMapping(requestdata2);
            String requestbody3 = jsonMapping(requestdata3);

            String requestbody = '[' + requestbody1 + ',' + requestbody2 + ',' + requestbody3 + ']';
            LOGGER.info("The json request body is " + requestbody);

            Response response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata1.get(AddFeatureUtils.X_TENANT_ID))
                    .header("X-Request-Id", testdata1.get(AddFeatureUtils.X_REQUEST_ID)).when()
                    .put(CommonData.ADD_FEATURE);

            AddFeatureUtils.validateSuccessResponse(response);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "AddFeature")
    public void addFeatureAddMultipleFeatureidsWithOneFeatureIdAlreadyPresent() throws IOException {

        LOGGER.info("Starting TestCase \"addFeatureAddMultipleFeatureidsWithOneFeatureIdAlreadyPresent\"");
        try {
            Map<String, String> testdata1 = new HashMap<String, String>();
            testdata1 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_FIRST);

            Map<String, String> testdata2 = new HashMap<String, String>();
            testdata2 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_SECOND);

            Map<String, String> testdata3 = new HashMap<String, String>();
            testdata3 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_THIRS);

            Map<String, String> testdata4 = new HashMap<String, String>();
            testdata4 = testData(AddFeatureUtils.MULTIPLE_FEATURE_IDS_FOURTH);

            Map<String, String> testdata5 = new HashMap<String, String>();
            testdata5 = testData(AddFeatureUtils.FEATURE_ALREADY_PRESENT);

            AddFeatureRequestNew requestdata1 = new AddFeatureRequestNew();
            AddFeatureRequestNew requestdata2 = new AddFeatureRequestNew();
            AddFeatureRequestNew requestdata3 = new AddFeatureRequestNew();
            AddFeatureRequestNew requestdata4 = new AddFeatureRequestNew();
            AddFeatureRequestNew requestdata5 = new AddFeatureRequestNew();

            requestdata1.setFeatureId(testdata1.get(AddFeatureUtils.FEATURE_ID));
            requestdata1.setDescription(testdata1.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            requestdata2.setFeatureId(testdata2.get(AddFeatureUtils.FEATURE_ID));
            requestdata2.setDescription(testdata2.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            requestdata3.setFeatureId(testdata3.get(AddFeatureUtils.FEATURE_ID));
            requestdata3.setDescription(testdata3.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            requestdata4.setFeatureId(testdata4.get(AddFeatureUtils.FEATURE_ID));
            requestdata4.setDescription(testdata4.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            requestdata5.setFeatureId(testdata5.get(AddFeatureUtils.FEATURE_ID));
            requestdata5.setDescription(testdata5.get(AddFeatureUtils.FEATURE_DESCRIPTION));

            String requestbody1 = jsonMapping(requestdata1);
            LOGGER.info("The json request body is " + requestbody1);

            String requestbody2 = jsonMapping(requestdata2);
            LOGGER.info("The json request body is " + requestbody2);

            String requestbody3 = jsonMapping(requestdata3);
            LOGGER.info("The json request body is " + requestbody3);

            String requestbody4 = jsonMapping(requestdata4);
            LOGGER.info("The json request body is " + requestbody4);

            String requestbody5 = jsonMapping(requestdata5);
            LOGGER.info("The json request body is " + requestbody5);

            String requestbody = '[' + requestbody1 + ',' + requestbody2 + ',' + requestbody3 + ',' + requestbody4
                    + ',' + requestbody5 + ']';
            LOGGER.info(requestbody);
            Response response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata1.get(AddFeatureUtils.X_TENANT_ID))
                    .header("X-Request-Id", testdata1.get(AddFeatureUtils.X_REQUEST_ID)).when()
                    .put(CommonData.ADD_FEATURE);

            AddFeatureUtils.validateFeatureIdAlreadyExistsResponse(response);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

}
