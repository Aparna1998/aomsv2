/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.addFeature;

import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.VALID_DATA;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.addFeature;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.addFeatureHeadersAreEmpty;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.addFeatureInvalidRequestBody;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.fetchResponsefromDbandCompare;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.testData;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.validateMalformedResponse;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.validateSuccessResponse;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.verifyDataIsNotInserted;
import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has different miscellaneous test cases Add Feature API and verifying the response.
 * 
 * @author palvadi
 */
public class MiscellaneousScenariosLogAutomation {

    private static final Logger LOGGER = LoggerFactory.getLogger("addFeature.Miscellaneous");


    @Test(groups = "AddFeatureLogAutomation")
    public void addFeatureWithValidData() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeatureWithValidData\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(VALID_DATA);
            Response response = addFeature(testdata, false);
            validateSuccessResponse(response);
            String featureId = AddFeatureUtilsLogAutomation.getFeatureId(testdata);
            String XRequestId = AddFeatureUtilsLogAutomation.getXRequestId(testdata);
            LOGGER.info("The feature Id is " + featureId);
            LOGGER.info("The feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            AddFeatureUtilsLogAutomation.verifyLogsForSuccessResponse(featureId, XRequestId);
            fetchResponsefromDbandCompare(testdata, false);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }

    }

    @Test(groups = "AddFeature")
    public void addFeatureInvalidRequestFormat() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeatureInvalidRequestFormat\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(VALID_DATA);
            Response response = addFeatureInvalidRequestBody(testdata, false);
            validateMalformedResponse(response);
            verifyDataIsNotInserted(testdata, false);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "AddFeature")
    public void addFeatureNotPassingAnyParametersInTheRequest() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"addFeatureNotPassingAnyParametersInTheRequest\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.REQUEST_BODY_IS_EMPTY);
            Response response = addFeature(testdata, false);
            validateMalformedResponse(response);
            verifyDataIsNotInserted(testdata, false);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "AddFeature")
    public void addFeatureNotPassingAnyHeadersInTheRequest() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeatureNotPassingAnyHeadersInTheRequest\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.HEADERS_ARE_EMPTY);
            Response response = addFeatureHeadersAreEmpty(testdata);
            validateMalformedResponse(response);
            verifyDataIsNotInserted(testdata, false);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }
}
