/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.addFeature;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertNotEquals;

import com.tomtom.aomsv2.addFeature.AddFeatureUtils;
import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.commonControls.CommonMethods;
import com.tomtom.aomsv2.commonControls.Messages;
import com.tomtom.aomsv2.commonControls.ResponseCodes;
import com.tomtom.aomsv2.request.entity.AddFeatureRequestNew;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * This class has the common methods for code re usability by other classes of addFeature package.
 * 
 * @author palvadi
 */
public class AddFeatureUtilsLogAutomation {

    private static final Logger LOGGER = LoggerFactory.getLogger("AddFeature.AddFeatureUtils");

    public static Response response;
    private static final String CONTENT_LENGTH = "Content-Length";

    /* Variables defined for corresponding row numbers present in Add Feature Test data sheet */

    public static final int VALID_DATA = 1;
    public static final int FEATURE_ID_256_CHARACTERS = 2;
    public static final int FEATURE_ID_255_CHARACTERS = 3;
    public static final int FEATURE_ID_GREATER_THAN_256_CHARACTERS = 4;
    public static final int FEATURE_ID_HAS_SPECIAL_CHARACTER = 5;
    public static final int FEATURE_ID_NULL = 6;
    public static final int FEATURE_ID_IS_EMPTY = 7;
    public static final int DESCRIPTION_OF_1024_CHARACTERS = 8;
    public static final int DESCRIPTION_OF_1023_CHARACTERS = 9;
    public static final int DESCRIPTION_GREATER_THAN_1024_CHARACTERS = 10;
    public static final int DESCRIPTION_HAS_SPECIAL_CHARACTERS = 11;
    public static final int DESCRIPTION_IS_NULL = 12;
    public static final int DESCRIPTION_IS_EMPTY = 13;
    public static final int TENANT_ID_IS_256_CHARACTERS = 14;
    public static final int TENANT_ID_IS_255_CHARACTERS = 15;
    public static final int TENANT_ID_GREATER_THAN_256_CHARACTERS = 16;
    public static final int TENANT_ID_HAS_SPECIAL_CHARACTERS = 17;
    public static final int TENANT_ID_NOT_IN_DATABASE = 18;
    public static final int TENANT_ID_IS_NULL = 19;
    public static final int TENANT_ID_IS_NULL_AS_STRING = 20;
    public static final int TENANT_ID_IS_MISSING = 21;
    public static final int X_REQUEST_ID_OF_256_CHARACTERS = 22;
    public static final int X_REQUEST_ID_OF_255_CHARACTERS = 23;
    public static final int X_REQUEST_ID_GREATER_THAN_256_CHARACTERS = 24;
    public static final int X_REQUEST_ID_HAS_SPECIAL_CHARACTERS = 25;
    public static final int X_REQUEST_ID_NOT_IN_DATABASE = 26;
    public static final int MULTIPLE_FEATURE_IDS_FIRST = 27;
    public static final int MULTIPLE_FEATURE_IDS_SECOND = 28;
    public static final int MULTIPLE_FEATURE_IDS_THIRS = 29;
    public static final int MULTIPLE_FEATURE_IDS_FOURTH = 30;
    public static final int MULTIPLE_FEATURE_IDS_FIFTH = 31;
    public static final int MULTIPLE_FEATURE_IDS_SIXTH = 32;
    public static final int FEATURE_ALREADY_PRESENT = 33;
    public static final int REQUEST_BODY_IS_EMPTY = 34;
    public static final int HEADERS_ARE_EMPTY = 35;
    public static final int FEATURE_E2E_SCENARIO = 36;
    // Variables defined for test data Hash Map of the method 'testData'
    public static final String FEATURE_ID = "Featureid";
    public static final String FEATURE_DESCRIPTION = "FeatureDescription";
    public static final String X_TENANT_ID = "X_Tenant_Id";
    public static final String X_REQUEST_ID = "X_requestId";
    public static final String DB_FEATURE_ID = "Db_FeatureId";
    public static final String DB_TENANT_ID = "Db_TenantId";
    public static final String DB_DESCRIPTION = "Db_Description";
    public static final String PACKAGE_ID = "packageId";

    public static final String SUCCESSRESPONSE = "successresponse";
    public static final String MALFORMEDRESPONSE = "malformedresponse";
    public static final String CLIENTUNAUTHORIZED = "clientunauthorized";
    public static final String FEATUREIDALREADYPRESENT = "featurealreadypresent";
    public static String XREQUESTID = "xrequestid";
    public static String FEATUREID = "featureid";
    public static String HTTPSTATUSCODE = "httpstatuscode";
    public static String HTTPSTATUSMESSAGE = "httpstatusmessage";
    public static final String LOGFILENAME = "/var/log/aomsv2/AddFeature.log";
    public static String MESSAGE = "Message";

    public static final String X_REQUEST_ID_RANDOM = null;
    public static String category = "category";

    @Test
    public static HashMap<String, String> testData(int rownum) throws IOException {

        /*
         * This method fetches data from AddFeature Test Data sheet and stores the data in HashMap created for each
         * corresponding test case
         */

        HashMap<String, String> hm = new HashMap<String, String>(100);
        Random rd = new Random();

        int randInt1 = rd.nextInt(100000);

        String sALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * sALTCHARS.length());
            salt.append(sALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();

        InputStream fis = AddFeatureUtils.class.getClassLoader().getResourceAsStream("testData/AddFeature.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet testdatasheet = wb.getSheet("AddFeature");
        XSSFRow currentRow = testdatasheet.getRow(rownum);

        XSSFCell featureid = currentRow.getCell(1);
        String featureId = featureid.toString();
        String featureIdStr;
        if (featureId.equals("new")) {
            featureIdStr = featureId;
        } else if (featureId.equals("NULL")) {
            featureIdStr = featureId;
        } else if (featureId.equals("EMPTY")) {
            featureIdStr = featureId;
        } else {
            featureIdStr = featureId + randInt1 + saltStr;
        }
        hm.put(FEATURE_ID, featureIdStr);

        XSSFCell description = currentRow.getCell(2);
        String descriptionStr = description.toString();
        hm.put(FEATURE_DESCRIPTION, descriptionStr);

        XSSFCell xTenantId = currentRow.getCell(3);
        String xTenantIdStr = xTenantId.toString();
        hm.put(X_TENANT_ID, xTenantIdStr);

        XSSFCell xRequestID = currentRow.getCell(4);
        String xRequestId = xRequestID.toString();
        String xRequestId_random = randInt1 + saltStr;
        hm.put(X_REQUEST_ID, xRequestId);
        hm.put(X_REQUEST_ID_RANDOM, xRequestId_random);

        fis.close();
        wb.close();
        return hm;
    }

    public static String jsonMapping(AddFeatureRequestNew requestdata) {
        // This method converts the request data taken from the excel to JSON format
        List<JSONObject> list;
        JSONObject jsonobject = new JSONObject(requestdata);
        list = new ArrayList<JSONObject>();
        list.add(jsonobject);
        String reqbody = list.toString();
        int endIndex = reqbody.length();
        String requestbody = reqbody.substring(1, endIndex - 1);
        return requestbody;
    }

    public static Response addFeature(HashMap<String, String> testdata, boolean apigeeFlag) {

        // This method creates the Rest Assured request and fetch the response
        AddFeatureRequestNew requestdata = new AddFeatureRequestNew();

        if (testdata.get(AddFeatureUtils.FEATURE_ID).equals("NULL")) {
            requestdata.setFeatureId(null);
        } else if (testdata.get(AddFeatureUtils.FEATURE_ID).equals("EMPTY")) {
            requestdata.setFeatureId("");
        } else {
            requestdata.setFeatureId(testdata.get(AddFeatureUtils.FEATURE_ID));

        }

        if (testdata.get(AddFeatureUtils.FEATURE_DESCRIPTION).equals("NULL")) {
            requestdata.setDescription(null);
        } else if (testdata.get(AddFeatureUtils.FEATURE_DESCRIPTION).equals("EMPTY")) {
            requestdata.setDescription("");
        } else {
            requestdata.setDescription(testdata.get(AddFeatureUtils.FEATURE_DESCRIPTION));
        }

        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';
        LOGGER.info("The json request body is " + requestbody);
        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY).body(requestbody)
                    .when().put("/aomsv2/v1/aois/feature");
            response.body().print();
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata.get(AddFeatureUtils.X_TENANT_ID))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID_RANDOM)).when().put("/aomsv2/v1/aois/feature");

            response.body().print();
        }

        LOGGER.info("The TenantId set is : " + testdata.get(X_TENANT_ID));
        LOGGER.info("The XRequestId set is :" + testdata.get(X_REQUEST_ID));

        return response;
    }

    public static String getFeatureId(HashMap<String, String> hm) {

        String FeatureId = hm.get(FEATURE_ID);
        return FeatureId;

    }

    public static String getXRequestId(HashMap<String, String> hm) {

        String XRequestId = hm.get(AddFeatureUtils.X_REQUEST_ID_RANDOM);
        return XRequestId;

    }

    public static Response addFeatureTenantValidations(HashMap<String, String> testdata) {

        /*
         * This method creates the Rest Assured request for test cases dealing with Tenant Validations and fetch the
         * response
         */

        AddFeatureRequestNew requestdata = new AddFeatureRequestNew();
        requestdata.setFeatureId(testdata.get(AddFeatureUtils.FEATURE_ID));
        requestdata.setDescription(testdata.get(AddFeatureUtils.FEATURE_DESCRIPTION));
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';
        LOGGER.info("The json request body is " + requestbody);

        switch (testdata.get(AddFeatureUtils.X_TENANT_ID)) {

        case "NULL":

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", "")
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_FEATURE);

            LOGGER.info("The TenantId set is : null");
            LOGGER.info("The XRequestId set is :" + testdata.get(X_REQUEST_ID));

            break;

        case "NULLASSTRING":

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", "null")
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_FEATURE);

            LOGGER.info("The TenantId set is : 'null'");
            LOGGER.info("The XRequestId set is : " + testdata.get(X_REQUEST_ID));

            break;

        case "MISSING":

            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_FEATURE);

            LOGGER.info("The TenantId is not set");
            LOGGER.info("The XRequestId set is :" + testdata.get(X_REQUEST_ID));

            break;

        default:
            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata.get(AddFeatureUtils.X_TENANT_ID))
                    .header("X-Request-Id", testdata.get(AddFeatureUtils.X_REQUEST_ID_RANDOM)).when()
                    .put(CommonData.ADD_FEATURE);

            LOGGER.info("The TenantId set is : " + testdata.get(X_REQUEST_ID_RANDOM));
            LOGGER.info("The XRequestId set is : " + testdata.get(X_REQUEST_ID));
            break;

        }

        return response;
    }

    public static Response addFeatureXRequestValidations(HashMap<String, String> testdata) {

        /*
         * This method creates the Rest Assured request for test cases dealing with XRequest Validations and fetch the
         * response
         */

        AddFeatureRequestNew requestdata = new AddFeatureRequestNew();
        requestdata.setFeatureId(testdata.get(AddFeatureUtils.FEATURE_ID));
        requestdata.setDescription(testdata.get(AddFeatureUtils.FEATURE_DESCRIPTION));
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';
        LOGGER.info(("The json request body is " + requestbody));
        LOGGER.info("testing: " + testdata.get(AddFeatureUtils.X_REQUEST_ID));
        switch (testdata.get(AddFeatureUtils.X_REQUEST_ID)) {

        case "NULL":

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(X_TENANT_ID))
                    .header("X-Request-Id", "").when().put(CommonData.ADD_FEATURE);

            LOGGER.info("The TenantId set is : " + testdata.get(X_TENANT_ID));
            LOGGER.info("The XRequestId set is : null");

            break;

        case "NULLASSTRING":

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(X_TENANT_ID))
                    .header("X-Request-Id", "null").when().put(CommonData.ADD_FEATURE);

            LOGGER.info("The TenantId set is : " + testdata.get(X_TENANT_ID));
            LOGGER.info("The XRequestId set is : 'null'");

            break;

        case "MISSING":

            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(X_TENANT_ID))
                    .when().put(CommonData.ADD_FEATURE);

            LOGGER.info("The TenantId set is : " + testdata.get(X_TENANT_ID));
            LOGGER.info("The XRequestId set is not set");
            break;

        case "LENGTHGREATERTHAN256":

            response = given()
                    .relaxedHTTPSValidation()
                    .body(requestbody)
                    .header("X-TENANT", testdata.get(AddFeatureUtils.X_TENANT_ID))
                    .header("X-Request-Id",
                            "Featurefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeqewrwewreewrewredfdsfsadfdsafdsa")
                    .when().put(CommonData.ADD_FEATURE);

            LOGGER.info("The TenantId set is : " + testdata.get(X_TENANT_ID));
            LOGGER.info("The XRequestId set is : "
                    + "Featurefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeaturefeqewrwewreewrewredfdsfsadfdsafdsa");

            break;

        default:
            response = given().relaxedHTTPSValidation().body(requestbody)
                    .header("X-TENANT", testdata.get(AddFeatureUtils.X_TENANT_ID))
                    .header("X-Request-Id", testdata.get(AddFeatureUtils.X_REQUEST_ID_RANDOM)).when()
                    .put(CommonData.ADD_FEATURE);

            LOGGER.info("The TenantId set is : " + testdata.get(X_TENANT_ID));
            LOGGER.info("The XRequestId set is : " + testdata.get(X_REQUEST_ID_RANDOM));
            break;

        }

        return response;

    }

    public static Response addFeatureHeadersAreEmpty(HashMap<String, String> testdata) {

        /* This method creates the Rest Assured request if headers are empty and fetch the response */

        AddFeatureRequestNew requestdata = new AddFeatureRequestNew();
        requestdata.setFeatureId(testdata.get(AddFeatureUtils.FEATURE_ID));
        requestdata.setDescription(testdata.get(AddFeatureUtils.FEATURE_DESCRIPTION));
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']';
        LOGGER.info("The json request body is " + requestbody);
        response = given().relaxedHTTPSValidation().body(requestbody).when().put(CommonData.ADD_FEATURE);

        return response;

    }

    public static Response addFeatureInvalidRequestBody(HashMap<String, String> testdata, boolean apigeeFlag) {

        /* This method creates the Rest Assured request for test case of invalid request body and fetch the response */

        AddFeatureRequestNew requestdata = new AddFeatureRequestNew();
        requestdata.setDescription(testdata.get(FEATURE_DESCRIPTION));
        String requestbodyFromJson = jsonMapping(requestdata);
        String requestbody = '[' + requestbodyFromJson + ']' + ']'; // invalid request body
        LOGGER.info("The json request body is " + requestbody);

        Response response = null;
        if (apigeeFlag) {
            response = given().config(RestAssuredConfig.newConfig().sslConfig(CommonMethods.getSSLConfig()))
                    .queryParam(CommonData.API_KEY, CommonData.APIGEE_CS_SERVICES_FEATURE_APIKEY).body(requestbody)
                    .when().put(CommonData.ADD_FEATURE);
        } else {
            response = given().relaxedHTTPSValidation().body(requestbody).header("X-TENANT", testdata.get(X_TENANT_ID))
                    .header("X-Request-Id", testdata.get(X_REQUEST_ID)).when().put(CommonData.ADD_FEATURE);
        }

        LOGGER.info("The TenantId set is : " + testdata.get(X_TENANT_ID));
        LOGGER.info("The XRequestId set is : " + testdata.get(X_REQUEST_ID));

        return response;

    }

    public static void validateSuccessResponse(Response response) {

        /* This method validates the response passed is Valid Success Response */

        response.then().assertThat().statusCode(ResponseCodes.ADD_FEATURE_SUCCESS_RESPONSE_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.ADD_FEATURE_CREATED);

        LOGGER.info("The Response Message is " + Messages.ADD_FEATURE_CREATED);
        LOGGER.info("The Content Type is " + ContentType.JSON);
        // LOGGER.info("The Content Length is " + ContentLengthInteger);

    }

    public static void validateMalformedResponse(Response response) {

        // This method validates the response passed is Bad Request Response

        response.then().assertThat().statusCode(ResponseCodes.ADD_FEATURE_MALFORMED_REQUEST_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.ADD_FEATURE_MALFORMED_REQUEST);
        String contentLength = response.getHeader(CONTENT_LENGTH);
        int contentLengthInt = Integer.parseInt(contentLength);
        assertNotEquals(2, contentLengthInt);

        LOGGER.info("The Response Message is " + Messages.ADD_FEATURE_MALFORMED_REQUEST);
        LOGGER.info("The Content Type is " + ContentType.JSON);
        LOGGER.info("The Content Length is " + contentLengthInt);
    }

    public static void validateclientUnauthorizedResponse(Response response) {

        // This method validates the response passed is Client Unauthorized Response

        response.then().assertThat().statusCode(ResponseCodes.ADD_FEATURE_CLIENT_UNAUTHORIZED_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.ADD_FEATURE_CLIENT_UNAUTHORIZED);
        String contentLength = response.getHeader(CONTENT_LENGTH);
        int contentLengthInt = Integer.parseInt(contentLength);
        assertNotEquals(2, contentLengthInt);

        LOGGER.info("The Response Message is " + Messages.ADD_FEATURE_CLIENT_UNAUTHORIZED);
        LOGGER.info("The Content Type is " + ContentType.JSON);
        LOGGER.info("The Content Length is " + contentLengthInt);

    }

    public static void validateFeatureIdAlreadyExistsResponse(Response response) {

        /* This method validates the response passed is FeatureId Already Exists Response */

        response.then().assertThat().statusCode(ResponseCodes.ADD_FEATURE_ID_ALREADY_PRESENT_CODE).and().assertThat()
                .contentType(ContentType.JSON).and().assertThat().statusLine(Messages.ADD_FEATURE_ID_ALREADY_PRESENT);
        String contentLength = response.getHeader(CONTENT_LENGTH);
        int contentLengthInt = Integer.parseInt(contentLength);
        assertNotEquals(2, contentLengthInt);

        LOGGER.info("The Response Message is " + Messages.ADD_FEATURE_ID_ALREADY_PRESENT);
        LOGGER.info("The Content Type is " + ContentType.JSON);
        LOGGER.info("The Content Length is " + contentLengthInt);

    }

    public static void fetchResponsefromDbandCompare(HashMap<String, String> testdata, boolean apigeeFlag)
            throws SQLException, ClassNotFoundException {

        /* This method fetches the response from database and compare against the response received */

        HashMap<String, String> testdatafromexcel = new HashMap<String, String>();
        testdatafromexcel = testdata;
        String featureIdInsertedThroughAPI = testdatafromexcel.get(FEATURE_ID);
        String descriptionInsertedThroughAPI = testdatafromexcel.get(FEATURE_DESCRIPTION);
        String tenantIdInsertedThroughAPI = testdatafromexcel.get(X_TENANT_ID);

        HashMap<String, String> dbresponse = new HashMap<String, String>();
        Connection con = null;
        if (apigeeFlag) {
            con = CommonMethods.connectDBAoisSchemaWithPreprodAPIGEE();
        } else {
            con = CommonMethods.connectDBAoisSchema();
        }
        Statement stmt = con.createStatement();
        String Query = "select * from aois.addon_feature where id=" + "\'" + featureIdInsertedThroughAPI + "\'" + ";";

        ResultSet rs = stmt.executeQuery(Query);
        if (rs.next()) {

            String featureId = rs.getString(1);
            LOGGER.info("The db response for feature id is" + featureId);
            dbresponse.put(DB_FEATURE_ID, featureId);

            String tenantId = rs.getString(2);
            LOGGER.info("The db response for tenant id is" + tenantId);
            dbresponse.put(DB_TENANT_ID, tenantId);

            String description = rs.getString(3);
            LOGGER.info("The db response for description is" + description);
            dbresponse.put(DB_DESCRIPTION, description);

        }

        String featureIdFromDatabase = dbresponse.get(DB_FEATURE_ID);
        String descriptionFromDatabase = dbresponse.get(DB_DESCRIPTION);
        String tenantFromDatabase = dbresponse.get(DB_TENANT_ID);
        AssertJUnit.assertEquals(featureIdFromDatabase, featureIdInsertedThroughAPI);
        AssertJUnit.assertEquals(descriptionFromDatabase, descriptionInsertedThroughAPI);
        if (!apigeeFlag) {
            AssertJUnit.assertEquals(tenantFromDatabase, tenantIdInsertedThroughAPI);
        }

        LOGGER.info("The database is reflecting the values inserted through API");

    }

    public static void verifyDataIsNotInserted(HashMap<String, String> testdata, boolean apigeeFlag)
            throws ClassNotFoundException, SQLException {

        /* This method verifies the database that data is not inserted when an invalid request is sent */

        HashMap<String, String> testdatafromexcel = new HashMap<String, String>();
        testdatafromexcel = testdata;
        Connection con = null;
        if (apigeeFlag) {
            con = CommonMethods.connectDBAoisSchemaWithPreprodAPIGEE();
        } else {
            con = CommonMethods.connectDBAoisSchema();
        }
        Statement stmt = con.createStatement();
        String featureIdInsertedThroughAPI = testdatafromexcel.get(FEATURE_ID);
        LOGGER.info("Feature ID: " + featureIdInsertedThroughAPI);
        String Query = "select * from aois.addon_feature where id=" + "\'" + featureIdInsertedThroughAPI + "\'" + ";";
        ResultSet rs = stmt.executeQuery(Query);

        if (!rs.next()) {
            AssertJUnit.assertTrue(true);
        } else {
            AssertJUnit.assertFalse(true);
        }

    }

    public static void verifyLogsForMalformedResponse(String featureId, String xrequestId, String logMessage)
            throws UnrecoverableKeyException, KeyManagementException, MalformedURLException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, IOException {
        try {

            HTTPSTATUSCODE = "HttpStatusCode: 400";
            HTTPSTATUSMESSAGE = "Bad Request";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xrequestId
                    + "\"}},{\"match\":{\"message\":\"Response Body:" + logMessage
                    + " \"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + " \"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
                    + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            CommonMethods.verifyLogsfromKibana(urlParameters);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void verifylogsforClientUnauthorized(String featureId, String xRequestId)
            throws UnrecoverableKeyException, KeyManagementException, MalformedURLException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, IOException {

        try {
            HTTPSTATUSCODE = "HttpStatusCode: 403";
            HTTPSTATUSMESSAGE = "Client Unauthorized";
            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + featureId
                    + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + " \"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
                    + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The search query is: " + urlParameters);

            CommonMethods.verifyLogsfromKibana(urlParameters);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void verifyLogsForSuccessResponse(String xRequestId, String featureId) throws MalformedURLException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException, IOException, InterruptedException {

        try {
            HTTPSTATUSCODE = "HttpStatusCode: 204";
            HTTPSTATUSMESSAGE = "Feature is created successfully";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + featureId
                    + "\"}},{\"match\":{\"message\":\"INFO\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
                    + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The search query is: " + urlParameters);

            CommonMethods.verifyLogsfromKibana(urlParameters);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void verifyLogsForFeatureAlreadyPresent(String xRequestId, String featureId)
            throws MalformedURLException, IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        try {

            HTTPSTATUSCODE = "HttpStatusCode: 409";
            HTTPSTATUSMESSAGE = "Feature with the given ID already exists";

            String urlParameters = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"message\":\"" + xRequestId
                    + "\"}},{\"match\":{\"message\":\"" + featureId
                    + "\"}},{\"match\":{\"message\":\"ERROR\"}},{\"match\":{\"message\":\"Response Body:"
                    + HTTPSTATUSCODE + "\"}},{\"match\":{\"message\":\"" + HTTPSTATUSMESSAGE
                    + "\"}},{\"match\":{\"source\":\"" + LOGFILENAME + "\"}}]}}}";

            LOGGER.info("The search query is: " + urlParameters);

            CommonMethods.verifyLogsfromKibana(urlParameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
