/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.addFeature;

import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.addFeature;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.fetchResponsefromDbandCompare;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.testData;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.validateMalformedResponse;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.validateSuccessResponse;
import static com.tomtom.aomsv2logautomation.addFeature.AddFeatureUtilsLogAutomation.verifyDataIsNotInserted;
import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for Description field for Add Feature API and verifying
 * the response.
 * 
 * @author palvadi
 */
public class FeatureIdValidationsLogAutomation {

    private static final Logger LOGGER = LoggerFactory.getLogger("addFeature.AddFeatureFeatureIdValidations");


    @Test(groups = "AddFeature")
    public void addFeatureWithFeatureIdof256characters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeatureWithFeatureIdof256characters\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.FEATURE_ID_256_CHARACTERS);
            Response response = addFeature(testdata, false);
            validateSuccessResponse(response);
            String featureId = AddFeatureUtilsLogAutomation.getFeatureId(testdata);
            String XRequestId = AddFeatureUtilsLogAutomation.getXRequestId(testdata);
            LOGGER.info("The Feature Id is " + featureId);
            LOGGER.info("The XRequest Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            AddFeatureUtilsLogAutomation.verifyLogsForSuccessResponse(featureId, XRequestId);
            fetchResponsefromDbandCompare(testdata, false);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "AddFeature")
    public void addFeatureWithFeatureIdof255characters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeatureWithFeatureIdof255characters\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.FEATURE_ID_255_CHARACTERS);
            Response response = addFeature(testdata, false);
            validateSuccessResponse(response);
            String featureId = AddFeatureUtilsLogAutomation.getFeatureId(testdata);
            String XRequestId = AddFeatureUtilsLogAutomation.getXRequestId(testdata);
            LOGGER.info("The Feature Id is " + featureId);
            LOGGER.info("The XRequest Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            AddFeatureUtilsLogAutomation.verifyLogsForSuccessResponse(featureId, XRequestId);
            fetchResponsefromDbandCompare(testdata, false);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "AddFeature")
    public void addFeatureWithFeatureIdgreaterthan256characters() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"addFeatureWithFeatureIdgreaterthan256characters\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.FEATURE_ID_GREATER_THAN_256_CHARACTERS);
            Response response = addFeature(testdata, false);
            validateMalformedResponse(response);
            verifyDataIsNotInserted(testdata, false);
            String featureId = AddFeatureUtilsLogAutomation.getFeatureId(testdata);
            String XRequestId = AddFeatureUtilsLogAutomation.getXRequestId(testdata);
            LOGGER.info("The Feature Id is " + featureId);
            LOGGER.info("The XRequest Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            String LogMessage = "Value length is exceeding maximum length limit of 256 for the property : featureId";
            AddFeatureUtilsLogAutomation.verifyLogsForMalformedResponse(featureId, XRequestId, LogMessage);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }

    }

    @Test(groups = "AddFeature")
    public void addFeatureWithFeatureIdhasspecialcharacters() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeatureWithFeatureIdhasspecialcharacters\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.FEATURE_ID_HAS_SPECIAL_CHARACTER);
            Response response = addFeature(testdata, false);
            AddFeatureUtilsLogAutomation.validateSuccessResponse(response);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "AddFeature")
    public void addFeatureWhenFeatureIdisEmpty() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeatureWhenFeatureIdisEmpty\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.FEATURE_ID_IS_EMPTY);
            Response response = addFeature(testdata, false);
            validateMalformedResponse(response);
            verifyDataIsNotInserted(testdata, false);
            String featureId = AddFeatureUtilsLogAutomation.getFeatureId(testdata);
            String XRequestId = AddFeatureUtilsLogAutomation.getXRequestId(testdata);
            LOGGER.info("The Feature Id is " + featureId);
            LOGGER.info("The XRequest Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            String LogMessage = "Value is empty for the property : featureId";
            AddFeatureUtilsLogAutomation.verifyLogsForMalformedResponse(featureId, XRequestId, LogMessage);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "AddFeature")
    public void addFeatureWhenFeatureIdisNull() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeatureWhenFeatureIdisNull\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.FEATURE_ID_NULL);
            Response response = addFeature(testdata, false);
            validateMalformedResponse(response);
            verifyDataIsNotInserted(testdata, false);
            String featureId = AddFeatureUtilsLogAutomation.getFeatureId(testdata);
            String XRequestId = AddFeatureUtilsLogAutomation.getXRequestId(testdata);
            LOGGER.info("The Feature Id is " + featureId);
            LOGGER.info("The XRequest Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            String LogMessage = "Value is null for the property : featureId";
            AddFeatureUtilsLogAutomation.verifyLogsForMalformedResponse(featureId, XRequestId, LogMessage);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }

    }

    @Test(groups = "AddFeature")
    public void addFeatureWhenFeatureIdisMissing() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeatureWhenFeatureIdisMissing\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.FEATURE_ID_NULL);
            Response response = addFeature(testdata, false);
            validateMalformedResponse(response);
            verifyDataIsNotInserted(testdata, false);
            String featureId = AddFeatureUtilsLogAutomation.getFeatureId(testdata);
            String XRequestId = AddFeatureUtilsLogAutomation.getXRequestId(testdata);
            LOGGER.info("The Feature Id is " + featureId);
            LOGGER.info("The XRequest Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            String LogMessage = "due to failure in validation";
            AddFeatureUtilsLogAutomation.verifyLogsForMalformedResponse(featureId, XRequestId, LogMessage);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "AddFeature")
    public void addFeatureIdAlreadyPresent() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"addFeatureIdAlreadyPresent\"");
        try {
            HashMap<String, String> testdata = new HashMap<String, String>();
            testdata = testData(AddFeatureUtilsLogAutomation.FEATURE_ALREADY_PRESENT);
            Response response = addFeature(testdata, false);
            AddFeatureUtilsLogAutomation.validateFeatureIdAlreadyExistsResponse(response);
            String featureId = AddFeatureUtilsLogAutomation.getFeatureId(testdata);
            String XRequestId = AddFeatureUtilsLogAutomation.getXRequestId(testdata);
            LOGGER.info("The Feature Id is " + featureId);
            LOGGER.info("The XRequest Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            AddFeatureUtilsLogAutomation.verifyLogsForFeatureAlreadyPresent(featureId, XRequestId);
            ;
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }

    }

}
