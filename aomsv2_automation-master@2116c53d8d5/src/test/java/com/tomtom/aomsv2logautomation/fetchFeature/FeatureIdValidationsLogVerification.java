/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.fetchFeature;

import com.tomtom.aomsv2.fetchFeature.FetchFeatureUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations of Feature Id field for Fetch Feature API and verifying
 * the response.
 * 
 * @author palvadi
 */
public class FeatureIdValidationsLogVerification {

    private static final Logger LOGGER = LoggerFactory.getLogger("FetchFeature.FeatureIdValidations");



    @Test(groups = "FetchFeature")
    public void fetchFeatureValidFeatureId() throws IOException, SQLException, ClassNotFoundException,
            InterruptedException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchFeatureValidResponse\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.VALID_DATA);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateSuccessResponse(response);
        FetchFeatureUtils.checkDatabaseAndCompare(testdata, false);
        FetchFeatureUtils.VerifyResponseCountforSingleResponse(response);
        String FeatureId = FetchFeatureUtils.getFeatureId(testdata);
        String XRequestId = FetchFeatureUtils.getXRequestId(testdata);
        LOGGER.info("The Feature Id is " + FeatureId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchFeatureUtils.verifylogsforSuccessResponse(FeatureId, XRequestId);

    }

    @Test(groups = "FetchFeature")
    public void featureIdNotinDatabase() throws IOException, SQLException, ClassNotFoundException {

        LOGGER.info("Starting TestCase \"featureIdNotinDatabase\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.FEATURE_ID_NOT_IN_DATABASE);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateResponseBodyisNull(response);
        FetchFeatureUtils.verifyDataIsNotPresentInDb(testdata, false);

    }

    @Test(groups = "FetchFeature")
    public void featureIdis256CharactersAndNotinDatabase() throws IOException, SQLException, ClassNotFoundException {

        LOGGER.info("Starting TestCase \"featureIdis256CharactersAndNotinDatabase\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.FEATURE_ID_256_CHARACTERS_NOT_IN_DATABASE);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateResponseBodyisNull(response);
        FetchFeatureUtils.verifyDataIsNotPresentInDb(testdata, false);

    }

    @Test(groups = "FetchFeature")
    public void featureIdis257Characters() throws IOException, SQLException, ClassNotFoundException {
        LOGGER.info("Starting TestCase \"featureIdis257Characters\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.FEATURE_ID_257_CHARACTERS);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateMalformedResponse(response);
        FetchFeatureUtils.verifyDataIsNotPresentInDb(testdata, false);

    }

    @Test(groups = "FetchFeature")
    public void featureIdis255CharactersAndNotinDatabase() throws IOException, SQLException, ClassNotFoundException {
        LOGGER.info("Starting TestCase \"featureIdis255CharactersAndNotinDatabase\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.FEATURE_ID_255_CHARACTERS_NOT_IN_DATABASE);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateResponseBodyisNull(response);
        FetchFeatureUtils.verifyDataIsNotPresentInDb(testdata, false);
    }

    @Test(groups = "FetchFeature")
    public void featureIdis256CharactersAndPresentinDatabase() throws IOException, SQLException,
            ClassNotFoundException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, InterruptedException {
        LOGGER.info("Starting TestCase \"featureIdis256CharactersAndPresentinDatabase\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.FEATURE_ID_256_CHARACTERS_PRESENT_IN_DATABASE);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateSuccessResponse(response);
        FetchFeatureUtils.checkDatabaseAndCompare(testdata, false);
        String featureId = FetchFeatureUtils.getFeatureId(testdata);
        String XRequestId = FetchFeatureUtils.getXRequestId(testdata);
        LOGGER.info("The feature Id is " + featureId);
        LOGGER.info("The feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchFeatureUtils.verifylogsforSuccessResponse(featureId, XRequestId);

    }

    @Test(groups = "FetchFeature")
    public void featureIdis255CharactersAndPresentinDatabase() throws IOException, SQLException,
            ClassNotFoundException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, InterruptedException {
        LOGGER.info("Starting TestCase \"featureIdis255CharactersAndPresentinDatabase\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.FEATURE_ID_255_CHARACTERS_PRESENT_IN_DATABASE);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateSuccessResponse(response);
        FetchFeatureUtils.checkDatabaseAndCompare(testdata, false);
        String featureId = FetchFeatureUtils.getFeatureId(testdata);
        String XRequestId = FetchFeatureUtils.getXRequestId(testdata);
        LOGGER.info("The feature Id is " + featureId);
        LOGGER.info("The feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchFeatureUtils.verifylogsforSuccessResponse(featureId, XRequestId);

    }

    @Test(groups = "FetchFeature")
    public void verifyWhenFeatureIdisNull() throws IOException, SQLException, ClassNotFoundException,
            InterruptedException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException {
        LOGGER.info("Starting TestCase \"verifyWhenFeatureIdisNull\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.FEATURE_ID_IS_NULL);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateSuccessResponse(response);
        String featureId = FetchFeatureUtils.getFeatureId(testdata);
        String XRequestId = FetchFeatureUtils.getXRequestId(testdata);
        LOGGER.info("The feature Id is " + featureId);
        LOGGER.info("The feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchFeatureUtils.verifylogsforSuccessResponse(featureId, XRequestId);
    }

    @Test(groups = "FetchFeature")
    public void featureIdNotMappedToTenant() throws IOException, SQLException, ClassNotFoundException {
        LOGGER.info("Starting TestCase \"featureIdNotMappedToTenant\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.FEATURE_ID_NOT_MAPPED_WITH_TENANT);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateResponseBodyisNull(response);

    }

    @Test(groups = "FetchFeature")
    public void featureIdisRepeated() throws IOException, SQLException, ClassNotFoundException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("Starting TestCase \"featureIdNotMappedToTenant\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.VALID_DATA);
        Response response = FetchFeatureUtils.getResponseInputParametersRepeated(testdata, "FEATUREIDREPEATED", false);
        FetchFeatureUtils.validateMalformedResponse(response);
        String featureId = FetchFeatureUtils.getFeatureId(testdata);
        String XRequestId = FetchFeatureUtils.getXRequestId(testdata);

        LOGGER.info("The feature Id is " + featureId);
        LOGGER.info("The XRequest Id is " + XRequestId);
        Thread.sleep(40000);
        FetchFeatureUtils.verifylogsforBadRequest(featureId, XRequestId);
    }

    @Test(groups = "FetchFeature")
    public void featureIdisMissing() throws IOException, SQLException, ClassNotFoundException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("Starting TestCase \"featureIdNotMappedToTenant\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.FEATURE_ID_IS_MISSING);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateSuccessResponse(response);
        // VerifyMultipleFeaturesResponse(response);
    }

    @Test(groups = "FetchFeature")
    public void featureIdisNullAsString() throws IOException, SQLException, ClassNotFoundException {
        LOGGER.info("Starting TestCase \"featureIdNotMappedToTenant\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchFeatureUtils.testData(FetchFeatureUtils.FEATURE_ID_IS_NULL_AS_STRING);
        Response response = FetchFeatureUtils.getResponse(testdata, false);
        FetchFeatureUtils.validateResponseBodyisNull(response);

    }

}
