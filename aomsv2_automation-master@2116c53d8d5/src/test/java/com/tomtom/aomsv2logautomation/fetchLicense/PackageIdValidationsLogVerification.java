/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.fetchLicense;

import com.tomtom.aomsv2.fetchLicense.FetchLicenseUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.xml.bind.JAXBException;

/**
 * This class has the test cases having different validations of Package Id field for Fetch License API and verifying
 * the response.
 * 
 * @author palvadi
 */
public class PackageIdValidationsLogVerification {

    private Logger LOGGER = LoggerFactory.getLogger("FetchLicense.PackageIdValidations");



    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIdIsInInuse() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePackageIdIsInInuse\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.INUSE_PACKAGE);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateSuccessResponse(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforSuccessResponse(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIsinOutofUse() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePackageIsinOutofUse\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.OUTOFUSE_PACKAGE);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIsDeprecated() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePackageIsDeprecated\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.DEPRECATED_PACKAGE);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateSuccessResponse(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforSuccessResponse(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIsNullAsString() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePackageIsNullAsString\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGE_ID_IS_NULL_AS_STRING);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIsNull() throws IOException, SQLException, ClassNotFoundException, JAXBException,
            InterruptedException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePackageIsNull\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGEID_IS_NULL);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIsGreaterThan256Characters() throws IOException, SQLException,
            ClassNotFoundException, JAXBException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePackageIsGreaterThan256Characters\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGE_ID_GREATER_THAN_256_CHARACTERS);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIs256Characters() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePackageIs256Characters\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGE_ID_IS_256_CHARACTERS);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateSuccessResponse(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforSuccessResponse(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIs255Characters() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePackageIs255Characters\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGE_ID_IS_255_CHARACTERS);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateSuccessResponse(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforSuccessResponse(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIdIsNotinDatabase() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, InterruptedException {

        LOGGER.info("Starting TestCase \"fetchLicensePackageIdIsNotinDatabase\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.PACKAGE_ID_NOT_IN_DATABASE);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePassTwoPackageIds() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePassTwoPackageIds\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.TWO_PACKAGEIDS_IN_THE_REQUEST);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePackageIdIsMissing() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePackageIdIsMissing\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.TWO_PACKAGEIDS_IN_THE_REQUEST);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }
}
