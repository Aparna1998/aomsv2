/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.fetchLicense;

import com.tomtom.aomsv2.fetchLicense.FetchLicenseUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.xml.bind.JAXBException;

/**
 * This class has the test cases having different validations of Content Item Id field for Fetch License API and
 * verifying the response.
 * 
 * @author palvadi
 */
public class ContentItemIdValidationsLogVerification {

    private static final Logger LOGGER = LoggerFactory.getLogger("FetchLicense.ContentItemIdValidations");



    @Test(groups = "FetchLicense")
    public void fetchLicenseContentItemIsNullAsString() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("Starting TestCase \"fetchLicenseContentItemIsNullAsString\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.CONTENT_ITEM_ID_IS_NULL_AS_STRING);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);
    }

    @Test(groups = "FetchLicense")
    public void fetchLicenseContentItemIsNull() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicenseContentItemIsNull\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.CONTENT_ITEM_IS_NULL);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicenseContentItemIsGreaterThan256Characters() throws IOException, SQLException,
            ClassNotFoundException, JAXBException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicenseContentItemIsGreaterThan256Characters\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.CONTENTITEMISGREATERTHAN256CHARACTERS);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicenseContentItemIs256Characters() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicenseContentItemIs256Characters\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.CONTENT_ITEM_IS_256_CHARACTERS);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicenseContentItemIs255Characters() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicenseContentItemIs255Characters\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.CONTENT_ITEM_IS_255_CHARACTERS);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicenseContentItemIsNotinDatabase() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicenseContentItemIsNotinDatabase\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.CONTENT_ITEM_ID_IS_NOT_IN_DATABASE);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

    @Test(groups = "FetchLicense")
    public void fetchLicensePassTwoContentItems() throws IOException, SQLException, ClassNotFoundException,
            JAXBException, InterruptedException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException {

        LOGGER.info("Starting TestCase \"fetchLicensePassTwoContentItems\"");

        HashMap<String, String> testdata = new HashMap<String, String>();
        testdata = FetchLicenseUtils.testData(FetchLicenseUtils.TWO_CONTENT_ITEM_IDS_IN_THE_REQUEST);
        String requestbody = FetchLicenseUtils.xmlMapping(testdata);
        Response response = FetchLicenseUtils.getFetchLicenseResponse(requestbody, testdata, false);
        FetchLicenseUtils.validateMalformedRequest(response);
        Object PackageId = FetchLicenseUtils.getPackageId(testdata);
        Object XRequestId = FetchLicenseUtils.getXRequestId(testdata);
        Object TenantId = testdata.get(FetchLicenseUtils.TENANTID);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        FetchLicenseUtils.verifylogsforBadRequest(PackageId, XRequestId, TenantId);

    }

}
