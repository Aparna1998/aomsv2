/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.deleteFeature;

import com.tomtom.aomsv2.deleteFeature.DeleteFeatureUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations of XRequest Id field for Delete Feature API and verifying
 * the response.
 * 
 * @author palvadi
 */
public class DeleteFeatureXRequestValidationsLogVerification {

    private static final Logger LOGGER = LoggerFactory.getLogger("DeleteFeature.DeleteFeature_XRequestValidations");

    @Test(groups = "DeleteFeature")
    public void deleteFeatureXRequestIdis256Characters() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("Starting TestCase \"deleteFeatureXRequestIdis256Characters\"");

        HashMap<Object, Object> testdata = DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_X_REQUEST_ID_IS_256_CHARACTERS);
        DeleteFeatureUtils.addFeature(DeleteFeatureUtils.ADD_FEATURE_VALID_DATA, false);
        Response response = DeleteFeatureUtils.deleteFeature(
                DeleteFeatureUtils.DELETE_FEATURE_X_REQUEST_ID_IS_256_CHARACTERS, false, testdata);
        DeleteFeatureUtils.validateSuccessResponse(response);
        Object FeatureId = DeleteFeatureUtils.getFeatureId(testdata);
        Object XRequestId = DeleteFeatureUtils.getXRequestId(testdata);
        LOGGER.info("The Feature Id is " + FeatureId);
        LOGGER.info("The XRequest Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        DeleteFeatureUtils.verifylogsforSuccessResponse(FeatureId, XRequestId);

    }

    @Test(groups = "DeleteFeature")
    public void deleteFeatureXRequestIdis255Characters() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("Starting TestCase \"deleteFeatureXRequestIdis255Characters\"");

        HashMap<Object, Object> testdata = DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_X_REQUEST_ID_IS_255_CHARACTERS);
        DeleteFeatureUtils.addFeature(DeleteFeatureUtils.ADD_FEATURE_VALID_DATA, false);
        Response response = DeleteFeatureUtils.deleteFeature(
                DeleteFeatureUtils.DELETE_FEATURE_X_REQUEST_ID_IS_255_CHARACTERS, false, testdata);
        DeleteFeatureUtils.validateSuccessResponse(response);
        Object FeatureId = DeleteFeatureUtils.getFeatureId(testdata);
        Object XRequestId = DeleteFeatureUtils.getXRequestId(testdata);
        LOGGER.info("The Feature Id is " + FeatureId);
        LOGGER.info("The XRequest Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        DeleteFeatureUtils.verifylogsforSuccessResponse(FeatureId, XRequestId);

    }

    @Test(groups = "DeleteFeature")
    public void deleteFeatureXRequestIdisGreaterThan256Characters() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("Starting TestCase \"deleteFeatureXRequestIdisGreaterThan256Characters\"");

        HashMap<Object, Object> testdata = DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_FEATURE_ID_IS_GREATER_THAN_256_CHARACTERS);

        Response response = DeleteFeatureUtils.deleteFeature(
                DeleteFeatureUtils.DELETE_FEATURE_FEATURE_ID_IS_GREATER_THAN_256_CHARACTERS, false, testdata);
        DeleteFeatureUtils.validateBadRequestResponse(response);
        Object FeatureId = DeleteFeatureUtils.getFeatureId(testdata);
        Object XRequestId = DeleteFeatureUtils.getXRequestId(testdata);
        String LogMessage = "featureId request parameter validation failed: ValidationException{parameterName='featureId', value='null', errorType=NOT_FOUND";
        LOGGER.info("The Feature Id is " + FeatureId);
        LOGGER.info("The XRequest Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        DeleteFeatureUtils.verifylogsforBadRequest(FeatureId, XRequestId, LogMessage);

    }

    @Test(groups = "DeleteFeature")
    public void deleteFeatureXRequestIdisNull() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("Starting TestCase \"deleteFeatureXRequestIdisNull\"");

        HashMap<Object, Object> testdata = DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_TENANT_NOT_MAPPED);
        DeleteFeatureUtils.addFeature(DeleteFeatureUtils.ADD_FEATURE_VALID_DATA, false);
        Response response = DeleteFeatureUtils.deleteFeature(DeleteFeatureUtils.DELETE_FEATURE_TENANT_NOT_MAPPED,
                false, testdata);
        DeleteFeatureUtils.validateSuccessResponse(response);
        Object FeatureId = DeleteFeatureUtils.getFeatureId(testdata);
        Object XRequestId = DeleteFeatureUtils.getXRequestId(testdata);
        LOGGER.info("The Feature Id is " + FeatureId);
        LOGGER.info("The XRequest Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        DeleteFeatureUtils.verifylogsforSuccessResponse(FeatureId, XRequestId);

    }

    @Test(groups = "DeleteFeature")
    public void deleteFeatureXRequestIdisMissing() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("Starting TestCase \"deleteFeatureXRequestIdisMissing\"");

        HashMap<Object, Object> testdata = DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_X_REQUEST_ID_IS_NULL);
        DeleteFeatureUtils.addFeature(DeleteFeatureUtils.ADD_FEATURE_VALID_DATA, false);
        Response response = DeleteFeatureUtils.deleteFeatureXRequestIdisMissing(
                DeleteFeatureUtils.DELETE_FEATURE_X_REQUEST_ID_IS_NULL, testdata);
        DeleteFeatureUtils.validateSuccessResponse(response);
    }

    @Test(groups = "DeleteFeature")
    public void deleteFeatureXRequestIdisEmpty() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("Starting TestCase \"deleteFeatureXRequestIdisEmpty\"");
        HashMap<Object, Object> testdata = DeleteFeatureUtils
                .deleteFeatureTestData(DeleteFeatureUtils.DELETE_FEATURE_VALID_DATA);
        Object tenant = testdata.get(DeleteFeatureUtils.TENANT_ID_DELETE_FEATURE);
        System.out.println("The tenant is " + tenant);
        DeleteFeatureUtils.addFeature(DeleteFeatureUtils.ADD_FEATURE_VALID_DATA, false);
        Response response = DeleteFeatureUtils.deleteFeatureXRequestIdisEmpty(testdata);
        DeleteFeatureUtils.validateBadRequestResponse(response);

    }

}
