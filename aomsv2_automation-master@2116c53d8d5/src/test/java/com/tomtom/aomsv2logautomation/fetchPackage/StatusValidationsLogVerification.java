/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.fetchPackage;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.fail;

import com.tomtom.aomsv2.commonControls.CommonData;
import com.tomtom.aomsv2.fetchPackage.FetchPackageUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations of Status field for Fetch Package API and verifying the
 * response.
 * 
 * @author palvadi
 */
public class StatusValidationsLogVerification {

    private Logger LOGGER = LoggerFactory.getLogger("fetchPackage.ValidData");


    @Test(groups = "FetchPackage")
    public void fetchPackageStatusisMissing() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageStatusisMissing\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.STATUS_MISSING);
            Response response = given().relaxedHTTPSValidation()
                    .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                    .param("startDate", testdata.get(FetchPackageUtils.START_DATE)).param("endDate", "")
                    .param("status", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
            LOGGER.info("The Start Date set is " + testdata.get(FetchPackageUtils.START_DATE));
            LOGGER.info("The End Date set is as Null");
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);

            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforBadRequestResponse(PackageId, XRequestId);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageOnlyStatesisGivenintheRequestInuse() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageOnlyStatesisGivenintheRequestInuse\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.ONLY_STATES_IS_GIVEN_IN_REQUEST_INUSE);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateSuccessResponse(response);
            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageDuplicateParameterStatesIsGiven() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageDuplicateParameterStatesIsGiven\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.ONLY_STATES_IS_GIVEN_IN_REQUEST_INUSE);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The status set is set twice with the value " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);

            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforBadRequestResponse(PackageId, XRequestId);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageOnlyStatesisGivenintheRequestOutofUse() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageOnlyStatesisGivenintheRequestOutofUse\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.ONLY_STATES_IS_GIVEN_IN_REQUEST_INUSE);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateSuccessResponse(response);
            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageOnlyStatesisGivenintheRequestDeprecated() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageOnlyStatesisGivenintheRequestDeprecated\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.ONLY_STATES_IS_GIVEN_IN_REQUEST_INUSE);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateSuccessResponse(response);
            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageWhenMultipleStatesisGivenInuseAndDeprecated() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageWhenMultipleStatesisGivenInuseAndDeprecated\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.INUSE_AND_OUT_OF_USE_IN_SAME_STATES_PARAM);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateSuccessResponse(response);
            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStatesandDateRangeIsGiven() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageStatesandDateRangeIsGiven\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.ONLY_STATES_IS_GIVEN_IN_REQUEST_INUSE);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .param("startDate", testdata.get(FetchPackageUtils.START_DATE))
                    .param("endDate", testdata.get(FetchPackageUtils.END_DATE))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateSuccessResponse(response);
            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageWhenMultipleStatesisGivenOutofUseAndDeprecated() throws IOException,
            ClassNotFoundException, SQLException {

        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.OUT_OF_USE_AND_DEPRECATED_IN_SAME_STATES_PARAM);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateSuccessResponse(response);
            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStatesIsGivenInInvalidFormatInuse() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageStatesIsGivenInInvalidFormatInuse\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.STATUS_INUSE_INVALID_FORMAT);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);

            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforBadRequestResponse(PackageId, XRequestId);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStatesIsGivenInInvalidFormatOutofUse() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageStatesIsGivenInInvalidFormatOutofUse\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.STATUS_OUT_OF_USE_INVALID_FORMAT);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);

            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforBadRequestResponse(PackageId, XRequestId);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStatesIsGivenInInvalidFormatDeprecated() throws IOException, ClassNotFoundException,
            SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageStatesIsGivenInInvalidFormatDeprecated\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.STATUS_DEPRECATED_INVALID_FORMAT);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateMalformedResponse(response);

            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforBadRequestResponse(PackageId, XRequestId);

        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageFeatureIdandStatesisGiven() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageFeatureIdandStatesisGiven\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.VALID_DATA);
            Response response = given().relaxedHTTPSValidation()
                    .param("featureId", testdata.get(FetchPackageUtils.FEATURE_ID))
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Feature Id set is " + testdata.get(FetchPackageUtils.FEATURE_ID));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateResponseisNull(response);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackagePackageIdandStatesisGiven() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"fetchPackagePackageIdandStatesisGiven\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.VALID_DATA);
            Response response = given().relaxedHTTPSValidation()
                    .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateResponseisNull(response);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    @Test(groups = "FetchPackage")
    public void fetchPackageStatesandDateRangeisGiven() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageStatesandDateRangeisGiven\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.VALID_DATA);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateSuccessResponse(response);
            FetchPackageUtils.checkDBAndCompare(response, false);
            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);
        }

        catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

    // Tosee

    @Test(groups = "FetchPackage")
    public void fetchPackagePackageNotAssosiatedWithState() throws IOException, ClassNotFoundException, SQLException {

        LOGGER.info("Starting TestCase \"fetchPackageStatesandDateRangeisGiven\"");
        try {
            HashMap<Object, Object> testdata = new HashMap<Object, Object>();
            testdata = FetchPackageUtils.testData(FetchPackageUtils.VALID_DATA);
            Response response = given().relaxedHTTPSValidation()
                    .param("states", testdata.get(FetchPackageUtils.STATUS))
                    .param("packageId", testdata.get(FetchPackageUtils.PACKAGE_ID))
                    .header("X-TENANT", testdata.get(FetchPackageUtils.XTENANT))
                    .header("X-Request-Id", testdata.get(FetchPackageUtils.XREQUESTID)).when()
                    .get(CommonData.FETCH_PACKAGE);

            LOGGER.info("The Package Id set is " + testdata.get(FetchPackageUtils.PACKAGE_ID));
            LOGGER.info("The status set is " + testdata.get(FetchPackageUtils.STATUS));
            LOGGER.info("The Tenant Id set is " + testdata.get(FetchPackageUtils.XTENANT));
            LOGGER.info("The XRequest Id set is " + testdata.get(FetchPackageUtils.XREQUESTID));

            FetchPackageUtils.validateSuccessResponse(response);
            FetchPackageUtils.checkDBAndCompare(response, false);
            Object PackageId = FetchPackageUtils.getPackageId(testdata);
            Object XRequestId = FetchPackageUtils.getXRequestId(testdata);
            LOGGER.info("The Package Id is " + PackageId);
            LOGGER.info("The Feature Id is " + XRequestId);
            LOGGER.info("Slept");
            Thread.sleep(40000);
            LOGGER.info("woke up");
            FetchPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);
        } catch (Exception e) {
            fail("The exception message is " + e.getStackTrace());
        }
    }

}
