/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.addPackage;

import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackage;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackageTestData;

import com.tomtom.aomsv2.addPackage.AddPackageUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;

/**
 * This class has the test cases having different validations for Checksum Value field in Add Package Request and
 * verifying the response.
 * 
 * @author palvadi
 */
public class ChecksumValueValidationsLogAutomation {

    private static final Logger LOGGER = LoggerFactory.getLogger("AddPackage.ChecksumValueValidations");


    @Test(groups = "AddPackage")
    public void checksumValueis2047Characters() throws IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException, InterruptedException {
        LOGGER.info("Starting TestCase \"ChecksumValueis2047characters\"");
        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.CHECKSUM_VALUE_IS_2047_CHARACTERS);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateSuccessResponse(response);
        Object PackageId = AddPackageUtils.getPackageId(addPackageTestData);
        Object XRequestId = AddPackageUtils.getXRequestId(addPackageTestData);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        AddPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);
    }

    @Test(groups = "AddPackage")
    public void checksumValuehasSpecialCharacters() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("Starting TestCase \"ChecksumValuehasspecialcharacters\"");
        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.CHECKSUM_VALUE_HAS_SPECIAL_CHARACTERS);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateSuccessResponse(response);
        Object PackageId = AddPackageUtils.getPackageId(addPackageTestData);
        Object XRequestId = AddPackageUtils.getXRequestId(addPackageTestData);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        AddPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);
    }

    @Test(groups = "AddPackage")
    public void checksumValueis2048Characters() throws IOException, UnrecoverableKeyException, KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException, InterruptedException {
        LOGGER.info("Starting TestCase \"ChecksumValueis2048characters\"");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.CHECKSUM_VALUE_IS_2048_CHARACTERS);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateSuccessResponse(response);
        Object PackageId = AddPackageUtils.getPackageId(addPackageTestData);
        Object XRequestId = AddPackageUtils.getXRequestId(addPackageTestData);
        LOGGER.info("The Package Id is " + PackageId);
        LOGGER.info("The Feature Id is " + XRequestId);
        LOGGER.info("Slept");
        Thread.sleep(40000);
        LOGGER.info("woke up");
        AddPackageUtils.verifylogsforSuccessResponse(PackageId, XRequestId);

    }

    @Test(groups = "AddPackage")
    public void checksumValueisGreaterThan2048Characters() throws IOException, InterruptedException,
            UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {
        LOGGER.info("Starting TestCase \"ChecksumValueisgreaterthan2048characters\"");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.CHECKSUM_VALUE_IS_GREATER_THAN_1024_CHARACTERS);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void checksumValueisNull() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("Starting TestCase \"ChecksumValueisgreaterthan2048characters\"");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.CHECKSUM_VALUE_IS_NULL);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void checksumValueisMissing() throws IOException, InterruptedException, UnrecoverableKeyException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        LOGGER.info("Starting TestCase \"checksumValueisMissing\"");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.CHECKSUM_VALUE_IS_MISSING);
        Response response = addPackage(addPackageTestData, false);
        AddPackageUtils.validateMalformedRequest(response);

    }

}
