/*******************************************************************************
 * Copyright (C) 2018 TomTom. All rights reserved.
 *
 * This Java class is subject of the following restrictions:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of  source code must  retain the above copyright  notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The end-user documentation included with the redistribution, if any, must
 *    include  the following  acknowledgment:  "This product includes  software
 *    developed  by TomTom."
 *    Alternately, this  acknowledgment may  appear in the software itself,  if
 *    and wherever such third-party acknowledgments normally appear.
 *
 * 4. The name ''TomTom'' must  not  be used to  endorse or promote  products
 *    derived from  this software without prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS  FOR A PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT SHALL  WWS
 * OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES   (INCLUDING, BUT  NOT  LIMITED  TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOS  OF USE, DATA, OR  PROFITS;
 * OR BUSINESS  INTERRUPTION)  HOWEVER CAUSED AND ON ANY  THEORY  OF LIABILITY,
 * WHETHER  IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  NEGLIGENCE  OR
 * OTHERWISE) ARISING IN  ANY WAY OUT OF THE  USE  OF THIS  SOFTWARE,  EVEN  IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.tomtom.aomsv2logautomation.addPackage;

import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackageTestData;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.addPackagewhenRepeated;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.jsonMapping;
import static com.tomtom.aomsv2.addPackage.AddPackageUtils.validateSuccessResponse;

import com.tomtom.aomsv2.addPackage.AddPackageUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;

/**
 * This class has the test cases for adding multiple packages in the same request and validating the response.
 * 
 * @author palvadi
 */
public class AddingMultiplePackagesLogAutomation {

    private static final Logger LOGGER = LoggerFactory.getLogger("AddPackage.AddingMultiplePackages");



    @Test(groups = "AddPackage")
    public void addMultiplePackagesinthesamerequest() throws IOException {

        LOGGER.info("Starting TestCase \"addMultiplePackagesinthesamerequest\"");

        HashMap<Object, Object> addPackageTestData1 = new HashMap<Object, Object>();
        HashMap<Object, Object> addPackageTestData2 = new HashMap<Object, Object>();
        HashMap<Object, Object> addPackageTestData3 = new HashMap<Object, Object>();
        addPackageTestData1 = addPackageTestData(AddPackageUtils.IN_USE_PACKAGE);
        addPackageTestData2 = addPackageTestData(AddPackageUtils.OUT_OF_USE_PACKAGE);
        addPackageTestData3 = addPackageTestData(AddPackageUtils.METADATA_MULTIPLE_CONTENT_ITEMS);
        String requestbody1 = jsonMapping(addPackageTestData1);
        String requestbody2 = jsonMapping(addPackageTestData2);
        String requestbody3 = jsonMapping(addPackageTestData3);
        String requestbody = '[' + requestbody1 + ',' + requestbody2 + ',' + requestbody3 + ']';
        LOGGER.info("the request body is  " + requestbody);
        Response response = addPackagewhenRepeated(addPackageTestData1, requestbody, false);
        validateSuccessResponse(response);

    }

    @Test(groups = "AddPackage")
    public void addMultiplePackagesHavingTheSamePackageIdInthesamerequest() throws IOException {

        LOGGER.info("Starting TestCase \"addMultiplePackagesHavingTheSamePackageIdInthesamerequest\"");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.IN_USE_PACKAGE);
        String requestbody1 = jsonMapping(addPackageTestData);
        int length1 = requestbody1.length();
        String requestbodyedited1 = requestbody1.substring(2, length1 - 1);
        String requestbody2 = jsonMapping(addPackageTestData);
        int length2 = requestbody2.length();
        String requestbodyedited2 = requestbody2.substring(2, length2 - 1);
        String requestbody = '[' + requestbodyedited1 + requestbodyedited2 + ']';
        Response response = addPackagewhenRepeated(addPackageTestData, requestbody, false);
        AddPackageUtils.validateMalformedRequest(response);

    }

    @Test(groups = "AddPackage")
    public void addMultiplePackagesHavingTheSamePartNumber() throws IOException {

        LOGGER.info("Starting TestCase \"addMultiplePackagesHavingTheSamePartNumber\"");

        HashMap<Object, Object> addPackageTestData = new HashMap<Object, Object>();
        addPackageTestData = addPackageTestData(AddPackageUtils.IN_USE_PACKAGE);
        String requestbody1 = jsonMapping(addPackageTestData);
        int length1 = requestbody1.length();
        String requestbodyedited1 = requestbody1.substring(2, length1 - 1);
        String requestbody2 = jsonMapping(addPackageTestData);
        int length2 = requestbody2.length();
        String requestbodyedited2 = requestbody2.substring(2, length2 - 1);
        String requestbody = '[' + requestbodyedited1 + ',' + requestbodyedited2 + ']';
        Response response = addPackagewhenRepeated(addPackageTestData, requestbody, false);
        AddPackageUtils.validateMalformedRequest(response);

    }

}